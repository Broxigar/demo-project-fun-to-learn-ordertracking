const { addDecoratorsLegacy, override, disableEsLint, fixBabelImports } = require('customize-cra');

module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: 'css',
  }),
  addDecoratorsLegacy(),
  disableEsLint()
);

// module.exports = {
//     webpack: override(
//         addDecoratorsLegacy(),
//         disableEsLint()
//     )
// };
