import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import moment from 'moment'
import momenttz from 'moment-timezone'

import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Collapse from '@material-ui/core/Collapse';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContentText from '@material-ui/core/DialogContentText';

import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';

import Edit from '@material-ui/icons/Edit';
import DeleteForever from '@material-ui/icons/DeleteForever';
import LocalLibrary from '@material-ui/icons/LocalLibrary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ArrowBack from '@material-ui/icons/ArrowBack';
import {PermissibleRender} from '@brainhubeu/react-permissible';

import {success, gray, info, warning, error, bginfo, bgwarning, bgerror} from '../common/Colors'
import NewCourse from '../common/NewCourse'
import { GenerateFileLinkView } from '../util/generateFileLink';
import {Spin } from 'antd'
import loading from '../util/loading';


const styles = theme => ({
  root: {
    textAlign: 'left',
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing.unit *2
  },
  paddingTop:{
    paddingTop: theme.spacing.unit * 2,
  },
  paddingLeft:{
    paddingLeft: theme.spacing.unit * 2,
  },
  padding: {
    paddingBottom: theme.spacing.unit*2,
    paddingTop: theme.spacing.unit*1,
  },
  button: {
    margin: theme.spacing.unit,
  },
  label:{
    padding: '3px',
    paddingLeft: '5px',
    paddingRight: '5px',
    marginRight: '5px',
    marginTop: '4px',
    borderRadius: 3,
    fontSize: '12px',
    color: gray, 
    textDecoration: 'None',
  },
  list:{
    color: gray,
    padding: '3px',
    fontSize: '14px',
    textDecoration: 'underline',
  },
  Success: {
    backgroundColor: success,
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8,
    },
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  img: {
    padding: 5,
    paddingBottom: 8,
    width: theme.breakpoints.values.lg/4,
  },
  span: {
    fontSize: '14px',
    color: gray,
    paddingLeft: '5px'
  },
  badge: {
    top: 5,
    right: -15,
    cursor: 'pointer',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
    // width: theme.breakpoints.values.sm,
  },
  fullWidth: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit,
    border: 2,
    width: theme.breakpoints.values.lg,
  }, 


});

@withStyles(styles)
@inject('helperStore', 'ordersStore', 'courseStore', 'authStore', 'userStore', 'utilStore')
@withRouter
@observer
export default class CourseDetail extends Component{
  state = {
    expanded: this.props.match.params.idcourse?true:false,
    isLoading: false,
    errorInfo: '',
    mode: 'view', 
    dialogOpen: false,
  };

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };
  handleOrderEdit = () => {
    this.setState({mode: 'edit'})
  }
  handleCancleEdit = () => {
    this.setState({mode: 'view'})
  }

  handleDialogClose = () => {
    this.setState({ dialogOpen: false });
  }

  handleDialogOpen = () => {
    this.setState({ dialogOpen: true });
  }

  handleOrderDelete(idcourse) {
    this.setState({isLoading: true, dialogOpen: false})
    this.props.courseStore.deleteCourse(idcourse).then(res => {
      if(res.statusCode !== 200){
        this.setState({errorInfo: res})
      }else{
        this.setState({errorInfo: ""})
      }
      this.setState({isLoading: false})
    })
  }

  render(){
    const { classes, course, match, helperStore, courseStore, authStore, userStore, utilStore} = this.props;
    let order = course

    if(!order) order = courseStore.getCourseById(match.params.idcourse)
    courseStore.isLoading||!order ? loading : ''

    let timezone = momenttz.tz.guess()
    let isoCreateDate = moment(order.dateTime, moment.ISO_8601).tz(timezone)
    let localCreatedate = isoCreateDate.format("YYYY-MM-DD HH:mm")

    if(this.state.mode == 'edit'){
      return <NewCourse course={order} formName="Modify Course" cancleFunction={this.handleCancleEdit} />
    }
  
    return(
      <Grid container item xs={12} className={classes.root} key={order.idcource}>
        {this.props.match.params.idcourse ? 
          <Grid item xs={12}>
            <Button variant="outlined" color="secondary" size="small" onClick={this.props.history.goBack}>
                <ArrowBack /> &nbsp; Go Back 
            </Button>
            <Grid item xs={12} className={classes.padding}><Divider/></Grid>
          </Grid> : ''}
        
        <Grid container item xs={12} >
          {this.state.errorInfo}
          <Grid item xs={10} >
            <Typography variant="subheading" gutterBottom >
              <LocalLibrary/> {`${order.idcource}. ${order.courseCode||''} : ${order.name||''} - ${order.courseType||'' } - ${ order.university||''} - ${order.country||''} `}
            </Typography>
            <Typography vcolor="textSecondary" variant="caption"  gutterBottom>
              Last Updated on Date: {localCreatedate} By {userStore.getUserById(order.createBy).name + ' (id:'+order.createBy +')'}
            </Typography>
          </Grid>
          <Grid container item xs={2} justify="flex-end">
            <Grid item>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
                <IconButton onClick={ this.handleDialogOpen }>
                  <DeleteForever />
                </IconButton> 
              </PermissibleRender>
              <IconButton onClick={this.handleOrderEdit}>
                <Edit />
              </IconButton>
              <IconButton
                    className={classnames(classes.expand, {
                      [classes.expandOpen]: this.state.expanded,
                    })}
                    onClick={this.handleExpandClick}
                    aria-expanded={this.state.expanded}
                    aria-label="Show more"
                  >
                    <ExpandMoreIcon />
              </IconButton>
            </Grid>
          </Grid>
          <Grid container item xs={12}>
            <Collapse in={this.state.expanded} timeout="auto" className={classes.fullWidth}>
              <Spin spinning={utilStore.isDownloading}>
                <Grid item xs={12}>
                  <pre className={classes.span}  style={{whiteSpace: 'pre-wrap', wordBreak: 'keep-all'}}>
                    {order.description}
                  </pre>
                </Grid>
                <Grid item xs={12}>
                  <List>
                    Documents:
                    <span className={[classes.label , classes.Success].join(' ')}><a role="button" onClick={() => utilStore.downloadAllFiles(`${order.idcource}. ${order.courseCode||''} : ${order.name||''} - ${order.courseType||'' } - ${ order.university||''} - ${order.country||''} `, order.fileUrl, 'Course Description.txt', order.description)} >Download All</a></span>
                    <GenerateFileLinkView files={order.fileUrl} />
                  </List>
                </Grid>
              </Spin>
            </Collapse> 
          </Grid>  
        </Grid>
        
        
        <Grid item xs={12} className={classes.paddingTop}><Divider/></Grid>
        <Dialog
          open={this.state.dialogOpen}
          onClose={this.handleDialogClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{ "Are you sure to delete?" }</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {order.idcource + '. ' + order.name + ' - ' + order.schoole}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleDialogClose} color="primary">
              Cancle
            </Button>
            <Button onClick={() => this.handleOrderDelete(order.idcource)} color="primary" autoFocus>
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>
      );
    }
} 
