import React from 'react';
import { observer } from 'mobx-react';

import classNames from 'classnames';
import Select from 'react-select';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import CancelIcon from '@material-ui/icons/Cancel';
import {emphasize} from '@material-ui/core/styles/colorManipulator';


const styles = theme => ({
  input: {
    display: 'flex',
    padding: 0,
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden',
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08,
    ),
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  singleValue: {
    fontSize: 16,
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16,
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  divider: {
    height: theme.spacing.unit * 1,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginTop: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
});

function NoOptionsMessage(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

function Control(props) {
  return (
    <TextField
      fullWidth
      InputProps={{
        inputComponent,
        inputProps: {
          className: props.selectProps.classes.input,
          inputRef: props.innerRef,
          children: props.children,
          ...props.innerProps,
        },
      }}
      {...props.selectProps.textFieldProps}
    />
  );
}

function Option(props) {
  return (
    <MenuItem
      buttonRef={props.innerRef}
      selected={props.isFocused}
      component="div"
      style={{
        fontWeight: props.isSelected ? 500 : 400,
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

function Placeholder(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.placeholder}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function SingleValue(props) {
  return (
    <Typography className={props.selectProps.classes.singleValue} {...props.innerProps}>
      {props.children}
    </Typography>
  );
}

function ValueContainer(props) {
  return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
}

function MultiValue(props) {
  return (
    <Chip
      tabIndex={-1}
      label={props.children}
      className={classNames(props.selectProps.classes.chip, {
        [props.selectProps.classes.chipFocused]: props.isFocused,
      })}
      onDelete={props.removeProps.onClick}
      deleteIcon={<CancelIcon {...props.removeProps} />}
    />
  );
}

function Menu(props) {
  return (
    <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
      {props.children}
    </Paper>
  );
}

const components = {
  Control,
  Menu,
  MultiValue,
  NoOptionsMessage,
  Option,
  Placeholder,
  SingleValue,
  ValueContainer,
};

@withStyles(styles)
@observer
export  class AutoCompleteCourse extends React.Component {
  state = {
    selected: this.props.defaultValue
  }

  localSelectValue = (selectedObj) => {
    let obj = {}
    obj['target'] = {id:'', value:''}
    if(selectedObj){
      obj['target']['id'] = 'idcourse'
      obj['target']['value'] = selectedObj.value
      // console.log(obj)
      this.props.onChangeCallback(obj)
    }else{
      this.props.onChangeCallback(obj)
    }
    this.setState({selected: selectedObj})
   
  }
  render() {
    const { classes, theme, placeholderText, options } = this.props;
    const { selected } = this.state

    let localOptions = []
    for(let option of options){
      // console.log(option)
      localOptions.push({label: `${option.courseCode}: ${option.name}`, value: option.idcource})
    }

    const selectStyles = {
      input: base => ({
        ...base,
        '& input': {
          font: 'inherit',
        },
      }),
    };

    return (
          <Select
            classes={classes}
            className={classes.textField}
            styles={selectStyles}
            options={localOptions}
            components={components}
            value={selected}
            onChange={this.localSelectValue}
            placeholder={placeholderText}
            textFieldProps={{
              label: 'Type Course Name or Course Code to Filter:',
              InputLabelProps: {
                shrink: true,
              },
            }}
            isClearable
          />
    );
  }
}



@withStyles(styles)
@observer
export  class AutoCompleteUniversity extends React.Component {
  state = {
    //this.props.defaultValue
    selected: ''
  }
  localSelectValue = (selectedObj) => {
    this.setState({
      selected: selectedObj
    })
    this.props.onChangeCallback(selectedObj)
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.country !== this.props.country){
      this.setState({
        selected: ""
      })
    }

  }

  render() {
    const { classes, theme, placeholderText, options } = this.props;
    const { selected } = this.state

    let localOptions = []
    for(let option of options){
      // console.log(option)
      localOptions.push({label: option.name, value: option.name})
    }

    const selectStyles = {
      input: base => ({
        ...base,
        '& input': {
          font: 'inherit',
        },
      }),
    };

    return (
          <Select
            classes={classes}
            className={classes.textField}
            styles={selectStyles}
            options={localOptions}
            components={components}
            value={selected}
            onChange={this.localSelectValue}
            placeholder={placeholderText}
            textFieldProps={{
              label: 'Type to Select University',
              InputLabelProps: {
                shrink: true,
              },
            }}
            isClearable
          />
    );
  }
}

@withStyles(styles)
@observer
export  class AutoCompleteCustomer extends React.Component {
  state = {
    selected: this.props.defaultValue
  }
  localSelectValue = (selectedObj) => {
    this.setState({
      selected: selectedObj
    })
    this.props.onChangeCallback(selectedObj)
  }
  render() {
    const { classes, theme, placeholderText, options } = this.props;
    const { selected } = this.state

    let localOptions = []
    for(let option of options){
      // console.log(option)
      localOptions.push({label: `${option.code}-${option.customerName}-${option.customerEmail}-${option.university}-${option.wechatName}`, value: option.idcustomer})
    }

    const selectStyles = {
      input: base => ({
        ...base,
        '& input': {
          font: 'inherit',
        },
      }),
    };

    return (
          <Select
            classes={classes}
            className={classes.textField}
            styles={selectStyles}
            options={localOptions}
            components={components}
            value={selected}
            onChange={this.localSelectValue}
            placeholder={placeholderText}
            textFieldProps={{
              label: 'Type to Select Customer',
              InputLabelProps: {
                shrink: true,
              },
            }}
            isClearable
          />
    );
  }
}

@withStyles(styles)
@observer
export  class AutoCompleteUser extends React.Component {
  state = {
    selected: this.props.defaultValue
  }
  localSelectValue = (selectedObj) => {
    this.setState({
      selected: selectedObj
    })
    this.props.onChangeCallback(selectedObj)
  }
  render() {
    const { classes, theme, placeholderText, options } = this.props;
    const { selected } = this.state

    let localOptions = []
    for(let option of options){
      // console.log(option)
      localOptions.push({label: `${option.name}-${(''+option.role).split('|').join(',')}-${(''+option.labels).split('|').join(',')}`, value: option.iduser})
    }

    const selectStyles = {
      input: base => ({
        ...base,
        '& input': {
          font: 'inherit',
        },
      }),
    };

    return (
          <Select
            classes={classes}
            className={classes.textField}
            styles={selectStyles}
            options={localOptions}
            components={components}
            value={selected}
            onChange={this.localSelectValue}
            placeholder={placeholderText}
            textFieldProps={{
              label: 'Type to Select User',
              InputLabelProps: {
                shrink: true,
              },
            }}
            isClearable
          />
    );
  }
}
