import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import blue from '@material-ui/core/colors/blue';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

import SearchIcon from '@material-ui/icons/Search'

import TablePagination from "@material-ui/core/TablePagination";
import moment from 'moment'
import loading from "./loading";
import OrderDetail from './OrderDetail'
import AssignFuncBar from './AssignFuncBar'
import { gray, lightBlue} from './Colors'
import abc from './afk'

const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  paddingTop: {
    paddingTop: theme.spacing.unit,
    backgroundColor: 'None',
  },
  padding: {
    padding: `0 ${theme.spacing.unit * 2}px`,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
    paddingTop: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
  textField: {
    marginLeft: 8,
    flex: 1,
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  listitemBackColor: {
    backgroundColor: blue[100],
  },
  iconButton: {
    padding: 10,
  },
  search:{
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  },
  span: {
    fontSize: '14px',
    color: gray,
    paddingLeft: '5px'
  },
  label:{
    padding: '3px',
    paddingLeft: '5px',
    paddingRight: '5px',
    marginRight: '5px',
    marginTop: '4px',
    marginLeft: '3px',
    borderRadius: 3,
    fontSize: '12px',
    textDecoration: 'None',
    backgroundColor: lightBlue,
  },
  toggleContainer: {
    height: 56,
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    margin: `${theme.spacing.unit}px 0`,
    background: theme.palette.background.default,
    selected: lightBlue
  },
});

@withStyles(styles)
@inject('ordersStore', 'courseStore')
@withRouter
@observer
export default class Orders extends Component{
  state = {
    currentStatus: 'confirming',
    orderStatus: [
      { label: 'Admin Confirming', value: 'confirming'}, 
      { label: 'Confirmed & Waiting Assign', value: 'waiting'},
      { label: 'Waiting Writer Confirm', value: 'waiting writer'}, 
      { label: "Writer Processing", value: 'processing'},
      { label: "QA Verifying", value: 'verifying'},
      { label: "Complete", value: 'complete'},
      { label: "Revision/Modifying", value: 'modifying'},
      { label: "Error/Refund Orders", value: 'error'}],
    search: '',
    page: 0, 
    rowsPerPage: 10,
    sort: 'idorder',
  };

  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
  };
  handleInputChange = event => {
    const {id, value} = event.target
    this.setState({[id]: value})
  }
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleSort = (event, sort) => {
    this.setState({
      sort: sort
    })
  };
  
  render(){
    const { classes, courseStore, ordersStore } = this.props;
    const { currentStatus, orderStatus, page, rowsPerPage, sort } = this.state;
    // console.log(this.props.ordersStore.isLoading)
    if(ordersStore.isLoading) return loading
    let current_month = moment().format("YYYY-MM")
    let orders = this.props.ordersStore.Orders
    let ordersCompleted = ordersStore.getHistoryOrders(current_month) || []
    let filtered = []
    let filteredCompleted = []
    if(this.state.search){
      for(let ele of orders){
        let courseDetail = ele.idcourse ? courseStore.getCourseById(ele.idcourse) : {}
        ele['courseDetail'] = courseDetail
        if(JSON.stringify(ele).toLowerCase().indexOf(this.state.search.toLowerCase())>=0){
          filtered.push(ele)
        }
      }
    }else{
      filtered = orders;

    }
    if(this.state.search){
      console.log("SSSSSSSSSSSSSSSSSSSSSSSSSS")
      for(let ele of ordersCompleted){
        let courseDetail = ele.idcourse ? courseStore.getCourseById(ele.idcourse) : {}
        ele['courseDetail'] = courseDetail
        if(JSON.stringify(ele).toLowerCase().indexOf(this.state.search.toLowerCase())>=0){
          filteredCompleted.push(ele)
        }
      }
    }else{
      filteredCompleted = ordersCompleted
    }
console.log(currentStatus)

     let filteredByStatus = currentStatus==='complete'?  filteredCompleted || [] : filtered.filter(o => o.status===currentStatus) || []
      if (sort === "full paid" || sort === "bound paid"|| sort === "part paid"){
      filteredByStatus = filteredByStatus.filter(ele => ele.paymentStatus === sort )
    }
    else filteredByStatus = filteredByStatus.sort((a,b) =>  a[sort]>b[sort] ? -1 : 1 )


    return(
      <Grid container className={classes.root}>
        <Grid container justify={'space-between'} >
          <Grid item className={classes.search}>
            <TextField 
              id='search'
              placeholder="Search Orders" 
              className={classes.textField} 
              onChange={this.handleInputChange}
              value={this.state.search}
              />
            <IconButton className={classes.iconButton} aria-label="Search">
              <SearchIcon />
            </IconButton>
          </Grid>
          <AssignFuncBar currentStatus={currentStatus} />
        </Grid>
        <Tabs 
          value={currentStatus} 
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          className={classes.tabsRoot}
          >
          { orderStatus.map((item, key) => <Tab key={key} value={item.value} label={
            <div>{item.label} 
              <span className={this.props.classes.label}>
                {item.value==='complete' ? filteredCompleted.length : filtered.filter(o => o.status===item.value).length}
              </span>
            </div>
          } /> )}
        </Tabs>
        <Grid item className={[classes.paddingTop, classes.toggleContainer].join(' ')}>
          <ToggleButtonGroup value={this.state.sort}  exclusive onChange={this.handleSort}>
            <ToggleButton value="idorder">
              Create Time
            </ToggleButton>
            <ToggleButton value="deadline">
              Deadline
            </ToggleButton>
            <ToggleButton value="realdeadline">
              Real Deadline
            </ToggleButton>
            <ToggleButton value="full paid">
              Full Paid
            </ToggleButton>
            <ToggleButton value="bound paid">
              Bound Paid
            </ToggleButton>
            <ToggleButton value="part paid">
              Part Paid
            </ToggleButton>
            {/* <ToggleButton value="justify">
              
            </ToggleButton> */}
          </ToggleButtonGroup>
        </Grid>
        <Grid container item className={classes.paddingTop}>
          {filteredByStatus.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => {
            return <OrderDetail order={row} key={index} />
          })
          }
        </Grid>
        <TablePagination
            rowsPerPageOptions={[10, 20, 50]}
            component="div"
            count={
              filteredByStatus.length
            }
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
      </Grid>
    );
  }
} 
