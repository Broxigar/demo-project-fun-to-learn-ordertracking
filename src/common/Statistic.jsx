import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';

import blue from '@material-ui/core/colors/blue';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import OrderDetail from './OrderDetail'
import AssignFuncBar from './AssignFuncBar'

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import {
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer,PieChart, Pie,
} from 'recharts';

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
  cx, cy, midAngle, innerRadius, outerRadius, percent, index,
}) => {
   const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};

const COLORS = ['#a8e6cf', '#dcedc1', '#ffd3b6', '#ffaaa5', '#ff8b94']


const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit * 1,
    overflowX: 'auto',
  },
  paddingTop: {
    paddingTop: theme.spacing.unit * 2,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  listitemBackColor: {
    backgroundColor: blue[100],
  }
});

@withStyles(styles)
@inject('ordersStore', 'authStore')
@withRouter
@observer
export default class Statistic extends Component{
  state = {
    orderStatus: ['confirming', 'waiting', 'waiting writer', 'processing', 'verifying', 'complete', 'modifying', 'error']
  };

  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
  };
 
  render(){
    const { classes, ordersStore, authStore } = this.props;
    let orderStatus = authStore.currentUser.role.indexOf('counsellor')>=0 ? ['processing', 'verifying', 'complete', 'error'] : ['confirming', 'waiting', 'waiting writer', 'processing', 'verifying', 'complete', 'modifying', 'error']
    // let allOrdersStatistic = []
    // let userOrderStatistic = []
    // for(let key of orderStatus){
    //   let tempAll = {}
    //   let tempUser = {}
    //   tempAll['status'] = key
    //   tempAll['number'] = ordersStore.getStatisticByKey(key)||0
    //   allOrdersStatistic.push(tempAll)
    //   tempUser['status'] = key
    //   tempUser['number'] = ordersStore.getMyStatisticByKey(key)||0
    //   userOrderStatistic.push(tempUser)
    // }

    return(
    <Paper className={classes.root}>
      <Grid className={classes.root}>
        <h4>Orders' Statistic</h4>
      </Grid>
      { authStore.currentUser.role.indexOf('counsellor')>=0 ? 
        <Grid className={classes.root}>
          Total {ordersStore.userOrdersDetail.size} orders, {ordersStore.UserOrdersPaidNumber} paid. 
        </Grid>
        : ''
      }
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>No.</TableCell>
            <TableCell align="right">Order Status</TableCell>
            <TableCell align="left">Order Number</TableCell>
            <TableCell align="left">My Order Number</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {orderStatus.map((row, index) => {
            return (
              <TableRow key={row.id} key={index}>
                <TableCell >{index+1}</TableCell>
                <TableCell align="right">{row}</TableCell>
                <TableCell align="left">{ordersStore.getStatisticByKey(row)}</TableCell>
                <TableCell align="left">{ordersStore.getMyStatisticByKey(row)||0}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
{/* 
      <Grid container item className={classes.paddingTop} >
            <Grid item sm={12} md={12} lg={6}>
              <h4>All Orders</h4>
              <ResponsiveContainer width={'100%'} height={300}>
                <PieChart margin={{
                          top: 20, right: 30, left: 20, bottom: 5,
                          }}>
                  <Pie dataKey="number" nameKey="status" isAnimationActive={false} data={allOrdersStatistic} cx={250} cy={120} outerRadius={120} fill="#8884d8"  labelLine={false} label>
                  {
                    allOrdersStatistic.map((entry, index) => <Cell key={`${entry.Country}`} fill={COLORS[index % COLORS.length]} />)
                  }
                  </Pie>
                  <Tooltip />
                  <Legend />
                </PieChart>
              </ResponsiveContainer>
            </Grid>
            <Grid item sm={12} md={12} lg={6}>
              <h4> My Orders</h4>
              <ResponsiveContainer width={'100%'} height={300}>
                <PieChart margin={{
                    top: 20, right: 5, left: 5, bottom: 5,
                    }}>
                  <Pie dataKey="number" nameKey="status" isAnimationActive={false} data={userOrderStatistic} cx={250} cy={120} outerRadius={120} fill="#8884d8" labelLine={false} label >
                  {
                    userOrderStatistic.map((entry, index) => <Cell key={`${entry.Country}`} fill={COLORS[index % COLORS.length]} />)
                  }
                  </Pie>
                  <Tooltip />
                  <Legend />
                </PieChart>
              </ResponsiveContainer>
            </Grid>
        </Grid> */}
    </Paper>
      
    );
  }
} 
