import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';

import Collapse from '@material-ui/core/Collapse';
import Grid from '@material-ui/core/Grid';
import blue from '@material-ui/core/colors/blue';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Popover from '@material-ui/core/Popover';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import GroupAdd from '@material-ui/icons/GroupAdd'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import moment from 'moment'

import Checkbox from '@material-ui/core/Checkbox';
import Edit from '@material-ui/icons/Edit';
import DeleteForever from '@material-ui/icons/DeleteForever';
import {success, warning, info, error} from '../common/Colors'
import { action } from 'mobx';

import {PermissibleRender} from '@brainhubeu/react-permissible';

import PriceQuoteDetail from './PriceQuoteDetail'
import PriceRequestNew from './PriceRequestNew'



const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
    // width: theme.breakpoints.values.sm,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
  },
  paddingTop: {
    paddingTop: theme.spacing.unit*2,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  listitemBackColor: {
    backgroundColor: blue[100],
  },
  title: {
    padding: '5px',
    marginRight: '10px',
    fontSize: '16px',
  },
  label: {
    padding: '4px',
    paddingLeft: '8px',
    paddingRight: '8px',
    marginRight: '8px',
    marginTop: '5px',
    borderRadius: 6,
    fontSize: '12px',
    textDecoration: 'None',
  },
  label2: {
    padding: '10px 8px',
    marginRight: '8px',
    marginTop: '5px',
    borderRadius: 6,
    fontSize: '12px',
    textDecoration: 'None',
  },
  colorInfo: {
    backgroundColor: info,
  },
  colorSuccess: {
    backgroundColor: success,
  },
  colorWarning: {
    backgroundColor: warning,
  },
  colorError: {
    backgroundColor: error,
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8,
    },
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
});

@withStyles(styles)
@inject('priceRequestStore', 'helperStore', 'userStore', 'authStore')
@withRouter
@observer
export default class PriceRequestDetail extends Component {
  state = {
    request: this.props.request||{"name":"", "description":"",  "currency":"", "finallPrice":"", "major":""},
    requestValidate:  {"name":"",  "currency":"", "finallPrice":"", "major":""},
    checkedCounsellors: [],
    expanded: false,
    anchorEl: null,
    currency: 1,
    mode: 'view',
    finalPrice: 0,
    finalCurrency: 0,
    dialogOpen: false
  };
  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
  };

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  handleDialogOpen = () => {
    this.setState({ dialogOpen: true });
  }

  handleDialogClose = () => {
    this.setState({ dialogOpen: false });
  }

  handleCurrencyChange = (event) => {
    this.setState({
      currency: event.target.value,
    });
  };

  handleInputChange = (event) => {
    console.log(event.target)
    let updatingOrderValidation = this.state.requestValidate
    if(event.target.value.trim().length===0){
      updatingOrderValidation[event.target.id] = event.target.id + ' is required'
    }else{
      updatingOrderValidation[event.target.id] = "" 
    }
    let updatingOrder = this.state.request
    updatingOrder[event.target.id] = event.target.value
    this.setState({
      request: updatingOrder,
      requestValidate: updatingOrderValidation
    })
  }

  handleFinalQuoteChange = (event) => {
    const {id, value} = event.target
    this.setState({[id]: value})
  }

  handleFilesUpload = (event) => {
    const files = Array.from(event.target.files)
    const formData = new FormData()
    files.forEach((file) => {
      formData.append('file', file)
    })
    this.props.helperStore.uploadFiles(formData).then(res =>{
      let orginalOrder = this.state.request
      orginalOrder['fileUrl'] = res
      this.setState({request: orginalOrder})
    })
  }

  validateField(){
    let updatingrequestValidate = this.state.requestValidate
    let validateStatus = false
    for(let key in updatingrequestValidate){
      console.log(this.state.request[key], key)
      if((''+this.state.request[key]).trim().length===0){
        updatingrequestValidate[key] = key + ' is required'
        validateStatus = true
      }
    }
    this.setState({
      requestValidate: updatingrequestValidate
    })
    console.log()
    return validateStatus
  }
  utcDatetime(dt, tz){
    // console.log(dt, tz)
    return moment.tz(dt,tz).format()
  }

  handleCreatePriceRequest = () => {
    if(!this.validateField()){
      let order = Object.assign({},this.state.request)
      if(typeof(order.fileUrl)=='object') order.fileUrl = JSON.stringify(order.fileUrl)
      if(!order.idpriceRequest) order.idUser = this.props.authStore.currentUser.iduser
      if(!order.idpriceRequest) order.status='confirming'  //confirming - requesting - complete
      if(!order.idpriceRequest) order.dateTime=moment.tz().format('YYYY-MM-DDTHH:mm:ss')
      this.props.priceRequestStore.updatePriceRequest(order).then(res =>{
        if(res.statusCode == 200){
          if(this.props.cancleFunction) this.props.cancleFunction()
          // this.props.history.push(`/packages/${res.insertId}`)
          this.props.history.goBack()
        }else{
          this.setState({
            error: JSON.stringify(res)
          })
        }
      })
      this.setState({mode:'view'})
    }
  }

  generateFileLink(filesObj){
    let files = []
    let filesArray = filesObj
    if (typeof(filesObj) === 'string'){
      filesArray = JSON.parse(filesObj)
    }
    filesArray = filesArray||[]
    for(let i=0; i<filesArray.length; i++){
      let file = filesArray[i]
      const size = file.size/1024 >= 1024? Math.round(file.size/(1024*1024)*100)/100 + ' MB': Math.round(file.size/1024*100)/100+' KB'
      files.push(<li key={i}><a href={this.props.helperStore.ROOT_API_URL + '/img/' + file.filename} key={i}>{file.originalname} - <span className={this.props.classes.span}>{size}</span></a></li>)
    }
    return files
  }
  handleViewChange(view){
    this.setState({mode:view})
  }
  handelCancel = () => {
    this.setState({
      mode:'view',
    })
  }
 
  handleCheckCounsellor(user){
    let counsellors = this.state.checkedCounsellors 
    if(this.state.checkedCounsellors.indexOf(user)===-1) {
      counsellors.push(user)
      this.setState({checkedCounsellors: counsellors})
    }
    else {
      counsellors = counsellors.filter(counsellor => counsellor !== user)
      this.setState({checkedCounsellors: counsellors})
    }
  }

  handleQuote = () => {
    if(this.state.checkedCounsellors.length===0) return alert('Counsellor(s) should be choosed')
    for(let checkedCounsellor of this.state.checkedCounsellors){
      let quote = { "user_iduser": checkedCounsellor.iduser, "priceRequest_idpriceRequest": this.state.request.idpriceRequest, "status":'requesting'}
      this.props.priceRequestStore.updatePriceQuote(quote)
        .finally(action(() => {
          let checkedCounsellors = this.state.checkedCounsellors
          checkedCounsellors = checkedCounsellors.filter(counsellor => counsellor !== checkedCounsellor)
          this.setState({checkedCounsellors: checkedCounsellors}) 
        }))
    }
    this.handleCounsellorsClose()
  }

  handleCounsellorsClick = event => {
    this.setState({
      anchorEl: event.currentTarget,
    });
  };

  handleCounsellorsClose = () => {
    this.setState({
      anchorEl: null,
    });
  };

  handleApplyClick = event => {
    this.setState({
      anchorEll: event.currentTarget,
    });
  }

  handleApplyClose = () => {
    this.setState({
      anchorEll: null,
    });
  };
  findUser(quotes, iduser){
    for(let q of quotes){
      if(q.user_iduser === iduser) return true
    }
    return false
  }

render(){
  const { classes, helperStore, priceRequestStore, userStore, authStore } = this.props;
  const { anchorEl, anchorEll, mode, request } = this.state;
  let quotes = priceRequestStore.getQuotesByIdpriceRequest(request.idpriceRequest)
  const open = Boolean(anchorEl);

  if(mode=='edit') return <PriceRequestNew request={request} cancelFuntion={this.handelCancel}/>

  return(
    <div className={classes.root}>
      <Grid container xs={12} key={request.idpriceRequest}>
        <Grid container xs={12} justify='space-between'>
          <Grid item >
            <Typography variant="subtitle" gutterBottom >
              <span className={classes.title}>
                {request.idpriceRequest + '. '+ request.name}
              </span>
              <span className={[classes.label, classes.colorSuccess].join(' ')}>
                {request.major}
              </span>
              <span className={[classes.label, classes.colorInfo].join(' ')}>
                {request.status}
              </span>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'saleman']} oneperm>
                { request.finallPrice ? 
                  <span className={[classes.label, classes.colorError].join(' ')}>
                    {helperStore.getCurrency(request.currency) ? helperStore.getCurrency(request.currency).currency+':' : '' }{ request.finallPrice?request.finallPrice:''}
                  </span>
                  : ''
                }
              </PermissibleRender>
            </Typography>
            <Typography vcolor="textSecondary" variant="caption"  gutterBottom>
              Create Date: {request.dateTime}, By {request.idUser + '-' + userStore.getUserById(request.idUser).name}
            </Typography>
          </Grid>
          <Grid item  justify="flex-end">
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
                <IconButton onClick={this.handleCounsellorsClick}>
                  <GroupAdd />
                </IconButton>
              </PermissibleRender>
              <IconButton onClick={this.handleDialogOpen}>
                <DeleteForever />
              </IconButton>
              <IconButton onClick={() => this.handleViewChange('edit')}>
                <Edit />
              </IconButton>
              <IconButton
                className={classnames(classes.expand, {
                  [classes.expandOpen]: this.state.expanded,
                })}
                onClick={this.handleExpandClick}
                aria-expanded={this.state.expanded}
                aria-label="Show more"
              >
                <ExpandMoreIcon />
            </IconButton>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Collapse in={this.state.expanded} timeout="auto" className={classes.fullWidth}>
            {request.description}
          </Collapse>
        </Grid>
        <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
          <Grid item xs={12}>          
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>No.</TableCell>
                  <TableCell>Counsellor</TableCell>
                  <TableCell>Labels</TableCell>
                  <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'counsellor']} oneperm>
                    <TableCell>Quote Price</TableCell>
                  </PermissibleRender>
                  <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'saleman']} oneperm>
                    <TableCell>Finnal Price</TableCell>
                  </PermissibleRender>
                  <TableCell>Status</TableCell>
                  <TableCell>Operation</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
              {quotes.map((quote, index) => {
                let counsellor = this.props.userStore.getUserById(quote.user_iduser)
                return (<PriceQuoteDetail counsellor={counsellor} quote={quote} request={request} key={index} index={index} />)
              })}
              </TableBody>
            </Table>
          </Grid>
        </PermissibleRender>
      </Grid>

      <Popover
        id="select counselor"
        open={open}
        anchorEl={anchorEl}
        onClose={this.handleCounsellorsClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <List>
          {userStore.getCounsellorsWithMajor(request.major)
            .filter(counsellor => !this.findUser(quotes, counsellor.iduser))
            .map((counsellor, index) => {
            return (
              <ListItem button key={counsellor.iduser}>
                <Checkbox
                  checked={this.state.checkedCounsellors.indexOf(counsellor) !== -1}
                  tabIndex={-1}
                  disableRipple
                  onClick={() => this.handleCheckCounsellor(counsellor)}
                />
                <ListItemText primary={counsellor.name} secondary={counsellor.labels?counsellor.labels.split('|').join(','):''}/>
              </ListItem>
            )
          })}
        </List>
        <Button onClick={this.handleQuote} className={classes.button} color="secondary">
          Request for Quote
        </Button>
      </Popover>

      <Dialog
          open={this.state.dialogOpen}
          onClose={this.handleDialogClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{ "Are you sure to delete?" }</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are your sure to delete {request.idpriceRequest + '. ' + request.name } ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleDialogClose} color="primary">
            Cancle
          </Button>
          <Button onClick={this.handlePriceRequestDelete} color="primary" autoFocus>
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </div>
    );
  }
} 

