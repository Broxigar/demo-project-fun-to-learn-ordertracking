import React from 'react';
import { List, message, Avatar, Spin, Button, Skeleton, Badge,Row, Col } from 'antd';
import '../index.css';
import {bggray, gray} from './Colors'
import {inject, observer} from "mobx-react";
import {withRouter} from "react-router-dom";
import config from '../config'
const ROOT_URL = config.ROOT_URL


//const fakeDataUrl = 'https://randomuser.me/api/?results=5&inc=name,gender,email,nat&noinfo';

const count = 5;

@inject('helperStore', 'authStore')
@withRouter
@observer
export default class NotificationList extends React.Component {
    state = {
        data: [],
        list:[],
        num: 0,
        loading: false,
        initLoading: true

    };

    componentDidMount() {
        let temp = this.fetchData(0)
        let num = this.state.num+1
        this.setState({
            data: temp,
            list: temp,
            num: num,
            initLoading: false,
        });
    }

    fetchData = (i) => {
        console.log(i)
        let iniData= []
        if( this.props.authStore.notifications.size>5){
            iniData = Array.from(this.props.authStore.notifications.values()).slice(i*5, 5+5*i)
        }
        else iniData = Array.from(this.props.authStore.notifications.values())

        return iniData
    };

    // handleInfiniteOnLoad = () => {
    //     let { data } = this.state;
    //     this.setState({
    //         loading: true,
    //     });
    //     if (data.length > this.props.authStore.notifications.values().length) {
    //         message.warning('Loaded all');
    //         this.setState({
    //             hasMore: false,
    //             loading: false,
    //         });
    //         return;
    //     }
    //     let temp = this.fetchData(this.state.num)
    //     data = data.concat(temp);
    //     this.setState(prevState =>({
    //         data,
    //         loading: false,
    //         num: prevState+1,
    //     }));
    // };

    onLoadMore = () => {
        console.log(this.state.list.length)
        console.log(this.props.authStore.notifications.size)
        if (this.state.list.length< this.props.authStore.notifications.size){
            this.setState({
                loading: true,
                list: this.state.data.concat([...new Array(count)].map(() => ({ loading: true }))),
            });

            let temp = this.fetchData(this.state.num)

            const data = this.state.data.concat(temp);
            let num = this.state.num+1
            this.setState(
                {
                    data,
                    list: data,
                    loading: false,
                    num: num
                },
            );
            window.dispatchEvent(new Event('resize'));
        }
        else message.warning("No more to load.")

    };

    render() {
        const { initLoading, loading, list } = this.state;
        const { authStore } = this.props
        const loadMore =
            !initLoading && !loading ? (
                <div
                    style={{
                        textAlign: 'center',
                        marginTop: 12,
                        height: 32,
                        lineHeight: '32px',
                    }}
                >
                    <Button onClick={this.onLoadMore}>loading more</Button>
                </div>
            ) : null;
        const ORDER = authStore.currentUser.role.indexOf("admin")>0? 'order':'orders'


        return (
            <List
                className="demo-loadMore-list"
                loading={initLoading}
                itemLayout="horizontal"
                loadMore={loadMore}
                dataSource={list}
                style={{
                    overflow: 'auto',
                    padding: '20px',
                    height:'flex',
                    minWidth: '400px',
                    maxWidth: '700px'
                }}
                renderItem={item => (
                    <List.Item
                        // style={{
                        //     backgroundColor: item.status ===0? null:gray
                        // }}
                        // actions={[<a key="list-loadmore-edit">edit</a>, <a key="list-loadmore-more">more</a>]}
                    >
                        <Skeleton avatar title={false} loading={item.loading} active>
                            <List.Item.Meta

                                avatar={
                                    item.status===0?
                                        <Badge
                                            dot
                                            offset={[-45,0]}
                                        >
                                            <Avatar style={{ backgroundColor: '#f56a00', verticalAlign: 'middle' }} size="large">
                                                Admin
                                            </Avatar>
                                        </Badge>:
                                        <Avatar style={{ backgroundColor: '#f56a00', verticalAlign: 'middle' }} size="large">
                                            Admin
                                        </Avatar>
                                }
                                //{item.orderID}
                                title={


                                    <Row>
                                        <a href={`${ROOT_URL}/#/${ORDER}/${item.orderID}`}>
                                        <Col span={8}><Badge>
                                            {`Order ID: ${item.orderID}`}

                                        </Badge>
                                        </Col>
                                        </a>
                                        <Col span={8} offset={8}>
                                            {item.dateTime.split('T')[0]}
                                        </Col>
                                    </Row>

                                }

                                description={
                                    authStore.notifiTypes[item.type-1]
                                }
                            />

                        </Skeleton>
                    </List.Item>
                )}
            />
        );
    }
}

//         return (
//             <div className="demo-infinite-container">
//                 <InfiniteScroll
//                     initialLoad={false}
//                     pageStart={0}
//                     loadMore={this.handleInfiniteOnLoad}
//                     hasMore={!this.state.loading && this.state.hasMore}
//                     useWindow={false}
//                 >
//                     <List
//                         dataSource={this.state.data}
//                         renderItem={item => (
//                             <List.Item key={item.idmessageQueue}>
//                                 <List.Item.Meta
//                                     avatar={
//                                         <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
//                                     }
//                                     title="12313131312313"
//                                     description="adsfasdfasf"
//                                 />
//                                 <div>Content</div>
//                             </List.Item>
//                         )}
//                     >
//                         {this.state.loading && this.state.hasMore && (
//                             <div className="demo-loading-container">
//                                 <Spin />
//                             </div>
//                         )}
//                     </List>
//                 </InfiniteScroll>
//             </div>
//         );
//     }
// }
