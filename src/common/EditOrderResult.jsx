import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { Spin, List, Input, Button } from 'antd';
import { GenerateFileLinkEdit} from '../util/generateFileLink'
import {info, bginfo} from './Colors'
const { TextArea } = Input;

@inject('userStore', 'orderResultStore')
@withRouter
@observer
export default class EditOrderResult extends Component{
  handleUpdateUserFile = newFileUrl => {
    this.props.orderResultStore.updateFiles(newFileUrl)
  }
  
  handleUpdateUserFile = newFileUrl => {
    this.props.orderResultStore.updateFiles(newFileUrl)
  }

  render(){
    let { orderResultStore, userStore} = this.props;
    let user = orderResultStore.iduser ? userStore.getUserById(orderResultStore.iduser) : {}

    return(
      <Spin spinning={orderResultStore.isLoading}>
        <List.Item style={{padding: '10px', background: info}}>
          <List.Item.Meta
            title={`${orderResultStore.datetime}: ${user.name}(${user.role})`}
            description={
              <div>
                <TextArea rows={12} onChange={(e) => orderResultStore.updateText(e.target.value)} value={orderResultStore.description||`
Please answer following question

How much time you generally spent on the following items?
Supporting documents (PPT/other course-material): 
Marking criteria: 
Brainstorming: 
Actual writing process: 

Please comments on difficulties you encountered during writing.

During your writing process, which aspect(s) you consider as the most time-consuming one?
      `}/>
              <Input
                  id="attachement"
                  label="Score Screen Shot"
                  type="file"
                  multiple
                  onChange = {(e)=>orderResultStore.uploadFiles(e.target.files)}
                />
                <GenerateFileLinkEdit files={orderResultStore.files} updateFileUrls = {this.handleUpdateUserFile} />
                <hr/>
                <Button onClick={this.props.cancel}>
                  Cancle
                </Button>
                &nbsp;
                <Button  onClick={() => {
                  orderResultStore.sumitOrderResult()
                  this.props.cancel()
                  }}>
                  Save
                </Button>
              </div>
            }
          />
        
        </List.Item>
      </Spin>
    );
  }
} 
