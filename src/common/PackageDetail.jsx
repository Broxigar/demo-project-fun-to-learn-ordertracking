import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import List from '@material-ui/core/List';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import Collapse from '@material-ui/core/Collapse';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import ListSubheader from '@material-ui/core/ListSubheader';

import Edit from '@material-ui/icons/Edit';
import Add from '@material-ui/icons/Add';
import FolderOpen from '@material-ui/icons/FolderOpen';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import moment from 'moment'
import momenttz from 'moment-timezone'
import classnames from 'classnames';
import {PermissibleRender} from '@brainhubeu/react-permissible';

import {success, gray, info, warning, error, bginfo, bgwarning, bgerror, bgsuccess, bgLightBlue} from './Colors'
import NewPackage from './NewPackage'
import OrderDetail from './OrderDetail'
import NewOrder from './NewOrder'
import NewPackageOrder from './NewPackageOrder'
import SettingsOverscan from '@material-ui/icons/SettingsOverscan';
import GridListTileBar from "@material-ui/core/GridListTileBar";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import {confirmPackagePayment} from "../util/updateOrder";


const styles = theme => ({
  root: {
    // padding: theme.spacing.unit * 2,
    textAlign: 'left',
    backgroundColor: theme.palette.background.paper,
    marginTop: theme.spacing.unit,
    paddingTop: theme.spacing.unit ,
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 2,
  },
  padding:{
    padding: theme.spacing.unit,
  },
  paddingLeft:{
    paddingLeft: theme.spacing.unit * 2,
  },
  // label:{
  //   marginRight: theme.spacing.unit *1,
  //   backgroundColor: warning,
  // },
  Extremely: {
    backgroundColor: error,
  },
  Important: {
    backgroundColor: warning,
  },
  Normal: {
    backgroundColor: info,
  }, 
  Success: {
    backgroundColor: success,
  },
  Note: {

  },
  button: {
    margin: theme.spacing.unit,
  },
  info:{
    backgroundColor: info,
  },
  bgerror:{
    backgroundColor: bgerror,
  },
  bgwarning:{
    backgroundColor: bgwarning,
  },
  bginfo:{
    backgroundColor: bginfo,
    padding: theme.spacing.unit, 
    borderRadius: '3px'
  },
  bgsuccess: {
    backgroundColor: bgsuccess,
  },
  normal: {
    backgroundColor: bgLightBlue,
  },
  errorText: {
    color: error, 
    fontSize: '14px',
    textDecoration: 'underline',
  },
  label:{
    padding: '3px',
    paddingLeft: '5px',
    paddingRight: '5px',
    marginRight: '5px',
    marginTop: '4px',
    borderRadius: 3,
    fontSize: '12px',
    color: gray, 
    textDecoration: 'None',
    // backgroundColor: warning,
  },
  list:{
    color: gray,
    padding: '3px',
    fontSize: '12px',
    textDecoration: 'underline',
  },

  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8,
    },
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  img: {
    padding: 5,
    paddingBottom: 8,
    width: theme.breakpoints.values.lg/4,
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
  span: {
    fontSize: 10,
    color: gray,
  },
  fullWidth: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit,
    border: 2,
    width: '100%',
  }, 


});

@withStyles(styles)
@inject('ordersStore', 'helperStore', 'authStore', 'userStore')
@withRouter
@observer
export default class PackageDetail extends Component{
  state = {
    checked: this.props.ordersStore.packageCheckedCheck(this.props.match.params.idorderPackage||this.props.packageOrder.idorderPackage),
    expand: false,
    orderExpand: true,
    mode: 'view',
    fileClass: {},
    fileDisplayOpen: false,
    activeFile: {},
  };

  handleCheck = () => {
    const idorderPackage =  this.props.packageOrder?this.props.packageOrder.idorderPackage:this.props.match.params.idorderPackage
    let orders = this.props.ordersStore.getOrdersByOrderPackageId(idorderPackage)
    console.log(orders)
    for(let order of orders){
      if(this.props.ordersStore.checked(order.idorder)){
        this.props.ordersStore.delOrdersChecked(order.idorder)
      } else {
        this.props.ordersStore.putOrdersChecked(order.idorder)
      }
    }
    if(this.state.checked){
      this.props.ordersStore.delPackageChecked(idorderPackage)
    }else{
      this.props.ordersStore.putPackageChecked(idorderPackage)
    }
    this.setState({
      checked: !this.state.checked,
    });
  };

  handleExpandClick = () => {
    this.setState(state => ({ expand: !state.expand }));
  };
  handleOrderExpandClick = () => {
    this.setState(state => ({ orderExpand: !state.orderExpand }));
  };
  handlePackageOrderEdit = () => {
    this.setState({mode:"edit"})
  }
  handlePackageOrderAdd = () => {
    this.setState({mode:"add"})
  }
  handleCancleEdit = () => {
    this.setState({mode:"view"})
  }
  goBack = () => {
    this.setState({mode:"view"})
  }

  handleAcceptorder = () => {
    const idorderPackage =  this.props.packageOrder?this.props.packageOrder.idorderPackage:this.props.match.params.idorderPackage
    let orders = this.props.ordersStore.getOrdersByOrderPackageId(idorderPackage)

    for(let order of orders){
      let orderObj = {} 
      let fieldStatus = {}
      let fieldRole = {}
      let fieldIduser = {}
      orderObj['idorder'] = order.idorder
      fieldStatus['key'] = 'status'
      fieldStatus['value'] = 'processing'
      fieldRole['key'] = 'currentProcessorRole'
      fieldRole['value'] = 'writer'
      fieldIduser['key'] = 'currentProcessor_iduser'
      fieldIduser['value'] = this.props.authStore.currentUser.iduser

      orderObj['fields'] = [fieldStatus, fieldRole, fieldIduser]

      this.props.ordersStore.updateOrder(orderObj).then(orderRes =>{
        console.log(orderRes)
        if(orderRes.statusCode === 200){
          let comment = {}
          comment['dateTime'] = moment.tz().format('YYYY-MM-DDTHH:mm:ss')
          comment['content'] = `${this.props.authStore.currentUser.name}(${this.props.authStore.currentUser.iduser}) accepted this order ${order.name} (${order.idorder}).`
          comment['order_idorder'] = order.idorder
          comment['commentBy'] = this.props.authStore.currentUser.iduser
          comment['status'] = 1
          this.props.ordersStore.updateOrderComment(comment)
        }
      })
      if(order.orderPackage_idorderPackage){
        this.props.ordersStore.updatePackageStatus(order.orderPackage_idorderPackage, 'processing')
      }
    }
  }

  generateFileLink(filesObj){
    let files = []
    let filesArray = filesObj
    if(typeof(filesObj)=='string') filesArray = JSON.parse(filesObj)
    for(let i=0; i<filesArray.length; i++){
      let file = filesArray[i]
      const size = file.size/1024 >= 1024? Math.round(file.size/(1024*1024)*100)/100 + ' MB': Math.round(file.size/1024*100)/100+' KB'
      files.push(<li key={i}><a href={this.props.helperStore.ROOT_API_URL + '/img/' + file.filename} key={i}>{file.originalname} - <span className={this.props.classes.span}>{size}</span></a></li>)
    }
    return files
  }
  generatePaymentImg(filesObj){
    let files = []
    let filesArray = filesObj
    if(typeof(filesArray) == 'string') filesArray = JSON.parse(filesObj)
    filesArray = filesArray||[]
    // console.log(filesArray)
    for(let i=0; i<filesArray.length; i++){
      let file = filesArray[i]
      let fileClass = typeof(file.fileclass) === 'string'? JSON.parse(file.fileclass) : file.fileclass
      // const size = file.size/1024 >= 1024? Math.round(file.size/(1024*1024)*100)/100 + ' MB': Math.round(file.size/1024*100)/100+' KB'
      files.push(
        <GridListTile key={i}>
          <img src={this.props.helperStore.ROOT_API_URL + '/img/' +file.filename} alt={file.filename} />
          <GridListTileBar
              title={fileClass.paymentCurrency + ': ' + fileClass.paymentMount}
              subtitle={<span>{fileClass.paymentType}</span>}
              actionIcon={
                <IconButton className={this.props.classes.icon} onClick={() => this.handlePaymentImgDislpay(file)}>
                  <SettingsOverscan />
                </IconButton>
              }
          />
        </GridListTile>)
    }
    return files
  }

  handlePaymentImgDislpay(file){
    let fileClass = typeof(file.fileclass) === 'string'? JSON.parse(file.fileclass) : file.fileclass

    this.setState({
      activeFile: file,
      fileClass: fileClass,
      fileDisplayOpen: true,
    })
  }

  handlePaymentImgDisplayCLose = () => {
    this.setState({
      activeFile: {},
      fileClass: {},
      fileDisplayOpen: false,
    })
  }

  handleConfirmPayment = () =>{
    if (window.confirm('Are you sure to Confirm the payment (Bound, Part or Full Payment)?')) {
      confirmPackagePayment(this.props.packageOrder)
    }
  }
  generateLabels(labelsObj){
    let labels = []
    let labelArray = JSON.parse(labelsObj)
    for(let i=0; i<labelArray.length; i++){
      let labelobj = labelArray[i]
      labels.push(<span className={[this.props.classes.label, this.props.classes[labelobj.level]].join(' ')} key={i} >{labelobj.content}</span>)
    }
    return labels
  }
  render(){
    let { classes, match, ordersStore, packageOrder, helperStore, authStore, userStore} = this.props;
    const { checked, mode} = this.state;
    if(ordersStore.isLoading) return(<Dialog open={true}> <CircularProgress /> </Dialog>)
    // console.log(packageOrder, this.props)
    if(!packageOrder) packageOrder = ordersStore.getOrderPackageById(match.params.idorderPackage)
    if(!packageOrder) return(<Dialog open={true}> <CircularProgress /> </Dialog>)
    console.log(packageOrder)
    console.log(match.params.idorderPackage)
    let orders = ordersStore.getOrdersByOrderPackageId(packageOrder?packageOrder.idorderPackage:match.params.idorderPackage)
     console.log(orders)

    let timezone = momenttz.tz.guess()
    let isoCreateDate = moment(packageOrder.dateTime, moment.ISO_8601).tz(timezone)
    let localCreatedate = isoCreateDate.format("YYYY-MM-DD HH:mm")

    if(mode === 'edit'){
      return <NewPackage order={packageOrder} formName="Modify Package Order" cancleFunction={this.handleCancleEdit} />
    }
    if(mode === 'add'){
      console.log(packageOrder)
      //return <NewOrder formName={'Add a New for Package  '+ packageOrder.name} goBack={this.goBack} idPackage = {packageOrder.idorderPackage} />
      return <NewPackageOrder formName={'Add a New for Package  '+ packageOrder.name} goBack={this.goBack} packageInfo = {packageOrder} />
    }
      
    return(
      <div className={[classes.root, classes.normal].join(' ')} key={packageOrder.idorderPackage}>
      <Grid container item xs={12}   >
        <Grid container item xs={12}>
          <Grid  item lg={9} sm={12}>
            <Typography variant="subheading" gutterBottom >
              <FolderOpen /> &nbsp; No. {packageOrder.idorderPackage}. 
              {packageOrder.name } &nbsp;
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'saleman']} oneperm>
                {packageOrder.salePrice?
                    <span className={[classes.label,classes.Success].join(' ')}>
                    {"Price: " + (helperStore.getCurrency(packageOrder.currency_sale) ? helperStore.getCurrency(packageOrder.currency_sale).currency : 'Null Currency') + ':' + packageOrder.salePrice }
                  </span>
                    :null
                }
              </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'qa', 'account', 'translater', 'exchanger']} oneperm>
                {packageOrder.paymentStatus && packageOrder.paymentVerify?
                    <span>
                  <span className={[classes.label, packageOrder.paymentStatus==='full paid'?classes.Success:classes.Important].join(' ')}>
                    { packageOrder.paymentStatus }</span>
                  <span className={[classes.label, packageOrder.paymentVerify==='verifying'?classes.Important:classes.Success].join(' ')}>
                    {packageOrder.paymentVerify}
                  </span>
                </span>
                    :null
                }

              </PermissibleRender>
              {/*<PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'saleman', 'qa', 'account', 'translater', 'exchanger']} oneperm>*/}
              {/*  <span> */}
              {/*    <span className={[classes.label, packageOrder.paymentStatus==='full paid'?classes.Success:classes.Important].join(' ')}>*/}
              {/*      {helperStore.getCurrency(packageOrder.currency_sale).currency + ':' + packageOrder.salePrice + ' '+ packageOrder.paymentStatus}</span>*/}
              {/*    { packageOrder.paymentVerify ? <span className={[classes.label, packageOrder.paymentVerify==='verifying'?classes.Important:classes.Success].join(' ')}>*/}
              {/*        {packageOrder.paymentVerify}*/}
              {/*      </span> : ''*/}
              {/*    }*/}
              {/*  </span>*/}
              {/*</PermissibleRender>*/}
              &nbsp; 
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'qa', 'account', 'translater', 'exchanger']} oneperm>
                {/*<span className={[classes.label, packageOrder.counselorPayment?classes.Success:classes.Important].join(' ')}>*/}
                {/*  Counseor {packageOrder.currency_cost ? helperStore.getCurrency(packageOrder.currency_cost).currency + ':' + packageOrder.cost : ''} {packageOrder.counselorPayment?'paid':'unpaid'}*/}
                {/*</span>*/}
              </PermissibleRender>
            </Typography>
            <Typography vcolor="textSecondary" variant="caption"  gutterBottom>
              Create Date: {localCreatedate}
              By {userStore.getUserById(packageOrder.user_iduser).name}
            </Typography>
          </Grid>
          <Grid container item lg={3} sm={12}  justify={"flex-end"}>
            <Grid>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'qa', 'saleman', 'account', 'translater', 'exchanger']} oneperm>
                <React.Fragment>
                  <IconButton>
                    <Add
                      onClick={this.handlePackageOrderAdd}
                    />
                  </IconButton>
                  <IconButton>
                    <Edit
                      onClick={this.handlePackageOrderEdit}
                    />
                  </IconButton>
                  {/*<Checkbox*/}
                  {/*  onChange={this.handleCheck}*/}
                  {/*  checked={checked}*/}
                  {/*/>*/}
                </React.Fragment>
              </PermissibleRender>
              <IconButton
                className={classnames(classes.expand, {
                  [classes.expandOpen]: this.state.expand,
                })}
                onClick={this.handleExpandClick}
                aria-expanded={this.state.expand}
                aria-label="Show more"
                >
                  <ExpandMoreIcon />
              </IconButton>
            </Grid>
          </Grid>
        </Grid>
        
        <Grid container item xs={12}>
          <Collapse in={this.state.expand} timeout="auto" className={classes.fullWidth}>
            <Grid item xs={12}>
              <Typography  variant="body1"  gutterBottom>{packageOrder.description}</Typography>
            </Grid>
            <Grid container item xs={12}>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'qa', 'saleman', 'account', 'translater', 'exchanger']} oneperm>
                <GridList cellHeight={180}>
                  <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
                    <ListSubheader component="div">Payment Images:</ListSubheader>
                  </GridListTile>
                  {this.generatePaymentImg(packageOrder.paymentImg)}
                </GridList>
              </PermissibleRender>
              <Grid item xs={12}>
                <List>
                  <Typography  variant="subheading"  gutterBottom> Supported Documents:</Typography>
                  {this.generateFileLink(packageOrder.fileUrl)}
                </List>
              </Grid>
            </Grid>
          </Collapse> 
        </Grid>
        <Grid container item xs={12}>
          {orders.length == 0? 
            <Grid>This package has None order, you can add on here:  
              <Button onClick={this.handlePackageOrderAdd} color="primary">
                Add Order
              </Button>
            </Grid>
            :
            <Grid container item  className={classes.paddingLeft} spacing={16} xs={12}>
              <Typography  variant="subheading"  gutterBottom> Sub orders:</Typography>
              {orders.map((row, index) => {
                  return <OrderDetail order={row} key={index} />
              })}
            </Grid>
          }
        </Grid>  
        
        {/* <Grid container item xs={12} className={classes.paddingLeft}>
          <Collapse in={this.state.orderExpand} timeout="auto" className={classes.fullWidth}>
            <Grid container item spacing={16} xs={12}>
              {orders.map((row, index) => {
                  return <OrderDetail order={row} key={index} />
              })}
            </Grid>
          </Collapse>
        </Grid> */}
        <Grid item xs={12} className={classes.padding}>
          <Divider/>
        </Grid>
        <Dialog
            fullWidth={true}
            maxWidth={'md'}
            open={this.state.fileDisplayOpen}
            onClose={this.handlePaymentImgDisplayCLose}
        >
          <DialogTitle id="max-width-dialog-title"> {this.state.fileClass.paymentType + ' ' + this.state.fileClass.paymentCurrency + ': ' + this.state.fileClass.paymentMount}</DialogTitle>
          <DialogContent>
            <img className={classes.image} src={this.props.helperStore.ROOT_API_URL + '/img/' +this.state.activeFile.filename} alt={this.state.activeFile.filename} />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handlePaymentImgDisplayCLose} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
        
        <Grid container item xs={12} className={classes.padding}>
          <Grid item xs={9} >
            {this.generateLabels(packageOrder.labels)}
          </Grid>
          <Grid item xs={3} justify={"flex-end"}>
            <Grid>
            <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['counsellor']}>
                {
                  packageOrder.status==='waiting writer' ? 
                  <Button  size="small"  variant="contained"  color="secondary" className={classes.button}  
                    onClick={this.handleAcceptorder}>
                    Accept this Package Order
                  </Button>
                  : ''
                }
            </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
                {
                  packageOrder.paymentVerify==='verifying' ?
                      <Button  size="small"   className={classes.button}
                               onClick={this.handleConfirmPayment}>
                        Confirm Payment
                      </Button>:''
                }
              </PermissibleRender>
            {/*<span className={[classes.label, classes.Normal].join(' ')}>*/}
            {/*  Status: {packageOrder.status}*/}
            {/*</span> */}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      </div>
      );
    }
} 

