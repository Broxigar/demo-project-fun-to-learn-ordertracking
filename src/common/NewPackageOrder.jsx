import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import momenttz from 'moment-timezone'

import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import IconButton from '@material-ui/core/IconButton';
import ArrowBack from '@material-ui/icons/ArrowBack';

import Button from '@material-ui/core/Button';
import blue from '@material-ui/core/colors/blue';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import Typography from '@material-ui/core/Typography';

import ListSubheader from '@material-ui/core/ListSubheader';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

import Cancel from '@material-ui/icons/Cancel'

import './react-datetime.css'
import {success,gray, info, warning, error, bginfo, bgwarning, bgerror} from './Colors'
import loading from './loading'
import { AutoCompleteCourse, AutoCompleteCustomer } from './AutoComplete'
import { GenerateLabelEdit } from '../util/generateLabel';
import { GenerateFileLinkEdit } from '../util/generateFileLink';
import { Spin, message } from 'antd'
import agent from "../agent";

message.config({
    top: 100,
    duration: 2,
    maxCount: 3,
});


const level = [
    "Extremely",
    "Important",
    "Normal",
    "Note"
]

const styles = theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing.unit *2
    },
    button: {
        margin: theme.spacing.unit,
    },
    padding: {
        padding: theme.spacing.unit * 2
    },
    input: {
        display: 'none',
    },
    marginTop: {
        marginTop: theme.spacing.unit * 2
    },
    listitemBackColor: {
        backgroundColor: blue[100],

    },
    Extremely: {
        backgroundColor: error,
    },
    Important: {
        backgroundColor: warning,
    },
    Normal: {
        backgroundColor: info,
    },
    bgerror: {
        backgroundColor: bgerror,
    },
    margin: {
        marginTop: theme.spacing.unit * 4,
        margin: theme.spacing.unit,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        marginTop: theme.spacing.unit * 2,
        // width: theme.breakpoints.values.sm,
    },
    label:{
        padding: '3px',
        paddingLeft: '5px',
        paddingRight: '5px',
        marginRight: '5px',
        marginTop: '4px',
        borderRadius: 3,
        fontSize: '12px',
        color: gray,
        textDecoration: 'None',
    },
    list:{
        color: gray,
        padding: '3px',
        fontSize: '14px',
        textDecoration: 'underline',
    },
    grayText: {
        color: gray,
    },
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
        marginLeft: 'auto',
        [theme.breakpoints.up('sm')]: {
            marginRight: -8,
        },
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    img: {

        padding: 5,
        paddingBottom: 8,
        width: 280,
    },
    span: {
        fontSize: '14px',
        color: gray,
        paddingLeft: '5px'
    },
    fullWidth: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        marginTop: theme.spacing.unit,
        border: 2,
        width: theme.breakpoints.values.lg,
    },
    // img: {
    //   padding: 5,
    //   paddingBottom: 8,
    //   width: 280,
    // },
    // span: {
    //   fontSize: '14px',
    //   color: gray,
    //   paddingLeft: '5px'
    // },
    badge: {
        top: 5,
        right: -15,
        cursor: 'pointer',
    },
    marginRight:{
        marginRight: theme.spacing.unit*3,
    },
    fileList: {
        margin: 5,
        padingBottom: 8,
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
    float:{
        // float: 'left',
        fontSize: '14px',
        color: gray,
        padding: '3px',
        // marginRight: '10px'
    },
    largeSpace: {
        lineHeight: 1.5
    }

});


@withStyles(styles)
@inject('helperStore', 'ordersStore', 'authStore', 'courseStore', 'customerStore')
@withRouter
@observer
export default class NewPackageOrder extends Component{
    state = {
        //currency: 1,
        order: this.props.order||{"courseType":this.props.packageInfo.courseType,"wechat":this.props.packageInfo.wechat, "idcourse":this.props.packageInfo.idcourse, "topic":"", "assignmentType":"", "idcustomer":this.props.packageInfo.idcustomer, "description":"",  "deadline":"", "realDeadline":"", "labels":[],  "fileUrl":[], "words":"", "referenceStyle":"", "expection":"", "emerg":0},
        label: {"level": "Normal", "content": ""},
        orderValidate: {"wechat":"", "courseType":"", "assignmentType":"", "idcustomer":"", "deadline":"", "realDeadline":"", "currency_sale":"","salePrice":"",},
        timeZone: momenttz.tz.guess(),
        paymentArray: this.props.order?typeof(this.props.order.paymentImg)==='string'?JSON.parse(this.props.order.paymentImg):this.props.order.paymentImg:[],
        paymentImgFiles: [],
        paymentObj: {paymentType: '', paymentCurrency: '', paymentMount: '', paymentImg: ''},
        paymentObjValidate: {paymentType: '', paymentCurrency: '', paymentMount: '', paymentImg: ''},
        customer: this.props.packageInfo.idcustomer? this.props.customerStore.getCustomerById(this.props.packageInfo.idcustomer) : undefined,
        errorInfo: [],
        imageAdding: "",
        fileAdding: "",
        courseInput: false,
        ready:false,
        update_currency_cost:""
    }


    handleTimeZoneChange = (event) => {
        this.setState({
            timeZone: event.target.value,
        });
    };

    handleInputChange = (event) =>{
        console.log(event.target.id)
        let updatingOrderValidation = this.state.orderValidate
        if(event.target.id in [...Object.keys(updatingOrderValidation)]){
            if((''+this.state.order[event.target.id]).trim().length===0){
                updatingOrderValidation[event.target.id] = event.target.id + ' is required'
            }else{
                updatingOrderValidation[event.target.id] = ""
            }
        }
        let updatingOrder = this.state.order
        updatingOrder[event.target.id] = event.target.value
        this.setState({
            order: updatingOrder,
            orderValidate: updatingOrderValidation
        })
    }
    handleLabelInputChange = (event) => {
        let updatingLabel = this.state.label
        updatingLabel[event.target.id] = event.target.value
        this.setState({
            label: updatingLabel
        })
    }
    handleAddLabel = (event) => {
        let updatingOrder = this.state.order
        let updatingLabels = this.state.order.labels
        if(typeof(updatingLabels) === 'string'){
            updatingLabels = JSON.parse(updatingLabels)
        }
        updatingLabels = updatingLabels.concat(this.state.label)
        updatingOrder["labels"] = updatingLabels
        this.setState({
            order: updatingOrder,
            label: {"level": "Normal", "content": ""}
        })
    }
    handleUpdateLabel = newLabels => {
        let orginalOrder = this.state.order
        orginalOrder['labels'] = newLabels
        this.setState({
            order: orginalOrder,
        })
    }


    handleAddPayment = () =>{
        let Order = this.state.order;
        let errorInfo=this.state.errorInfo;
        let packageInfo =this.props.packageInfo;
        let totalCost;
        let NeedPay;
        if(!Order.EditPrice){
            totalCost = Math.abs(parseInt(Order.salePrice))+(packageInfo.cost?parseInt(packageInfo.cost):0);
            NeedPay= this.props.helperStore.getCurrency(packageInfo.currency_sale).currency +(Math.abs(parseInt(packageInfo.salePrice) - totalCost))
            console.log("total: " + totalCost);
        }
        if(Order.EditPrice){
            totalCost = parseInt(Order.EditPrice) +(parseInt(packageInfo.cost)-parseInt(Order.salePrice));
            NeedPay = this.props.helperStore.getCurrency(packageInfo.currency_sale).currency +(Math.abs(parseInt(packageInfo.salePrice) - totalCost))
            console.log("total: " + totalCost);
        }
        if(Order.salePrice && Order.currency_sale){
            console.log("1st if"+Order.salePrice + packageInfo.cost+"totalCost"+totalCost)
                if(Order.currency_sale == packageInfo.currency_sale){
                    console.log("3 if")
                    //This cost more than package budget
                    if(totalCost > parseInt(packageInfo.salePrice)){
                        message.error("The price of this order has exceeded the rest budget of the package! Need to pay: "
                            +NeedPay+" more!")
                        this.setState({
                            errorInfo: errorInfo,
                            ready:false
                        })
                    }
                    //Sufficient budget left, update the data
                    else {
                        message.success("Sufficient budget for this order!")
                        console.log("yes else"+totalCost)

                        this.setState({
                            errorInfo:"",
                            update_currency_cost:Order.currency_sale,
                            update_cost:totalCost,
                            ready:true,
                        })
                    }
                }
                else{
                    alert("The Currency Type is different")
                    // errorInfo.push("The Currency Type is different")
                    this.setState({
                        errorInfo: errorInfo,
                        ready:false
                    })
                }
            }
        else{alert("Please input salePrice and currency type!")}



    }


    deleteArrayEle(array, filename){
        let newArray = []
        for(let ele of array){
            if(filename!==ele.filename) newArray.push(ele)
        }
        return newArray
    }

    handleFileclassChange = (event) => {
        this.setState({
            fileclass: event.target.value
        })
    }
    handleFilesUpload = (event) => {
        this.setState({fileAdding:"uploading..."})
        const files = Array.from(event.target.files)
        const formData = new FormData()
        files.forEach((file) => {
            formData.append('file', file)
        })
        formData.append('fileclass', this.state.fileclass)
        this.props.helperStore.uploadFiles(formData).then(res =>{
            if(res.statusCode === 200){
                console.log(res)
                let orginalOrder = this.state.order
                let fileUrl = this.state.order.fileUrl
                if(typeof(fileUrl) == 'string') fileUrl = JSON.parse(fileUrl)
                fileUrl = fileUrl||[]
                fileUrl = fileUrl.concat(res)
                orginalOrder['fileUrl'] = fileUrl
                this.setState({
                    order: orginalOrder,
                    fileAdding: ""
                })
            }else{
                this.setState({fileAdding: "upload files error: "+res})
            }
        }).finally(res=>{
            this.setState({fileAdding: ""})
        })
    }

    handleUpdateFileUrls = newFileUrls => {
        let orginalOrder = this.state.order
        orginalOrder['fileUrl'] = newFileUrls
        this.setState({order: orginalOrder})
    }

    validateField(){
        let updatingOrderValidate = this.state.orderValidate
        let validateStatus = false
        let errorInfo = []
        for(let key in updatingOrderValidate){
            // console.log(this.state.order[key], key)
            if((''+this.state.order[key]).trim().length===0){
                updatingOrderValidate[key] = key + ' is required'
                validateStatus = true
                // console.log(key, updatingOrderValidate[key])
                errorInfo.push(key + ' is required')
            }
        }
        this.setState({
            orderValidate: updatingOrderValidate,
            errorInfo: errorInfo
        })
        return validateStatus
    }
    utcDatetime(dt, tz){
        return moment.tz(dt,tz).format()
    }

    handleCreateOrder = () => {
        if(this.state.ready &&!this.validateField()){
            let order = Object.assign({}, this.state.order);
            let totalCost = this.state.update_cost;
            if(typeof(order.labels)=='object')  order.labels = JSON.stringify(order.labels)
            //if(typeof(order.paymentImg)=='object') order.paymentImg = JSON.stringify(order.paymentImg)
            if(typeof(order.fileUrl)=='object') order.fileUrl = JSON.stringify(order.fileUrl)
            if(!order.idorder) order.user_iduser = this.props.authStore.currentUser.iduser||1
            if(!order.idorder) order.autoProcess=0
            if(order.idorder) order.salePrice = this.state.order.EditPrice;

            // let paymentStatus = ''
            // for(let pay of this.state.paymentArray){
            //   let type = JSON.parse(pay.fileclass)
            //   if(type.paymentType == 'Bound Payment') paymentStatus = 'bound paid'
            //   if(type.paymentType == 'Part Payment') paymentStatus = 'part paid'
            //   if(type.paymentType == 'Full Payment') paymentStatus = 'full paid'
            // }

            //package orders full paid & verified already
            if(!order.idorder) order.status='waiting'
            if(!order.idorder) order.paymentStatus='full paid'
            if(!order.idorder) order.paymentVerify='verified'

            let topic = order.assignmentType==='Other'? order.topic : order.assignmentType+':'+order.topic
            let words = order.words ? ' -'+order.words+' words' : ''
            let courseCode = this.props.packageInfo.idcourse ? ' -'+this.props.courseStore.getCourseById(this.props.packageInfo.idcourse).courseCode||'' : ''
            let courseName = this.props.packageInfo.idcourse ? ' -'+this.props.courseStore.getCourseById(this.props.packageInfo.idcourse).name : ''
            let utcDeadline = moment.tz(order.deadline,'Asia/Shanghai').format('MMM Do')
            console.log(this.state.customer)
            order.name=`${this.state.customer.customerName}${order.idorder ? this.state.customer.code : parseInt(this.state.customer['code'], 10) + 1}${words}-${topic}-${order.courseType}${courseCode}${courseName}-${order.wechat}-${utcDeadline}`

            if(!order.idorder) order.currentProcessorRole='admin'
            if(!order.idorder) order.currentProcessor_iduser=1


            if(!order.idorder)
            {order.dateTime=moment.tz().format('YYYY-MM-DDTHH:mm:ss')
            order.deadline=this.utcDatetime(order.deadline, this.state.timeZone)
            order.realDeadline = this.utcDatetime(order.realDeadline, this.state.timeZone)}

            if(this.props.packageInfo.idorderPackage){
                order['orderPackage_idorderPackage'] = this.props.packageInfo.idorderPackage
                delete order['idorderPackage']
                delete order['currency_sale']
            }
            if(order.idorder) {
                delete order['EditPrice'];
                this.props.ordersStore.EditPackageOrder(order, totalCost, this.props.packageInfo.idorderPackage)
                    .then(res => {
                        // console.log(res.statusCode, JSON.stringify(res))
                        if (res.statusCode === 200) {
                            if (!order.idorder) {
                                let customer = this.props.customerStore.getCustomerById(this.state.order.idcustomer)
                                customer['code'] = parseInt(customer['code'], 10) + 1
                                this.props.customerStore.updateCustomer(customer)
                            }
                            if (this.props.cancleFunction) this.props.cancleFunction()
                            if (this.props.goBack) this.props.goBack()
                            this.props.history.push('/')
                            message.success('Successfully modified the order.')
                        } else {
                            this.setState({
                                error: JSON.stringify(res)
                            })
                        }
                    })

            }
            if(!order.idorder) {
                this.props.ordersStore.createNewPackageOrder(order, totalCost, this.props.packageInfo.idorderPackage)
                    .then(res => {
                        // console.log(res.statusCode, JSON.stringify(res))
                        if (res.statusCode === 200) {
                            if (!order.idorder) {
                                let customer = this.props.customerStore.getCustomerById(this.state.order.idcustomer)
                                customer['code'] = parseInt(customer['code'], 10) + 1
                                this.props.customerStore.updateCustomer(customer)
                            }
                            if (this.props.goBack) this.props.goBack()
                            this.props.history.push('/')
                            message.success('Successfully created the order.')
                        } else {
                            this.setState({
                                error: JSON.stringify(res)
                            })
                        }
                    })
            }

        }
        else{
            message.error("Please correct invalid information,or check payment by click 'Check Payment'!")
        }
    }


    generateErrorInfo(errorInfo){
        let info = []
        for(let ele of errorInfo){
            info.push(<div className={this.props.classes.float} key={ele}>{ele}</div>)
        }
        return info
    }

    handleSwitch = (event) => {
        let updatingOrder = this.state.order
        updatingOrder['emerg'] = updatingOrder['emerg']==0? 1 : 0

        this.setState({order:updatingOrder})
    }
    handleSwitchCourseInput = () => {
        this.setState({courseInput: !this.state.courseInput})
    }
    handleManualCourseInputChange = (event) =>{
        this.setState({manualCourseName: event.target.value})

    }
    handleManualCourseInputSubmit = () => {
        let courseObj = {}
        courseObj['name'] = this.state.manualCourseName
        courseObj['status'] = 'private'
        courseObj['createBy'] = this.props.authStore.currentUser.iduser
        courseObj['courseType'] = this.state.order.courseType
        this.props.courseStore.createCourse(courseObj).then(res =>{
            if(res.statusCode === 200){
                this.props.courseStore.loadCourses().then(rr => {
                    let updatingOrder = this.state.order
                    updatingOrder['idcourse'] = res.insertId
                    this.setState({
                        order: updatingOrder,
                    })
                })
            }else{
                alert('Add Course name error, please refresh and try again.')
            }
        })
    }

    render(){
        const { classes, helperStore, ordersStore, history, customerStore, authStore, courseStore} = this.props;
        const { order, orderValidate, customer, fileAdding, imageAdding } = this.state
        console.log(order)
        let courseName = order.idcourse ? courseStore.getCourseById(order.idcourse).name : ''
        return(
            <div>
                <Spin spinning={ordersStore.creating}>
                <h3>
                    <IconButton>
                        <ArrowBack onClick={() => this.props.goBack ? this.props.goBack() : history.goBack()} />
                    </IconButton>
                    &nbsp; { this.props.formName || "Create a New Order"}
                </h3>
                {
                    this.state.errorInfo.length > 0 ?
                        <Grid className={[classes.bgerror, classes.padding].join(' ')}>{this.generateErrorInfo(this.state.errorInfo)}</Grid>
                        : ''
                }
                <form className={classes.container}>
                    <div className={classes.root}>
                        <h5>1. Order Info</h5>

                        <TextField
                            id="assignmentType"
                            required
                            select
                            value={order.assignmentType}
                            onChange={this.handleInputChange}
                            label="Assignment Type"
                            error ={(orderValidate.assignmentType||'').length === 0? false : true  }
                            helperText={orderValidate.assignmentType}
                            className={classes.textField}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            SelectProps={{
                                native: true,
                                MenuProps: {
                                    className: classes.menu,
                                },
                            }}
                        >
                            <option value="" disabled selected>
                                Assignment Type
                            </option>
                            {helperStore.assignmentType.map(option => (
                                <option key={option} value={option}>
                                    {option}
                                </option>
                            ))}
                        </TextField>
                        {
                            order.assignmentType==='Other' ?
                                <TextField
                                    id="topic"
                                    value={order.topic}
                                    onChange={this.handleInputChange}
                                    label="Input Assignment Type"
                                    className={classes.textField}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                /> : ''
                        }

                        <TextField
                            id="words"
                            value={order.words}
                            onChange={this.handleInputChange}
                            label="Words Number"
                            className={classes.textField}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="referenceStyle"
                            select
                            value={order.referenceStyle||""}
                            onChange={this.handleInputChange}
                            label="Reference Style"
                            className={classes.textField}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            SelectProps={{
                                native: true,
                                MenuProps: {
                                    className: classes.menu,
                                },
                            }}
                        >
                            <option value="" disabled>
                                Reference Type
                            </option>
                            {helperStore.referenceType.map(option => (
                                <option key={option} value={option}>
                                    {option}
                                </option>
                            ))}
                        </TextField>
                        <TextField
                            id="expection"
                            value={order.expection}
                            onChange={this.handleInputChange}
                            label="Expectation"
                            className={classes.textField}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <FormControlLabel className={classes.marginTop} control={
                            <Switch
                                checked={order.emerg===0? false: true}
                                onChange={this.handleSwitch}
                                value={order.emerg}
                            />}
                                          label="Emergent"
                        />
                    </div>

                    <div className={[classes.root, classes.marginTop].join(' ')}>
                        <div>
                            <h5>2. Timezone Deadline and Notes/Labels</h5>
                            <TextField
                                id="timeZone"
                                select
                                required
                                defaultValue={this.state.timeZone}
                                onChange={this.handleTimeZoneChange}
                                label="Time Zone"
                                error ={this.state.timeZone.length === 0 ? true : false  }
                                className={classes.textField}
                                SelectProps={{
                                    native: true,
                                    MenuProps: {
                                        className: classes.menu,
                                    },
                                }}>
                                <option key={momenttz.tz.guess()} value={momenttz.tz.guess()}>{momenttz.tz.guess()}</option>
                                {
                                    helperStore.timezoneArray.map(t => (
                                        <option key={t.idtimezone} value={t.timezone}>{t.timezone + ' - ' + t.offset}</option>
                                    ))
                                }
                            </TextField>
                            <TextField
                                id="deadline"
                                required
                                // value={order.deadline}
                                onChange={this.handleInputChange}
                                label="Deadline"
                                type="datetime-local"
                                defaultValue={ moment(order.deadline, moment.ISO_8601).tz(this.state.timeZone).format("YYYY-MM-DDTHH:mm") || moment().add(7, 'days').format("YYYY-MM-DD") + "T23:59"}
                                error ={orderValidate.deadline.length === 0 ? false : true  }
                                helperText={orderValidate.deadline}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                className={classes.textField}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                            {console.log(moment(order.realDeadline, moment.ISO_8601).tz(this.state.timeZone).format("YYYY-MM-DDTHH:mm"))}

                            <TextField
                                id="realDeadline"
                                required
                                // value={order.realDeadline}
                                onChange={this.handleInputChange}
                                label="Real Deadline"
                                type="datetime-local"
                                defaultValue={moment(order.realDeadline, moment.ISO_8601).tz(this.state.timeZone).format("YYYY-MM-DDTHH:mm") || moment().add(14, 'days').format("YYYY-MM-DD") + "T23:59:59"}
                                error ={orderValidate.realDeadline.length === 0 ? false : true  }
                                helperText={orderValidate.realDeadline}
                                className={classes.textField}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </div>
                        <hr/>
                        <div>
                            <TextField
                                id="level"
                                select
                                onChange={this.handleLabelInputChange}
                                label="Note Level"
                                className={classes.textField}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                SelectProps={{
                                    native: true,
                                    MenuProps: {
                                        className: classes.menu,
                                    },
                                }}>
                                <option value="" disabled selected>
                                    Select Levels
                                </option>
                                {
                                    [...level].map(t => (
                                        <option key={t} value={t}>{t}</option>
                                    ))
                                }
                            </TextField>
                            <TextField
                                id="content"
                                value={this.state.label.content}
                                onChange={this.handleLabelInputChange}
                                label="Add Labels or Notes"
                                className={classes.textField}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                            <Button size="small" onClick={this.handleAddLabel} variant="contained" color="primary" className={classes.margin}>Add Label</Button>
                        </div>
                        <div>
                            Lables:
                            <GenerateLabelEdit labels={order.labels} updateLabel = {this.handleUpdateLabel}/>
                        </div>
                    </div>

                    <div className={[classes.root, classes.marginTop].join(' ')}>
                        <h5>4. Payment Information</h5>
                        <div>
                            <TextField
                                id="currency_sale"
                                required
                                select
                                label = "Currency"
                                value={order.currency_sale||""}
                                onChange={this.handleInputChange}
                                error ={orderValidate.currency_sale.length === 0 ? false : true  }
                                helperText={orderValidate.currency_sale}
                                className={classes.textField}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                SelectProps={{
                                    native: true,
                                    MenuProps: {
                                        className: classes.menu,
                                    },
                                }}
                            >
                                <option value="" disabled>
                                    Select Currency
                                </option>
                                {helperStore.currencyArray.map(option => (
                                    <option key={option.idcurrency} value={option.idcurrency}>
                                        {option.currency}
                                    </option>
                                ))}
                            </TextField>
                            {order.idorder?
                                <TextField
                                    id="EditPrice"
                                    required
                                    onChange={this.handleInputChange}
                                    defaultValue={order.salePrice}
                                    value={order.EditPrice}
                                    label="Sale Price"
                                    // error ={this.state.orderValidate.edit.length === 0 ? false : true  }
                                    // helperText={this.state.orderValidate.salePrice}
                                    className={classes.textField}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                                :
                                <TextField
                                    id="salePrice"
                                    required
                                    onChange={this.handleInputChange}
                                    value={order.salePrice}
                                    label="Sale Price"
                                    error ={this.state.orderValidate.salePrice.length === 0 ? false : true  }
                                    helperText={this.state.orderValidate.salePrice}
                                    className={classes.textField}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            }


                             <Button size="small" onClick={this.handleAddPayment} variant="contained" color="primary" className={classes.margin}>Check Payment</Button>

                        </div>
                        {/*<hr/>*/}

                    </div>


                    <div className={[classes.root, classes.marginTop].join(' ')}>
                        <h5>3. Documents/Materials </h5>
                        <div>
                            <TextField
                                id="fileclass"
                                select
                                label = "File Class"
                                onChange={this.handleFileclassChange}
                                className={classes.textField}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                SelectProps={{
                                    native: true,
                                    MenuProps: {
                                        className: classes.menu,
                                    },
                                }}
                            >
                                <option value="" disabled selected>
                                    Document Type
                                </option>
                                {helperStore.fileClass.map(option => (
                                    <option key={option} value={option}>
                                        {option}
                                    </option>
                                ))}
                            </TextField>
                            <TextField
                                id="attachement"
                                label="Choose Documents (Max 20MB)"
                                type="file"
                                accept="image/*"
                                onChange = {this.handleFilesUpload}
                                inputProps={{ multiple: true }}
                                className={classes.textField}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </div>
                        <div>
                            {fileAdding.length>0 ? fileAdding.length == 12 ?
                                <div className={classes.marginTop}><LinearProgress color="secondary" />{fileAdding}</div>
                                :
                                <div className={[classes.bgerror, classes.marginTop].join(' ')}>{fileAdding}</div>
                                : ''
                            }
                            <Grid className={classes.fileList}>
                                Materials:
                                <GenerateFileLinkEdit files={order.fileUrl} updateFileUrls = {this.handleUpdateFileUrls}/>
                            </Grid>
                        </div>
                    </div>

                    <div className={[classes.root, classes.marginTop].join(' ')}>
                        <h5>4. Customer Note (Other Import Information Customer Told)</h5>
                        <TextField
                            id="description"
                            fullWidth
                            value={order.description||`
Course Reference Number:
Customer Saying: 

Schoole system login URL:
Login username/password: 
(We won't share or otherwise engage in unauthorized use of your inforamtion)

Type of references and the edition of reference style:  

Other Special Requirements: 
Readings or other materials that should be included int he essay: 
Required Structure for the assignment and the approximate word count for each part: 
              `}
                            onChange={this.handleInputChange}
                            label="Description"
                            multiline
                            fullWidth
                            className={[classes.textField, classes.largeSpace, order.description? '':classes.grayText].join(' ')}
                            rows={15}
                        >

                        </TextField>

                    </div>
                    <div className={[classes.fileList].join(' ')}>
                        {this.props.cancleFunction ? <Button  variant="contained" onClick={this.props.cancleFunction} color="primary" className={classes.button}>Cancel</Button> : ""}
                        <Button  variant="contained" onClick={this.handleCreateOrder}  color="secondary" className={classes.button}>{this.props.formName ||"New Order"}</Button>
                    </div>
                </form>
                </Spin>
            </div>
        );
    }
}
