import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import momenttz from 'moment-timezone'

import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import LocalLibrary from '@material-ui/icons/LocalLibrary';
import ArrowBack from '@material-ui/icons/ArrowBack';
import Button from '@material-ui/core/Button';

import {AutoCompleteUniversity} from '../common/AutoComplete'

import './react-datetime.css'
import {gray} from './Colors'
import { GenerateFileLinkEdit } from '../util/generateFileLink';
import loading from '../util/loading';



const styles = theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing.unit *2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 1,
  },
  fileList: {
    margin: 5,
    padingBottom: 8,
  },
  label:{
    padding: '3px',
    paddingLeft: '5px',
    paddingRight: '5px',
    marginRight: '5px',
    marginTop: '4px',
    borderRadius: 3,
    fontSize: '12px',
    color: gray, 
    textDecoration: 'None',
  },
  list:{
    color: gray,
    padding: '3px',
    fontSize: '14px',
    textDecoration: 'underline',
  },
  span: {
    fontSize: '14px',
    color: gray,
    paddingLeft: '5px'
  },
  badge: {
    top: 5,
    right: -15,
    cursor: 'pointer',
  },

});


@withStyles(styles)
@inject('helperStore', 'courseStore', 'authStore')
@withRouter
@observer
export default class NewCourse extends Component{
  state = {
    currency: 1,
    order: Object.assign({},this.props.course)||{"name":"","courseCode":"", "description":"", "fileUrl":[], "courseType":"", "country":"", "university":""},
    orderValidate: { "courseType":"", "country":"", "university":""},
    selectedUniversity: {},
  }

  handleInputChange = (event) =>{
    let updatingOrderValidation = this.state.orderValidate
    if(event.target.value.trim().length===0){
      updatingOrderValidation[event.target.id] = event.target.id + ' is required'
    }else{
      updatingOrderValidation[event.target.id] = "" 
    }
    if (event.target.id ==="country"){
      this.props.helperStore.getCountryUni(event.target.value.toLowerCase())
    }
    let updatingOrder = this.state.order
    updatingOrder[event.target.id] = event.target.value
    this.setState({
      order: updatingOrder,
      orderValidate: updatingOrderValidation
    })
  }

  handleSelectInputChange = (selectObj) => {
    let updatingOrderValidation = this.state.orderValidate
    console.log(selectObj)
    if((''+selectObj).trim().length===0){
      updatingOrderValidation['university'] = 'university is required'
    }else{
      updatingOrderValidation['university'] = "" 
    }
    let updatingOrder = this.state.order
    updatingOrder['university'] = selectObj? selectObj.value: {}
    this.setState({
      order: updatingOrder,
      orderValidate: updatingOrderValidation, 
      selectedUniversity: selectObj
    })
  }
  handleSelectInputChange = (selectObj) => {
    let updatingOrder = this.state.order
    if(selectObj){
      updatingOrder['university'] = selectObj.value
      this.setState({
        order: updatingOrder,
      })
    }else{
      updatingOrder['university'] = ""
      this.setState({
        order: updatingOrder,
      })
    }
     
  }

  validateField(){
    let updatingOrderValidate = this.state.orderValidate
    let validateStatus = false
    for(let key in updatingOrderValidate){
      console.log(this.state.order[key], key)
      if((''+this.state.order[key]).trim().length===0){
        updatingOrderValidate[key] = key + ' is required'
        validateStatus = true
      }
    }
    this.setState({
      orderValidate: updatingOrderValidate
    })
    return validateStatus
  }

  handleCreateCourse = () => {
    if(!this.validateField()){
      let order = Object.assign({},this.state.order)
      let duplicateCourse = false
      for(let course of this.props.courseStore.Courses){
        console.log((course.name||'').toLowerCase().trim(), (order.name||'').toLowerCase().trim() ,  (course.courseCode||'').toLowerCase().trim() , (order.courseCode||'').toLowerCase().trim())
        if((course.name||'').toLowerCase().trim() === (order.name||'').toLowerCase().trim() && (course.courseCode||'').toLowerCase().trim() === (order.courseCode||'').toLowerCase().trim()){
          duplicateCourse = true
        }
      }
      if(!order.idcource && duplicateCourse){
        alert('The course name and code already exist in system, please reuse or update the existed course. ')
      }else{
        if(typeof(order.fileUrl)=='object') order.fileUrl = JSON.stringify(order.fileUrl)
        order.createBy = this.props.authStore.currentUser.iduser
        order.dateTime=moment.tz().format('YYYY-MM-DDTHH:mm:ss')
        this.props.courseStore.createCourse(order).then(res =>{
          if(res.statusCode == 200){
            if(this.props.cancleFunction) this.props.cancleFunction()
            // else this.props.history.push(`/courses/${res.insertId}`)
            else this.props.history.push(`/courses`)
          }else{
            this.setState({
              error: JSON.stringify(res)
            })
          }
        })
      }
      
    }
  }

  handleFileclassChange = (event) => {
    this.setState({
      fileclass: event.target.value
    })
  }
  handleFilesUpload = (event) => {
    this.setState({fileAdding:"uploading..."})
    const files = Array.from(event.target.files)
    const formData = new FormData()
    files.forEach((file) => {
      formData.append('file', file)
    })
    formData.append('fileclass', this.state.fileclass)
    this.props.helperStore.uploadFiles(formData).then(res =>{
      if(res.statusCode === 200){
        console.log(res)
        let orginalOrder = this.state.order
        let fileUrl = this.state.order.fileUrl
        if(typeof(fileUrl) == 'string') fileUrl = JSON.parse(fileUrl)
        fileUrl = fileUrl||[]
        fileUrl = fileUrl.concat(res)
        orginalOrder['fileUrl'] = fileUrl
        this.setState({
          order: orginalOrder,
          fileAdding: ""
        })
      }else{
        this.setState({fileAdding: "upload files error: "+res})
      }
    }).finally(res=>{
        this.setState({fileAdding: ""})
      })
  }

  handleUpdateFileUrls = newFileUrls => {
    let orginalOrder = this.state.order
    orginalOrder['fileUrl'] = newFileUrls
    this.setState({order: orginalOrder})
  }

  render(){
    const { classes, helperStore, courseStore, history} = this.props;
    const { order, isLoading } = this.state
    courseStore.isLoading||isLoading ? loading : ''
    // console.log(order.country)
    const university = order.country ? helperStore.countryuni :  helperStore.universities
    const countries = helperStore.countries
    const courseType = helperStore.courseType

    return(
      <div className={classes.root}>
        <h3>
          <IconButton>
              <ArrowBack onClick={() => this.props.cancleFunction?this.props.cancleFunction():this.props.history.goBack()} />
          </IconButton>
          &nbsp;
          <LocalLibrary/> { this.props.formName || "Create a New Course"}
        </h3>
        <Grid className={classes.bgerror}>{this.state.error}</Grid>
        <form className={classes.container}>
          <Grid container>
            <Grid item lg={1} sm={12} >
              <TextField
                id="country"
                required
                select
                defaultValue={order.country}
                onChange={this.handleInputChange}
                label="Country"
                error ={this.state.orderValidate.country.length === 0 ? false : true }
                helperText={this.state.orderValidate.country}
                InputLabelProps={{
                  shrink: true,
                }}
                className={classes.textField}
                SelectProps={{
                  native: true,
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
              >
                <option value="" disabled selected>
                  Country
                </option>
                  {countries.map(option => (
                    <option key={option} value={option}>
                      {option}
                    </option>
                  ))}
              </TextField> 
              </Grid>
              <Grid item lg={4} sm={12}>
                <AutoCompleteUniversity placeholderText="Type to search university  ..."  country ={order.country} defaultValue={{label: order.university, value: order.university}} options={university} onChangeCallback={this.handleSelectInputChange}/>
              </Grid>
          </Grid>
          <div>
            <TextField
              id="courseType"
              required
              select
              defaultValue={order.courseType}
              onChange={this.handleInputChange}
              label="Course Type"
              error ={this.state.orderValidate.courseType.length === 0 ? false : true }
              helperText={this.state.orderValidate.courseType}
              InputLabelProps={{
                shrink: true,
              }}
              className={classes.textField}
              SelectProps={{
                native: true,
                MenuProps: {
                  className: classes.menu,
                },
              }}
            >
              <option value="" disabled selected>
                Course Type
              </option>
                {courseType.map(option => (
                  <option key={option} value={option}>
                    {option}
                  </option>
                ))}
            </TextField> 
            <TextField
              id="courseCode"
              defaultValue={order.courseCode}
              onChange={this.handleInputChange}
              label="Course Code"
              InputLabelProps={{
                shrink: true,
              }}
              className={classes.textField}
            />
            <TextField
              id="name"
              defaultValue={order.name}
              onChange={this.handleInputChange}
              label="Course Name (year-semmester-name)"
              InputLabelProps={{
                shrink: true,
              }}
              className={classes.textField}
            />
          </div>
          <div>
            <TextField
              id="fileclass"
              select
              label = "File Class"
              onChange={this.handleFileclassChange}
              className={classes.textField}
              InputLabelProps={{
                shrink: true,
              }}
              SelectProps={{
                native: true,
                MenuProps: {
                  className: classes.menu,
                },
              }}
            >
            <option value="" disabled selected>
              Document Type
            </option>
              {helperStore.fileClass.map(option => (
                <option key={option} value={option}>
                  {option}
                </option>
              ))}
            </TextField> 
            <TextField
              id="attachement"
              label="Documents - Select Multipal supported files"
              type="file"
              onChange = {this.handleFilesUpload}
              inputProps={{ multiple: true }}
              className={classes.textField}

              InputLabelProps={{
                shrink: true,
              }}
              className={classes.textField}
            />
          </div>
          <Grid item xs={12} className={classes.fileList}>
            <GenerateFileLinkEdit files={order.fileUrl} updateFileUrls={this.handleUpdateFileUrls} />
          </Grid>

          <TextField
            id="description"
            defaultValue={order.description}
            onChange={this.handleInputChange}
            label="Description"
            multiline
            fullWidth
            className={classes.textField}
            rows={10}
            InputLabelProps={{
              shrink: true,
            }}
            className={classes.textField}
          />
          {this.props.cancleFunction ? <Button  variant="contained" onClick={this.props.cancleFunction} className={classes.textField} color="primary">Cancel</Button> : ""}
          <Button  variant="contained" onClick={this.handleCreateCourse} className={classes.textField} color="secondary">{this.props.formName ||"New Course"}</Button>
      </form>
    </div>
    );
  }
} 
