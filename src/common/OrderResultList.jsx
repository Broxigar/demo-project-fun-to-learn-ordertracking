import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import {GenerateFileLinkView} from '../util/generateFileLink'
import EditOrderResult from './EditOrderResult';
import {info, bginfo} from './Colors'
import {List, Button, Spin} from 'antd'
import OrderScore from './OrderScore';
import moment from 'moment'
import {PermissibleRender} from "@brainhubeu/react-permissible";

@inject('orderResultStore', 'userStore', 'ordersStore', 'authStore')
@withRouter
@observer
export default class OrderResultList extends Component{
  state = {
    editView: '',
    appendResult: false,
    // orderResultList: this.props.orderResultStore.currentOrderResultList(this.props.idorder)
  }
  // componentDidMount(){
  //   this.props.orderResultStore.getOrderResultList(this.props.idorder).then(res=>{
  //     this.setState({
  //       orderResultList: this.props.orderResultList.currentOrderResultList(this.props.idorder)
  //     })
  //   })
  // }
  componentDidMount(){
    this.props.orderResultStore.getOrderResultList(this.props.idorder)
  }
  editView(orderResult){
    this.props.orderResultStore.syncOrderResult(orderResult)
    this.setState({
      editView: orderResult.idorderResult
    })
  }
  handleCancelResult = () =>{
    this.props.orderResultStore.syncOrderResult({})
    this.setState({
      editView: ''
    })
  }
  appendResult = () => {
    this.props.orderResultStore.syncOrderResult({idorder: this.props.idorder, iduser: this.props.authStore.currentUser.iduser, datetime: moment().format('YYYY-MM-DDTHH:mm:ss')})
    this.setState({
      appendResult: true
    })
  }
  cancelAppendResult = () =>{
    this.props.orderResultStore.syncOrderResult({})
    this.setState({
      appendResult: false
    })
  }
  render(){
    const {userStore, orderResultStore, ordersStore, authStore, order} = this.props
    // const { orderResultList } = this.state
    const orderResultList = orderResultStore.orderResultList.get(this.props.idorder)||[]

    return(
        <div style={{background: bginfo, padding: '5px'}}>
          <Spin spinning={orderResultStore.orderResultIsLoading}>
            <List
                itemLayout="horizontal"
                dataSource={orderResultList}
                renderItem={(orderResult, index) => {
                  if(this.state.editView == orderResult.idorderResult) return(<EditOrderResult  cancel={this.handleCancelResult} />)
                  else return (


                        <List.Item actions={[<a onClick={() => this.editView(orderResult)}>Edit</a >, <a onClick={() => orderResultStore.deleteResult(orderResult)}>Delete</a >,
                          <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['qa', 'admin']} oneperm>
                            {
                                <a onClick={() => orderResultStore.sendToCustomer(order, orderResult)}>Send to Customer</a>
                            }
                          </PermissibleRender>
                            ]}>


                          <List.Item.Meta
                              title={`${index+1}. ${orderResult.datetime}: ${userStore.getUserById(orderResult.iduser).name}(${userStore.getUserById(orderResult.iduser).role})`}
                              description={
                                <div style={{paddingLeft: '15px'}}>
                                  <pre>{orderResult.description}</pre>
                                  <GenerateFileLinkView files={orderResult.files} />
                                </div>
                              }
                          />
                        </List.Item>


                  )
                }}
            />
            <hr/>
            {
              this.state.appendResult && <EditOrderResult cancel={this.cancelAppendResult} />
            }
            {
              order.status !== 'confirming' && order.status !== 'waiting' && order.status !== 'waiting writer' && !this.state.appendResult &&
              <Button onClick={this.appendResult}>Upload Result</Button>
            }
            <hr/>
            <OrderScore idorder={order.idorder}/>
          </Spin>
        </div>
    );
  }
}