import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import {withRouter, NavLink, Link} from 'react-router-dom';
import { Table, Divider, Tag } from 'antd';
import { withStyles } from '@material-ui/core/styles';
import {Grid} from '@material-ui/core'
import loading from '../common/loading'
import { Popover, Button } from 'antd';
import helperStore from '../stores/helperStore'
import { Typography } from 'antd';
import { Card } from 'antd';
import { Input, Spin, Row, Col, message } from 'antd';
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile';

import GridListTileBar from '@material-ui/core/GridListTileBar';
import SettingsOverscan from '@material-ui/icons/SettingsOverscan';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import Collapse from "@material-ui/core/Collapse";
import {GenerateFileLinkView} from "../util/generateFileLink";

const { Meta } = Card;
const { Search } = Input;

const styles = theme => ({
    root: {
        width: '100%',
        padding: theme.spacing.unit * 2,
        backgroundColor: theme.palette.background.paper,
    },
    paddingTop: {
        paddingTop: theme.spacing.unit * 2,
    },
    tabsRoot: {
        width: '100%',
        borderBottom: '1px solid #e8e8e8',
        paddingTop: theme.spacing.unit,
    },
    paddingBot: {

        paddingBottom:10,
    },
    padding: {


    },


});
const { Title } = Typography;



@withStyles(styles)
@inject('priceRequestStore','ordersStore', 'helperStore','courseStore','utilStore')
@withRouter
@observer
export default class SalePriceSearch extends Component {
    // state = {
    //     courseOrders:[]
    // }
    // handleChange = (event, currentStatus) => {
    //     this.setState({ currentStatus: currentStatus });
    // };
    state = {
        clicked: false,
        hovered: false,
        fileDisplayOpen: false,
        activeFile: {},
    };

    handlePaymentImgDislpay(file){
        this.setState({
          activeFile: file,
          fileDisplayOpen: true,
        })
      }
      handlePaymentImgDisplayCLose = () => {
        this.setState({
          activeFile: {},
          fileDisplayOpen: false,
        })
      }
      returnData = (idcourse)=>{
          let a = idcourse ? this.props.courseStore.getCourseById(idcourse)||{} : {};

          if (a.status === 'valid') a =a;
          else a = {}
          return a
      }


    render(){
        const { classes, courseStore, ordersStore,utilStore } = this.props;
        const data = ordersStore.PriceSearchOrders||[];
        //let courseDetail = data.idcourse ? courseStore.getCourseById(data.idcourse)||{} : {};
        //courseDetail = courseDetail.status === 'valid'? courseDetail : {};

        const columns = [
            {
                title: 'Course Code',
                dataIndex: 'courseCode',
                key: 'courseCode',
                //render: text => <a href="javascript:;">{text}</a>,
            },
            {
                title: 'Course Name',
                dataIndex: 'name',
                key: 'name',
            },
            {
                title: 'Order Name',
                dataIndex: 'orderName',
                key: 'orderName',
            },
            {
                title: 'Currency Type',
                dataIndex: 'currency_sale',
                key: 'currency_sale',
                render: text => text? <span>{helperStore.getCurrency(text).currency}</span> : null
            },
            {
                title: 'Sale Price',
                dataIndex: 'salePrice',
                key: 'salePrice',
            },
            {
                title: 'Score',
                key: 'score',
                dataIndex: 'score',

            },
            {
                title: 'Score Percent',
                dataIndex: 'scorePercent',
                key: 'scorePercent',
            },
            {
                title: 'Score Shot',
                dataIndex: 'scoreShot',
                key: 'scoreShot',

                render: text=> text?
                    <GridList cellHeight={40}>
                        <GridListTile>
                        <img src={this.props.helperStore.ROOT_API_URL + '/img/' + JSON.parse(text)[0].filename} alt={JSON.parse(text)[0].filename} />
                        <GridListTileBar
                            actionIcon={
                                <IconButton className={this.props.classes.icon} onClick={() => this.handlePaymentImgDislpay(JSON.parse(text)[0])}>
                                <SettingsOverscan />
                                </IconButton>
                            }
                            />
                        </GridListTile>
                    </GridList>
                    :null


            },
            {
                title: 'Order Details',
                dataIndex: 'Order Details',

            },

        ];
        //if(priceRequestStore.isLoading) return loading
        return(
            <div className={classes.root}>
                <Grid container justify={'space-between'} >
                    <Grid item>
                        <Title level={3}>Price Search</Title>

                    </Grid>
                    <Search
                        placeholder="input search text"
                        enterButton="Search"
                        size="large"
                        disabled={ordersStore.isLoading}
                        onSearch={value => {
                            if(value.length>0){
                                ordersStore.getHistoryOrdersByIdCourse(value)
                            }
                            else message.error('Please type in your search', 5);

                        }}
                    />


                </Grid>
                <Grid className={classes.paddingTop}>

                </Grid>
                <Grid >
                    <Spin spinning={ordersStore.isLoading} tip = 'Loading...' >
                        <Table columns={columns}
                               expandedRowRender=
                                   {order =>
                                       <div>
                                       <Grid container item xs={12}>

                                           <Grid item xs={12}>
                                               {
                                                   this.returnData(order.idcourse).idcource ?
                                                   <p>
                                                       Course Link: <Link className={classes.list} to={'/courses/'+this.returnData(order.idcourse).idcource}>{`${this.returnData(order.idcourse).idcource}. ${this.returnData(order.idcourse).courseCode||''} : ${this.returnData(order.idcourse).name||''} - ${this.returnData(order.idcourse).courseType||'' } - ${ this.returnData(order.idcourse).university||''} - ${this.returnData(order.idcourse).country||''} `}</Link>
                                                   </p> : 'No Course Linked to This Order'
                                               }
                                           </Grid>

                                               <Grid container item xs={12}>
                                                   <Grid item xs={12} >
                                                       <Spin spinning={utilStore.isDownloading}>
                                                           <Typography key={0} variant="subheading"  gutterBottom> Supported Documents/Materials: &nbsp;
                                                               <span className={[classes.label , classes.Success].join(' ')}>
                                                                   <a href='#' onClick={() => utilStore.downloadAllFiles(order.name, order.fileUrl, 'Customer Words.txt', order.description)} >
                                                                       Download All</a>
                                                               </span>
                                                           </Typography>
                                                           <GenerateFileLinkView files={order.fileUrl} />
                                                       </Spin>
                                                   </Grid>
                                               </Grid>
                                       </Grid>
                                       </div>
                                   }
                               expandIconColumnIndex={8}
                               expandIconAsCell={false}
                               dataSource={data}
                               rowKey='idkey'
                        />
                    </Spin>
                </Grid>

                <Dialog
                    fullWidth={true}
                    maxWidth={'md'}
                    open={this.state.fileDisplayOpen}
                    onClose={this.handlePaymentImgDisplayCLose}
                >
                    <DialogContent>
                    <img className={classes.image} src={this.props.helperStore.ROOT_API_URL + '/img/' +this.state.activeFile.filename} alt={this.state.activeFile.filename} />
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={this.handlePaymentImgDisplayCLose} color="primary">
                        Close
                    </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );

    }
}

