import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import {bgerror, gray, bgsuccess} from '../common/Colors'
import loading from '../common/loading'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ExchangeRequestDetail from '../common/ExchangeRequestDetail'
  
  const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
    },
    table: {
      minWidth: 700,
    },
    row: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
    button: {
      margin: theme.spacing.unit,
    },
    rightIcon: {
      marginLeft: theme.spacing.unit,
    },
    input: {
      display: 'none',
    },
    bgerror: {
      backgroundColor: bgerror,
    },
    label:{
      padding: '3px',
      paddingLeft: '5px',
      paddingRight: '5px',
      marginRight: '5px',
      marginTop: '4px',
      borderRadius: 3,
      fontSize: '14px',
      color: gray, 
      textDecoration: 'None',
      backgroundColor: bgsuccess,
    },
  });
  
  @withStyles(styles)
  @inject('exchangeRequestStore')
  @withRouter
  @observer
  
  export default class SaleExchangeRequests extends Component {

    render() {
      const { classes, exchangeRequestStore } = this.props;
      const requests = exchangeRequestStore.exchangeRequests
      if(exchangeRequestStore.isLoading) return loading  

        return (
          <div className={classes.root}>
            <Grid container className={classes.paddingTop}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell>ID</TableCell>
                    <TableCell>Exchange Amount</TableCell>
                    <TableCell>Account</TableCell>
                    <TableCell>Create Time</TableCell>
                    <TableCell>Deadline</TableCell>
                    <TableCell>Document</TableCell>
                    <TableCell>Status</TableCell>
                    <TableCell>Operation</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {requests.filter(request => request.status === 'opening').map((row, index) => {
                    return <ExchangeRequestDetail request={row} key={index} currentStatus={'opening'}/>
                  })}
                </TableBody>
              </Table>
            </Grid>
          </div>
        )
    }
    
  }
