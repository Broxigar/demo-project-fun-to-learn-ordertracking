import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import blue from '@material-ui/core/colors/blue';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import SearchIcon from '@material-ui/icons/Search'

import TablePagination from "@material-ui/core/TablePagination";

import OrderDetail from '../common/OrderDetail'


const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  paddingTop: {
    paddingTop: theme.spacing.unit,
    backgroundColor: 'None',
  },

  padding: {
    padding: `0 ${theme.spacing.unit * 2}px`,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
    paddingTop: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
  textField: {
    marginLeft: 8,
    flex: 1,
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  listitemBackColor: {
    backgroundColor: blue[100],
  },
  iconButton: {
    padding: 10,
  },
  search:{
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  }
});

@withStyles(styles)
@inject('ordersStore')
@withRouter
@observer
export default class SaleOrdersHistory extends Component{
  state = {
    currentStatus: '',
    months: [],
    search: '',
    page: 0, 
    rowsPerPage: 10,
  };

  componentDidMount(){
    this.setState({
      currentStatus: [...this.props.ordersStore.SaleCompletedOrdersMonths.values()][0] ? [...this.props.ordersStore.SaleCompletedOrdersMonths.values()][0].months : '',
      months: [...this.props.ordersStore.SaleCompletedOrdersMonths.values()]
    })
  }

  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
  };
  handleInputChange = event => {
    const {id, value} = event.target
    this.setState({[id]: value})
  }
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };
  
  render(){
    const { classes, ordersStore } = this.props;
    const { currentStatus, months, page, rowsPerPage } = this.state;
    let orders = currentStatus.length >0 ? ordersStore.getSaleHistoryOrders(currentStatus) : []
    if(!orders) orders = []
    console.log("tell mee"+currentStatus, orders)

    let filtered = []
    if(this.state.search){
      for(let ele of orders){
        if(JSON.stringify(ele).toLowerCase().indexOf(this.state.search.toLowerCase())>=0){
          filtered.push(ele)
        }
      }
    }else{
      filtered = orders
    }

    return(
      <Grid container className={classes.root}>
        <Grid container justify={'space-between'} >
          <Grid item className={classes.search}>
            <TextField 
              id='search'
              placeholder="Search Orders" 
              className={classes.textField} 
              onChange={this.handleInputChange}
              value={this.state.search}
              />
            <IconButton className={classes.iconButton} aria-label="Search">
              <SearchIcon />
            </IconButton>
          </Grid>
        </Grid>
        <Tabs 
          value={currentStatus} 
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          className={classes.tabsRoot}
          >
          { months.map((item, key) => <Tab key={key} value={item.months} label={
            <Badge className={classes.padding} color="primary" badgeContent={item.count}>
              {item.months}
            </Badge>
          } /> )}
        </Tabs>

        <Grid container item className={classes.paddingTop}>
          {
            filtered.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => {
              return <OrderDetail order={row} key={index} />
            })
          }
        </Grid>
        <TablePagination
            rowsPerPageOptions={[10, 20, 50]}
            component="div"
            count={ filtered.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
      </Grid>
    );
  }
} 
