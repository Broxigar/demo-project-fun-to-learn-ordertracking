import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import blue from '@material-ui/core/colors/blue';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search'

import OrderDetail from '../common/OrderDetail'
import AssignFuncBar from '../common/AssignFuncBar'

import loading from '../util/loading'
import { gray, lightBlue} from '../common/Colors'


const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  paddingTop: {
    paddingTop: theme.spacing.unit,
    backgroundColor: 'None',
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  listitemBackColor: {
    backgroundColor: blue[100],
  },
  label:{
    padding: '3px',
    paddingLeft: '5px',
    paddingRight: '5px',
    marginRight: '5px',
    marginTop: '4px',
    marginLeft: '3px',
    borderRadius: 3,
    fontSize: '12px',
    textDecoration: 'None',
    backgroundColor: lightBlue,
  },
});

@withStyles(styles)
@inject('helperStore', 'authStore', 'ordersStore')
@withRouter
@observer
export default class QualityOrders extends Component{
  state = {
    currentStatus: 'processing',
    orderStatus: [
      { label: "Writer Processing", value: 'processing'},
      { label: "QA Verifying", value: 'verifying'},
      { label: "Complete", value: 'complete'},
      { label: "Revision/Modifying", value: 'modifying'},
      { label: "Error/Refund Orders", value: 'error'}],
  };

  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
  };
  componentDidMount(){
    this.props.ordersStore.getInvitationsByIduser(this.props.authStore.currentUser.iduser, 'qa')
  }
 
  render(){
    const { classes, ordersStore} = this.props;
    const { currentStatus, orderStatus } = this.state;
    const qaorders = ordersStore.qaInvitations||[]
    
    console.log(qaorders, qaorders.length)

    return(
      <Grid container className={classes.root}>
        <Grid container justify={'space-between'} >
          <Grid item className={classes.search}>
            <TextField placeholder="Search Orders" className={classes.textField} />
            <IconButton className={classes.iconButton} aria-label="Search">
              <SearchIcon />
            </IconButton>
          </Grid>
          {/* <AssignFuncBar currentStatus={currentStatus} /> */}
        </Grid>
        <Tabs 
          value={currentStatus} 
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          className={classes.tabsRoot}
          >
          { orderStatus.map((item, key) => <Tab key={key} value={item.value} label={
            <div>{item.label} 
              <span className={this.props.classes.label}>
                {qaorders.filter(o => o.status===item.value).length}
              </span>
            </div>
          } /> )}
          {/* { orderStatus.map((item, key) => <Tab key={key} value={item} label={item} /> )} */}
        </Tabs>
        <Grid container item spacing={16} xs={12} className={classes.paddingTop}>
          {qaorders.map((order, index) => {
            if(order.status == currentStatus)
              return <OrderDetail order={order}  key={index} />
          })}
        </Grid>
        {ordersStore.isLoading? loading : ''}
      </Grid>
    );
  }
} 
