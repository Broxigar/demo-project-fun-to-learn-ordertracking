import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';

import {Table, Button, Radio, Spin} from 'antd'
import './quality.css'

const ButtonGroup = Button.Group;

@inject('helperStore', 'authStore', 'qualityStore')
@withRouter
@observer
export default class CheckTable extends Component{
    state={

    }
    handleChange = (event, currentStatus) => {
      this.setState({ currentStatus: currentStatus });
    };
  //   componentDidMount(){
  //     this.props.ordersStore.getInvitationsByIduser(this.props.authStore.currentUser.iduser, 'qa')
  //   }
 
  render(){
    const { classes, qualityStore} = this.props;

    const columns = [
      {
        title: 'No.',
        dataIndex: 'idcheckQuestion',
        width: '10%'
      },
      {
        title: 'Question',
        dataIndex: 'question',
        width: '65%'
      },
      {
        title: 'Operation',
        key: 'operation',
        width: '25%',
        render: (text, record, index) => 
          <Radio.Group buttonStyle="solid"
              onChange={(e) => this.props.qualityStore.setRecordStatus(record.idcheckQuestion, e.target.value)} 
              defaultValue={record.status}
              >
              <Radio.Button value={"NO"}>NO</Radio.Button>
              <Radio.Button value={"Not Apply"}>Not Apply</Radio.Button>
              <Radio.Button value={"YES"}>YES</Radio.Button>
          </Radio.Group>
      },
    ];

    return(
      <Spin spinning={qualityStore.isLoading}>
        <Table dataSource={qualityStore.checkQuestions} columns={columns} pagination={false} />
      </Spin>
    );
  }
} 
