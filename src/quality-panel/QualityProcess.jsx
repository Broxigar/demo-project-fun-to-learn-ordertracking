import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { Steps, Button, message, notification, Row, Col } from 'antd'
import CheckTable from './CheckTable'
import OrderDetail from '../common/OrderDetail'
import OrderResultAppend from './OrderResultAppend';
import {updateOrderStatus} from '../util/updateOrder'
import {addOrderComment} from "../util/addOrderComment";
import './quality.css'
import agent from '../agent'
import authStore from "../stores/authStore";

const { Step } = Steps;
notification.config({
  placement: 'bottomRight',
  bottom: 50,
  duration: 0,
});
message.config({
  top: 100,
  duration: 10,
  maxCount: 3,
});

@inject('authStore', 'qualityStore', 'ordersStore', 'orderResultStore')
@withRouter
@observer
export default class OrderProcess extends Component{
  state = {
    current: 0
  }
  componentDidMount(){
    
  }
  cancel = () =>{ this.props.history.push(`/qualityOrders`) }
  next = () => {
    if(this.state.current===0){
      this.props.qualityStore.submitCheckQuestion()
      .then(res =>{
        if(res ===200){
          this.goNext()
        }
      })
    }
    if(this.state.current===1){
      this.props.qualityStore.sumitOrderResult()
      // this.props.history.push('/qualityOrders')
      this.goNext()
    }
  }
  goNext(){
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
  };
 
  render(){
    const { orderResultStore, ordersStore, qualityStore} = this.props;
    this.props.qualityStore.setIdorder(this.props.match.params.idorder)
    const order = ordersStore.getOrderById(this.props.match.params.idorder)
    const { current } = this.state;

    const steps = [
      {
        title: 'Check Table',
        content: <CheckTable />,
      },
      {
        title: 'Remark Notes',
        content: <OrderResultAppend cancel={this.cancel} />
      },
      {
        title: 'Review and Confirm',
        content: <div>
          <Row type='flex' justify='space-around' align='middle' style={{marginTop: 10}}>
            <Col>
              <Button type="primary"  onClick={()=>{
                updateOrderStatus(order, 'complete')
                agent.Quality.email(this.props.match.params.idorder, 'Confirm')
                    .then(res=>{
                      res.forEach(val=> {
                            if(val.statusCode===200){
                              message.success("Successfully notified "+val.res+" by Email!")
                            }
                            else{
                              notification['error']({
                                message: 'Failed to send email!',
                                description:
                                    'Failed to send email to '+val.res+', please contact Administrator!',
                              });
                            }
                      }
                          )
                    })
                this.props.history.push('/qualityOrders')
              }}>Confirm and Complete (notify writer and sale in email)</Button>
            </Col>
          </Row>
          <Row type='flex' justify='space-around' align='middle' style={{marginTop: 10}}>
            <Col>
              <Button type="primary"  onClick={()=>{
                  updateOrderStatus(order, 'modifying')
                  this.props.history.push('/qualityOrders')
                agent.Quality.email(this.props.match.params.idorder, 'Modify')
                    .then(res=>{
                      res.forEach(val=> {
                            if(val.statusCode===200){
                              message.success("Successfully notified "+val.res+" by Email!")
                            }
                            else{
                              notification['error']({
                                message: 'Failed to send email!',
                                description:
                                    'Failed to send email to '+val.res+', please contact Administrator!',
                              });
                            }
                          }
                      )
                    })
                }}>Need further modify (notify writer in email)</Button>
            </Col>
          </Row>
          <Row type='flex' justify='space-around' align='middle' style={{marginTop: 10}}>
            <Col>
              <Button type="primary" onClick={()=>{
                if(window.confirm('Are you sure to move this order to waiting? assigned assistant and his/her submit will be deleted!! you can find the operation detail at comments. '))
                {
                    //ws notify admin that the order has been rejected
                    console.log('Order Rejected ws is connected!!')
                    let message={
                        orderID:order.idorder,
                        to_salesID:order.user_iduser,
                        to_xieshouID:0,
                        to_qaID:0,
                        from_iduser:authStore.currentUser.iduser,
                        type:2
                    };
                    // console.log(message)
                    authStore.ws.send(JSON.stringify(message))
                    //
                  agent.Quality.email(this.props.match.params.idorder, 'Reject')
                      .then(res=>{
                        res.forEach(val=> {
                              if(val.statusCode===200){
                                message.success("Successfully notified "+val.res+" by Email!")
                              }
                              else{
                                notification['error']({
                                  message: 'Failed to send email!',
                                  description:
                                      'Failed to send email to '+val.res+', please contact Administrator!',
                                });
                              }
                            }
                        )
                      })
                  ordersStore.deleteOrderResultByOrderId(order.idorder).then(res => {
                      addOrderComment(order, this.props.authStore.currentUser.iduser, `${this.props.authStore.currentUser.name}(${this.props.authStore.currentUser.iduser}) deleted the assistant and his/her submit. `)
                      updateOrderStatus(order, 'waiting')
                      console.log(this.props.match.params.idorder)

                      this.props.history.push('/qualityOrders')
                  })

                }
              }}>Reject the result, and ask to re-assign to new writer (notify writer and admin)</Button>
            </Col>
          </Row>
        </div>,
      },
    ];

    return(
      <div className='root'>
        {
          ordersStore.qaInvitations ? 
          <OrderDetail order={ordersStore.qaInvitations.filter(ele => ele.idorder==this.props.match.params.idorder)[0]}></OrderDetail>
          : ''
        }
        <hr />
         <Steps current={current}>
          {steps.map(item => (
            <Step key={item.title} title={item.title} />
          ))}
        </Steps>
        <div className="steps-content">{steps[current].content}</div>
        <div className="steps-action">
          {current > 0 && (
            <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
              Previous
            </Button>
          )}
          {current < steps.length - 1 && (
            <Button type="primary" onClick={this.next}>
              Next
            </Button>
          )}
          {current === steps.length - 1 && (
            ''
          )}
          <br/>
        </div>
      </div>
    );
  }
} 
