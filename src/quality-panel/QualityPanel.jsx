import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { NavLink } from 'react-router-dom';

import Collapse from '@material-ui/core/Collapse'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

import VerifiedUser from '@material-ui/icons/VerifiedUser';
import ListIcon from '@material-ui/icons/List'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import Folder from '@material-ui/icons/Folder'
import cyan from '@material-ui/core/colors/cyan';


const styles = theme => ({
  active: {
    backgroundColor: cyan[50],
  },
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 3,
  },
});

@withStyles(styles)

export default class QualityPanel extends Component{
  state = {
    open: true,
  };

  handleClick = () => {
    this.setState(state => ({ open: !state.open }));
  };
  render(){
    const { classes } = this.props;
      return(
        <List>
        <ListItem button onClick={this.handleClick}>
          <ListItemIcon>
            <VerifiedUser />
          </ListItemIcon>
          <ListItemText inset primary="Quality" />
          {this.state.open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
      
        <Collapse in={this.state.open} timeout="auto" unmountOnExit>
          <List component="div" >
            <ListItem button  className={classes.nested} component={NavLink} to="/qualityOrders" exact activeClassName={classes.active}>
              <ListItemIcon>
                <ListIcon />
              </ListItemIcon>
              <ListItemText primary="Orders" />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/qualityPackages" exact activeClassName={classes.active}>
              <ListItemIcon>
                <Folder />
              </ListItemIcon>
              <ListItemText primary="Packages" />
            </ListItem>
          </List>
        </Collapse>
        <Divider />
      </List>
    );
  }
} 
