import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import cyan from '@material-ui/core/colors/cyan';

import MoneyIcon from '@material-ui/icons/Money';
import CompareArrows from '@material-ui/icons/CompareArrows';
import VerticalAlignBottom from '@material-ui/icons/VerticalAlignBottom';
import { NavLink } from 'react-router-dom';


class ExchangePanel extends Component{
  state = {
    open: true,
  };

  handleClick = () => {
    this.setState(state => ({ open: !state.open }));
  };
    render(){
      const { classes } = this.props;
        return(
          <List>
          <ListItem button onClick={this.handleClick}>
          <ListItemIcon>
            <MoneyIcon />
          </ListItemIcon>
          <ListItemText inset primary="Exchange $" />
          {this.state.open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        
        <Collapse in={this.state.open} timeout="auto" unmountOnExit>
          <List component="div">
              <ListItem button className={classes.nested} component={NavLink} to="/exchangeRequests" exact activeClassName={classes.active}>
                <ListItemIcon>
                  <CompareArrows />
                </ListItemIcon>
                <ListItemText primary="Request" />
              </ListItem>
              <ListItem button className={classes.nested} component={NavLink} to="/exchangeCompleted" exact activeClassName={classes.active}>
                <ListItemIcon>
                  <VerticalAlignBottom />
                </ListItemIcon>
                <ListItemText primary="Completed" />
              </ListItem>
            </List>
          </Collapse>
          <Divider />
        </List>
      );
    }
} 

const styles = theme => ({
  active: {
    backgroundColor: cyan[50],
  },
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 3,
  },
});

ExchangePanel.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ExchangePanel);
