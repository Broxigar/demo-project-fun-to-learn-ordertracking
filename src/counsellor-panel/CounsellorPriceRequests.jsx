import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import {PermissibleRender} from '@brainhubeu/react-permissible';

import PriceQuoteDetail from '../common/PriceQuoteDetail'
import loading from '../common/loading'


const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
  },
  paddingTop: {
    paddingTop: theme.spacing.unit,
    backgroundColor: 'None',
  }
});

@withStyles(styles)
@inject('priceRequestStore', 'helperStore', 'userStore', 'authStore')
@withRouter
@observer
export default class CounsellorPriceRequests extends Component {
  state = {
    currentStatus: 'requesting', 
    priceRequestStatus: ['requesting','confirming', 'confirm']
  }
  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
  };
 
render(){
  const { classes, helperStore, priceRequestStore, userStore, authStore } = this.props;
  const { currentStatus, priceRequestStatus } = this.state
  const priceQuotes = priceRequestStore.getUserPriceQuote
  const counsellor = authStore.currentUser
  const request = {}
  // if(!request) request = priceRequestStore.getPriceRequestByIdpriceRequest(quote.priceRequest_idpriceRequest)


  if(!priceQuotes) return loading

  return(
    <div className={classes.root}>
      <Tabs 
        value={currentStatus} 
        onChange={this.handleChange}
        indicatorColor="primary"
        textColor="primary"
        className={classes.tabsRoot}
        >
        { priceRequestStatus.map((item, key) => <Tab key={key} value={item} label={item} /> )}
      </Tabs>
      <Grid container className={classes.paddingTop}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>No.</TableCell>
              <TableCell>Counsellor</TableCell>
              <TableCell>Labels</TableCell>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'counsellor']} oneperm>
                <TableCell>Quote Price</TableCell>
              </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'saleman']} oneperm>
                <TableCell>Finnal Price</TableCell>
              </PermissibleRender>
              <TableCell>Status</TableCell>
              <TableCell>Operation</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {priceQuotes.map((quote, index) => {
              if(quote.status == currentStatus) 
              return <PriceQuoteDetail counsellor={counsellor} quote={quote} 
                        request={priceRequestStore.getPriceRequestByIdpriceRequest(quote.priceRequest_idpriceRequest)} 
                        key={index} index={index} />
            })}
          </TableBody>
        </Table>
        
      </Grid>
    </div>
    );
  }
} 

