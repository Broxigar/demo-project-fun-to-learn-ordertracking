import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PackageDetail from '../common/PackageDetail'

const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
  },
  paddingTop: {
    paddingTop: theme.spacing.unit,
    backgroundColor: 'None',
  },
});

@withStyles(styles)
@inject('ordersStore')
@withRouter
@observer
export default class CounsellorPackages extends Component{
  state = {
    currentStatus: 'processing',
    orderStatus: [ 'waiting writer', 'processing', 'verifying', 'complete', 'modifying',  'error']
  };
  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
  };
 
render(){
  const { classes } = this.props;
  const { currentStatus, orderStatus } = this.state;
  const orderPackages = this.props.ordersStore.UserPackages
  console.log(orderPackages)
  return(
    <Grid container className={classes.root}>
      {/* <AssignFuncBar currentStatus={currentStatus} /> */}
      <Tabs 
        value={currentStatus} 
        onChange={this.handleChange}
        indicatorColor="primary"
        textColor="primary"
        className={classes.tabsRoot}
        >
        { orderStatus.map((item, key) => <Tab key={key} value={item} label={item} /> )}
      </Tabs>
      <Grid container item spacing={16} xs={12} className={classes.paddingTop}>
        {orderPackages.map((row, index) => {
          console.log(row)
          if(row.status == currentStatus && row.idorderPackage)
            return <PackageDetail packageOrder={row} key={index} />
        })}
      </Grid>
    </Grid>
    );
  }
} 

