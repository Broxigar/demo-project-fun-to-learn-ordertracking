import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import blue from '@material-ui/core/colors/blue';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Divider from '@material-ui/core/Divider'

import {Map} from 'immutable'

import agent from '../agent'

import {
    BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer,PieChart, Pie,
  } from 'recharts';


const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  paddingTop: {
    paddingTop: theme.spacing.unit,
    backgroundColor: 'None',
  },

  padding: {
    padding: `0 ${theme.spacing.unit * 2}px`,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
    paddingTop: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
  textField: {
    marginLeft: 8,
    flex: 1,
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  listitemBackColor: {
    backgroundColor: blue[100],
  },
  iconButton: {
    padding: 10,
  },
  search:{
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  }
});

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
  cx, cy, midAngle, innerRadius, outerRadius, percent, index, Customer, NewCustomer
}) => {
   const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);
console.log()
  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
      {`${Customer}: ${(percent * 100).toFixed(0)}%`}
    </text>
  );
};
const renderNewCustomizedLabel = ({
  cx, cy, midAngle, innerRadius, outerRadius, percent, index, Customer, NewCustomer
}) => {
   const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);
console.log()
  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
      {`${NewCustomer}: ${(percent * 100).toFixed(0)}%`}
    </text>
  );
};
const COLORS = ['#a8e6cf', '#dcedc1', '#ffd3b6', '#ffaaa5', '#ff8b94', '#00aedb', '	#f47835	', '#d41243', '#8ec127', '#a200ff',]


@withStyles(styles)
@inject('ordersStore', 'helperStore', 'statisticStore')
@withRouter
@observer
export default class CustomerAnalysis extends Component{
  state = {
    currentStatus: '',
    months: [],
    allCustomer: new Map(),
    newCustomer: new Map(),
    orderStatistic: new Map(),
  };

  componentDidMount(){
    let currentStatus = [...this.props.ordersStore.OrdersMonths.values()][0] ? [...this.props.ordersStore.OrdersMonths.values()][0].months : ''
    let months = [...this.props.ordersStore.OrdersMonths.values()]
    let countries = this.props.helperStore.countries
    this.setState({
        currentStatus: currentStatus,
        months: months,
        countries: countries,
    })
    // this.getOrderStatistic(countries, currentStatus)
    this.updateCustomer(countries, currentStatus)
  }

  updateCustomer(countries, currentStatus){
    for(let c of countries){
        this.getAllCustomer(c, currentStatus)
        this.getNewCustomer(c, currentStatus)
        
    }
  }

  getAllCustomer(country, month){
    agent.Statistic.getCustomerStatisticByCountryMonth(country, month).then(res => {
        if(res.statusCode === 200){
            delete res['statusCode']
            let tempAllCustomer = this.state.allCustomer.set(country+'-'+month, res)
            this.setState({allCustomer: tempAllCustomer})
        } 
    })
  }
  getNewCustomer(country, month){
    agent.Statistic.getNewCustomerStatisticByCountryMonth(country, month).then(res => {
        if(res.statusCode === 200){       
            delete res['statusCode']
            let tempNewCustomer = this.state.newCustomer.set(country+'-'+month, res)
            this.setState({newCustomer: tempNewCustomer})
        }
    })
  }
  getOrderStatistic(countries, month){
    for(let c of countries){
        agent.Statistic.getOrderStatisticByCountryMonth(c, month).then(res => {
            if(res.statusCode === 200){       
                delete res['statusCode']
                let newOrderStatistic = this.state.orderStatistic.set(c+'-'+month, res)
                this.setState({orderStatistic: newOrderStatistic})
            }
        })
    }
  }

  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
    this.updateCustomer(this.state.countries, currentStatus)
    // this.getOrderStatistic(this.state.countries, currentStatus)
  };
  handleInputChange = event => {
    const {id, value} = event.target
    this.setState({[id]: value})
  }
  
  render(){
    if(!this.state.countries) return (<p></p>)

    const { classes, statisticStore } = this.props;
    const { currentStatus, months, countries } = this.state;

    let data = []
    for(let country of countries){
      if(country !== 'India') {
        let obj = {}
        obj['Country'] = country
        const month = currentStatus
        let allCustomer = this.state.allCustomer.get(country+'-'+month)||[]
        let newCustomer = this.state.newCustomer.get(country+'-'+month)||[]
        obj['Customer'] = allCustomer.map(customer => customer.customerNumber).reduce((a,b)=>a+b,0)
        obj['NewCustomer'] = newCustomer.map(customer => customer.customerNumber).reduce((a,b)=>a+b,0)
        data.push(obj)
      } 
      
    }
    let allCustomer = data.map(d => d['Customer']).reduce((a,b)=> a+b, 0)
    let allNewCustomer = data.map(d => d['NewCustomer']).reduce((a,b)=> a+b, 0)
    // let data = orderStatistic.concat(allCustomer)
    console.log(data)
    return(
      <Grid container className={classes.root}>
        <Tabs 
          value={currentStatus} 
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          className={classes.tabsRoot}
          >
          { months.map((item, key) => <Tab key={key} value={item.months} label={item.months} /> )}
        </Tabs>
        <Grid container item className={classes.paddingTop} >
        <h4>Customer Increase by Countries</h4>
            <ResponsiveContainer width={'100%'} height={300}>
            <BarChart
                data={data}
                margin={{
                top: 20, right: 30, left: 20, bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="Country" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="Customer" stackId="a" fill={COLORS[0]} label minPointSize={0} barSize={30}/>
                <Bar dataKey="NewCustomer" stackId="a" fill={COLORS[1]} label minPointSize={0} barSize={30}/>
            </BarChart>
            </ResponsiveContainer>
            <Grid item xs={12} className={classes.paddingTop}>
                <Divider/>
            </Grid>
            <Grid container className={classes.paddingTop}>
                <Grid item sm={12} md={12} lg={6}>
                  <h4>All Custoemrs Percentage by Countries</h4>
                  <ResponsiveContainer width={'100%'} height={300}>
                    <PieChart margin={{
                              top: 20, right: 30, left: 20, bottom: 5,
                              }}>
                      <Pie dataKey="Customer" nameKey="Country" isAnimationActive={false} data={data} cx={250} cy={120} outerRadius={120} fill="#8884d8"  labelLine={false} label={renderCustomizedLabel}>
                      {
                        data.map((entry, index) => <Cell key={`${entry.Country}`} fill={COLORS[index % COLORS.length]} />)
                      }
                      </Pie>
                      {/* <Pie dataKey="NewCustomer" data={data} cx={500} cy={200} innerRadius={40} outerRadius={80} fill="#82ca9d" /> */}
                      <Tooltip />
                      <Legend />
                    </PieChart>
                  </ResponsiveContainer>
                </Grid>
                <Grid item sm={12} md={12} lg={6}>
                  <h4> New Customers Percentage By Countries</h4>
                  <ResponsiveContainer width={'100%'} height={300}>
                    <PieChart margin={{
                        top: 20, right: 5, left: 5, bottom: 5,
                        }}>
                      <Pie dataKey="NewCustomer" nameKey="Country" isAnimationActive={false} data={data} cx={250} cy={120} outerRadius={120} fill="#8884d8" labelLine={false} label={renderNewCustomizedLabel}>
                      {
                        data.map((entry, index) => <Cell key={`${entry.Country}`} fill={COLORS[index % COLORS.length]} />)
                      }
                      </Pie>
                      {/* <Pie dataKey="NewCustomer" data={data} cx={500} cy={200} innerRadius={40} outerRadius={80} fill="#82ca9d" /> */}
                      <Tooltip />
                      <Legend />
                    </PieChart>
                  </ResponsiveContainer>
                </Grid>
            </Grid>
        </Grid>
      </Grid>
    );
  }
} 
