import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import ArrowBack from '@material-ui/icons/ArrowBack';
import OrdersFiltered from '../common/OrdersFiltered';

const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
      backgroundColor: theme.palette.background.paper,
    },
  });
  
@withStyles(styles)
@inject('ordersStore')
@withRouter
@observer
export default class HeatmapOrdersDetail extends Component {
  state = {
    orders: []
  }
  componentDidMount(){
    const { by, iduser, datetime } = this.props.match.params
    console.log(this.props)
    if(by==='byDeadline'){
      this.props.ordersStore.getOrdersBySaleDeadline(iduser, datetime).then(res => {
        if(res.statusCode===200){
          this.setState({orders: res})
        }
      })
    }else if(by==='byCreatetime'){
      this.props.ordersStore.getOrdersBySaleCreateTime(iduser, datetime).then(res => {
        if(res.statusCode===200){
          this.setState({orders: res})
        }
      })
    }else if(by==='byCounsellor'){
      this.props.ordersStore.getOrdersByCounsellorDeadline(iduser, datetime).then(res => {
        if(res.statusCode===200){
          this.setState({orders: res})
        }
      })
    }
  }
  render() {
    const { classes, ordersStore } = this.props;
    const { orders } = this.state
    

    return (
        <div className={classes.root}>
           <Grid container justify={'space-between'} >
            <Grid item>
              <Button variant="outlined" color="secondary" size="small" onClick={this.props.history.goBack}>
                <ArrowBack /> &nbsp; Go Back 
              </Button>
            </Grid>
            <Grid item>
            </Grid>
          </Grid>

          <OrdersFiltered orders={orders} />
        </div>
      )
    }
  }