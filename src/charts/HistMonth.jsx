import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import blue from '@material-ui/core/colors/blue';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Divider from '@material-ui/core/Divider'

import {Map} from 'immutable'

import agent from '../agent'

import {
    BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer
  } from 'recharts';


const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  paddingTop: {
    paddingTop: theme.spacing.unit,
    backgroundColor: 'None',
  },

  padding: {
    padding: `0 ${theme.spacing.unit * 2}px`,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
    paddingTop: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
  textField: {
    marginLeft: 8,
    flex: 1,
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  listitemBackColor: {
    backgroundColor: blue[100],
  },
  iconButton: {
    padding: 10,
  },
  search:{
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  }
});

const COLORS = ['#a8e6cf', '#dcedc1', '#ffd3b6', '#ffaaa5', '#ff8b94']

@withStyles(styles)
@inject('ordersStore', 'helperStore', 'statisticStore')
@withRouter
@observer
export default class OrdersHistory extends Component{
  state = {
    currentStatus: '',
    months: [],
    allCustomer: new Map(),
    newCustomer: new Map(),
    orderStatistic: new Map(),
  };

  componentDidMount(){
    let currentStatus = [...this.props.ordersStore.OrdersMonths.values()][0] ? [...this.props.ordersStore.OrdersMonths.values()][0].months : ''
    let months = [...this.props.ordersStore.OrdersMonths.values()]
    let countries = this.props.helperStore.countries
    this.setState({
        currentStatus: currentStatus,
        months: months,
        countries: countries,
    })
    this.getOrderStatistic(countries, currentStatus)
    this.updateCustomer(countries, currentStatus)
  }

  updateCustomer(countries, currentStatus){
    for(let c of countries){
        this.getAllCustomer(c, currentStatus)
        this.getNewCustomer(c, currentStatus)
        
    }
  }

  getAllCustomer(country, month){
    agent.Statistic.getCustomerStatisticByCountryMonth(country, month).then(res => {
        if(res.statusCode === 200){
            delete res['statusCode']
            let tempAllCustomer = this.state.allCustomer.set(country+'-'+month, res)
            this.setState({allCustomer: tempAllCustomer})
        } 
    })
  }
  getNewCustomer(country, month){
    agent.Statistic.getNewCustomerStatisticByCountryMonth(country, month).then(res => {
        if(res.statusCode === 200){       
            delete res['statusCode']
            let tempNewCustomer = this.state.newCustomer.set(country+'-'+month, res)
            this.setState({newCustomer: tempNewCustomer})
        }
    })
  }
  getOrderStatistic(countries, month){
    for(let c of countries){
        agent.Statistic.getOrderStatisticByCountryMonth(c, month).then(res => {
            if(res.statusCode === 200){       
                delete res['statusCode']
                let newOrderStatistic = this.state.orderStatistic.set(c+'-'+month, res)
                this.setState({orderStatistic: newOrderStatistic})
            }
        })
    }
  }

  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
    this.updateCustomer(this.state.countries, currentStatus)
    this.getOrderStatistic(this.state.countries, currentStatus)
  };
  handleInputChange = event => {
    const {id, value} = event.target
    this.setState({[id]: value})
  }
  
  render(){
    if(!this.state.countries) return (<p></p>)

    const { classes, statisticStore } = this.props;
    const { currentStatus, months } = this.state;

    const ChartsForCountries = []
    

    for(let country of this.state.countries){
        const month = currentStatus
        let orderStatistic = this.state.orderStatistic.get(country+'-'+month)||[]
        let allCustomer = this.state.allCustomer.get(country+'-'+month)||[]
        let newCustomer = this.state.newCustomer.get(country+'-'+month)||[]
        for(let sta of orderStatistic){
            let findCustomer = false
            for(let customer of allCustomer){
                if(sta.university === customer.university){
                    findCustomer = true
                    sta['Customer'] = customer.customerNumber
                }
            }
            if(!findCustomer){
                sta['Customer'] = 0
            }
        }
        for(let sta of orderStatistic){
            let findCustomer = false
            for(let customer of newCustomer){
                if(sta.university === customer.university){
                    findCustomer = true
                    sta['NewCustomer'] = customer.customerNumber
                }
            }
            if(!findCustomer){
                sta['NewCustomer'] = 0
            }
        }


        const ChartCountry = orderStatistic.length === 0? 
        <Grid container className={classes.paddingTop} key={country}>{country}: No data to show 
            <Grid item xs={12}>
                <Divider/>
            </Grid>
        </Grid> 
        :
        <Grid container item className={classes.paddingTop} key={country}>
            {country}:
            <ResponsiveContainer width={'100%'} height={300}>
            <BarChart
                data={orderStatistic}
                margin={{
                top: 20, right: 30, left: 20, bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="university" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="orderNumber" fill={COLORS[0]} label={{ position: 'top' }} minPointSize={0} barSize={30}/>
                <Bar dataKey="totalSale" fill={COLORS[1]} label={{ position: 'top' }} minPointSize={0} barSize={30} unit="K" />
                <Bar dataKey="Customer" fill={COLORS[2]} label={{ position: 'top' }} minPointSize={0} barSize={30}/>
                <Bar dataKey="NewCustomer" fill={COLORS[3]} label={{ position: 'top' }} minPointSize={0} barSize={30}/>
            </BarChart>
            </ResponsiveContainer>
            <Grid item xs={12}>
                <Divider/>
            </Grid>
        </Grid>
        ChartsForCountries.push(ChartCountry)
    }

    return(
      <Grid container className={classes.root}>
        <Tabs 
          value={currentStatus} 
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          className={classes.tabsRoot}
          >
          { months.map((item, key) => <Tab key={key} value={item.months} label={item.months} /> )}
        </Tabs>

        {ChartsForCountries}
        
      </Grid>
    );
  }
} 
