import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import blue from '@material-ui/core/colors/blue';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Divider from '@material-ui/core/Divider'
import moment from 'moment';

import HeatMap from "react-heatmap-grid";
import {Map} from 'immutable'
import agent from '../agent'
import { bgLightBlue} from '../common/Colors'


const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  paddingTop: {
    paddingTop: theme.spacing.unit,
    backgroundColor: 'None',
  },

  padding: {
    padding: `0 ${theme.spacing.unit * 2}px`,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
    paddingTop: theme.spacing.unit,
  },
  h4:{
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
    paddingTop: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
  textField: {
    marginLeft: 8,
    flex: 1,
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  listitemBackColor: {
    backgroundColor: blue[100],
  },
  iconButton: {
    padding: 10,
  },
  search:{
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  }
});

const COLORS = ['#a8e6cf', '#dcedc1', '#ffd3b6', '#ffaaa5', '#ff8b94']

@withStyles(styles)
@inject('ordersStore', 'helperStore', 'userStore', 'authStore', 'statisticStore')
@withRouter
@observer
export default class HeatmapXieshou extends Component{
  state = {
    currentStatus: '',
    months: [],
    yLabels: this.props.userStore.Users.sort().map(user => user.name),
    xLabels: new Map(),
    orderStatisticDeadlineCounsellor: new Map(),
  };

  componentDidMount(){
    let currentStatus = [...this.props.ordersStore.OrdersMonths.values()][0] ? [...this.props.ordersStore.OrdersMonths.values()][0].months : ''
    let months = [...this.props.ordersStore.OrdersMonths.values()]
    
    this.setState({
        currentStatus: currentStatus,
        months: months,
    })
    if(currentStatus !== '') this.getOrderStatisticByDeadlineGroupByCounsellor(currentStatus)
    if(currentStatus !== '') {
      let tempXLabels = this.state.xLabels.get(currentStatus)
      if(!tempXLabels){
        let tempXlabel = this.generateXLabels(currentStatus)
        let tempXLabels = this.state.xLabels.set(currentStatus, tempXlabel)
        this.setState({xLabels: tempXLabels})
      }
    }

  }

  getOrderStatisticByDeadlineGroupByCounsellor(month){
    let tempArray = this.state.orderStatisticDeadlineCounsellor.get(month)
    if(!tempArray){
      agent.Statistic.getOrderStatisticByDeadlineGroupByCounsellor(month).then(res => {
        if(res.statusCode === 200){       
            delete res['statusCode']
            this.setState({
              orderStatisticDeadlineCounsellor: this.state.orderStatisticDeadlineCounsellor.set(month, this.transferMapToArray(res, month))
            })
        }
      })
    }
  }

  handleChange = (event, currentStatus) => {
    let tempXLabels = this.state.xLabels.get(currentStatus)
    if(!tempXLabels){
      let tempXlabel = this.generateXLabels(currentStatus)
      let tempXLabels = this.state.xLabels.set(currentStatus, tempXlabel)
      this.setState({xLabels: tempXLabels})
    }

    this.setState({ currentStatus: currentStatus });
    this.getOrderStatisticByDeadlineGroupByCounsellor(currentStatus)
  };

  generateXLabels(currentStatus) {
    const monthDays = moment(currentStatus, 'YYYY-MM').daysInMonth()
    const xLabels = new Array(monthDays).fill(0).map((_,i) => i>=9 ? `${i+1}` : `0${i+1}`);
    return xLabels
  }

  transferMapToArray(rawData, currentStatus){
    let yLabels = new Set()
    for(let ele of rawData){
      yLabels.add(ele.saleName)
    }
    this.setState({
      yLabels: [...yLabels.values()]
    })
    yLabels = [...yLabels.values()]

    let xLabels = this.state.xLabels.get(currentStatus)
    if(!xLabels) xLabels = this.generateXLabels(currentStatus)
    let dataMap = new Map()
    for(let y of yLabels){
      let row = new Map()
      for(let x of xLabels){
        row = row.set(x, 0)
      }
      dataMap = dataMap.set(y, row)
    }
    for(let ele of rawData){
      let salemanData = dataMap.get(ele.saleName)
      salemanData = salemanData.set(ele.day.split('-')[2], ele.orderNumber)
      dataMap = dataMap.set(ele.saleName, salemanData)
    }

    let data = []
    for(let key of yLabels){
      let row = []
      for(let d of xLabels){
        row.push(dataMap.get(key).get(d))
      }
      data.push(row)
    }
    return data
  }
  
  render(){
    const { classes, authStore } = this.props;
    const { currentStatus, months, yLabels, xLabels, orderStatisticDeadlineCounsellor} = this.state;
    const xLabel = xLabels.get(currentStatus)
    const ordersCollectCounsellor = orderStatisticDeadlineCounsellor.get(currentStatus)

    // console.log(this.state)
    if(currentStatus === '') return <p></p>
    if(!ordersCollectCounsellor) return <p></p>
    if(!xLabel) return <p></p>


    return(
      <Grid container className={classes.root}>
        <Tabs 
          value={currentStatus} 
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          className={classes.tabsRoot}
          >
          { months.map((item, key) => <Tab key={key} value={item.months} label={item.months} /> )}
        </Tabs>
        <Grid container className={classes.root}>
          <h4 >Xieshou Matrix by Deadline</h4>
          <Grid container className={classes.h4}>Show the number of orders by deadline </Grid>
          <Grid container style={{ fontSize: "14px", padding: '10px', background: bgLightBlue,  }}>
            <HeatMap
              xLabels={xLabel}
              yLabels={yLabels}
              xLabelsLocation={"bottom"}
              // xLabelsVisibility={xLabelsVisibility}
              xLabelWidth={60}
              data={ordersCollectCounsellor}
              squares
              onClick={(x, y) => alert(`Clicked ${x}, ${y}`)}
              cellStyle={(background, value, min, max, data, x, y) => ({
                background: `rgb(0, 151, 230, ${1 - (max - value) / (max - min)})`,
                fontSize: "11.5px",
                color: "#000"
              })}
              cellRender={value => value && `${value}`}
            />
          </Grid>
        </Grid>
      </Grid>
    );
  }
} 
