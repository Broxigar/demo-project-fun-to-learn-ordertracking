
import React from 'react';
import {  Route} from 'react-router-dom';

import AdminPanel from './admin-panel/AdminPanel'
import ExchangePanel from './exchange-panel/ExchangePanel'
import SalePanel from './sale-panel/SalePanel'

import QualityPanel from './quality-panel/QualityPanel'
import QualityOrders from './quality-panel/QualityOrders'
import QualityPackages from './quality-panel/QualityPackages'
import QualityProcess from './quality-panel/QualityProcess'

import CounsellorPanel from './counsellor-panel/CounsellorPanel'
import CounsellorPackages from './counsellor-panel/CounsellorPackages'

import AccountPanel from './account-panel/AccountPanel'
import {Orders, NewOrder, OrderDetail, Courses, NewCourse, CourseDetail, NewPackage, Packages, PackageDetail, AssignFuncBar, SaleFuncBar, Statistic} from './common/'
import CounsellorOrders from './counsellor-panel/CounsellorOrders'
import CounsellorStatistic from './counsellor-panel/CounsellorStatistic'
import CounsellorPriceRequests from './counsellor-panel/CounsellorPriceRequests'
import SaleOrders from './sale-panel/SaleOrders'
import SalePackages from './sale-panel/SalePackages'
import SalePriceRequests from './sale-panel/SalePriceRequests'
import SaleExchangeRequests from './sale-panel/SaleExchangeRequests'
import SaleExchangeReserved from './sale-panel/SaleExchangeReserved'
import AdminPriceRequests from './admin-panel/AdminPriceRequests'
import PriceRequestNew from './common/PriceRequestNew'

import SalesWechat from "./account-panel/SalesWechat";
import Account from './account-panel/Account'
import EditAccount from './account-panel/EditAccount'
import Customer from './account-panel/Customer'
import EditCustomer from './account-panel/EditCustomer'
import ExchangeRequests from './exchange-panel/ExchangeRequests'
import ExchangeRequestNew from './common/ExchangeRequestNew'
import OrdersHistory from './common/OrdersHistory';
import SaleOrdersHistory from './sale-panel/SaleOrdersHistory';

import HistMonth from './charts/HistMonth'
import CustomerAnalysis from './charts/CustomerAnalysis'
import HeatmapMonth from './charts/HeatmapMonth';
import OrderStatisticLineChartMonth from './charts/OrderStatisticLineChartMonth';
import SaleKPI from './charts/SaleKPI';
import CustomerDetail from './account-panel/CustomerDetail';
import HeatmapOrdersDetail from './charts/HeatmapOrdersDetail';
import Test from './common/Test';
import SalePriceSearch from './sale-panel/SalePriceSearch';
import WechatEdit from './account-panel/WechatEdit';
import UniversityManagement from './account-panel/UniversityManagement'
import NotificationList from "./common/NotificationBadge";


const ROLE_PANELS = {
  'admin': [<AdminPanel/>, <ExchangePanel/>, <AccountPanel/>],
  'saleman': [<SalePanel/>],
  'counsellor': [<CounsellorPanel/>],
  'account': [<AccountPanel/>],
  // 'translater': [],
  'qa': [<QualityPanel/>],
  'exchanger': [<ExchangePanel/>]
}
const ROLE_ROUTES = {
  'admin': [              
    <Route exact path="/"  component={Orders} />,
    <Route exact path="/orders"  component={Orders} />,
    <Route exact path="/historyOrders"  component={OrdersHistory} />, 
    <Route exact path="/heatmapAnalysis"  component={HeatmapMonth} />,
    <Route exact path="/heatmapOrders/:by/:iduser/:datetime"  component={HeatmapOrdersDetail} />,
    <Route exact path="/orderTrend"  component={OrderStatisticLineChartMonth} />,
    <Route exact path="/newOrder"  component={NewOrder} />,
    <Route exact path="/order/:idorder"  component={OrderDetail} />,
    <Route exact path="/counsellorOrders"  component={CounsellorOrders} />, 

    <Route exact path="/newCourse"  component={NewCourse} />,
    <Route exact path="/courses/:idcourse"  component={CourseDetail} />,
    <Route exact path="/courses"  component={Courses} />,

    <Route exact path="/newPackage"  component={NewPackage} />,
    <Route exact path="/packages"  component={Packages} />,
    <Route exact path="/packages/:idorderPackage"  component={PackageDetail} />,

    <Route exact path="/account/edit/:iduser"  component={EditAccount}/>,
    <Route exact path="/account/edit"  component={EditAccount}/>,
    <Route exact path="/account"  component={Account}/>,
    <Route exact path="/SalesWechat"  component={SalesWechat}/>,
    <Route exact path="/WechatEdit"  component={WechatEdit}/>,
    <Route exact path="/UniversityManagement"  component={UniversityManagement}/>,
    <Route exact path="/customer/edit/:idcustomer"  component={EditCustomer}/>,
    <Route exact path="/customer/edit"  component={EditCustomer}/>,
    <Route exact path="/customer/view/:idcustomer"  component={CustomerDetail}/>,
    <Route exact path="/customer"  component={Customer}/>,
    
    <Route exact path="/statistic"  component={Statistic}/>,
    <Route exact path="/orderIncrease"  component={HistMonth}/>,
    <Route exact path="/customerAnalysis"  component={CustomerAnalysis}/>,

    <Route exact path="/priceRequest"  component={AdminPriceRequests} />,
    <Route exact path="/newPriceRequest"  component={PriceRequestNew} />,

    <Route exact path="/exchangeRequests"  component={ExchangeRequests} />,
    <Route exact path="/newExchangeRequest"  component={ExchangeRequestNew} />,

    <Route exact path="/quality/:idorder" component={QualityProcess} />,
    <Route exact path="/testoperation" component={QualityProcess} />,
    <Route exact path="/NotificationList" component={NotificationList} />,

  ],
  'saleman': [      
    <Route exact path="/"  component={SaleOrders} />,
    <Route exact path="/saleOrders"  component={SaleOrders} />,
    <Route exact path="/saleHistoryOrders"  component={SaleOrdersHistory} />,
    <Route exact path="/saleOrderTrend"  component={OrderStatisticLineChartMonth} />, 
    <Route exact path="/heatmapAnalysis"  component={HeatmapMonth} />, 
    <Route exact path="/heatmapOrders/:by/:iduser/:datetime"  component={HeatmapOrdersDetail} />, 

    <Route exact path="/newOrder"  component={NewOrder} />,
    <Route exact path="/orders/:idorder"  component={OrderDetail} />,
    <Route exact path="/counsellorOrders"  component={CounsellorOrders} />,

    <Route exact path="/newCourse"  component={NewCourse} />,
    <Route exact path="/courses/:idcourse"  component={CourseDetail} />,
    <Route exact path="/courses"  component={Courses} />,

    <Route exact path="/newPackage"  component={NewPackage} />,
    <Route exact path="/salePackages"  component={SalePackages} />,
    <Route exact path="/salePackages/:idorderPackage"  component={PackageDetail} />,
    <Route exact path="/statistic"  component={Statistic}/>, 
    <Route exact path="/saleKPI"  component={SaleKPI}/>, 
    <Route exact path="/salePriceRequest"  component={SalePriceRequests} />,
      <Route exact path="/salePriceSearch"  component={SalePriceSearch} />,
    <Route exact path="/newPriceRequest"  component={PriceRequestNew} />,

    <Route exact path="/customer/edit/:idcustomer"  component={EditCustomer}/>,
    <Route exact path="/customer/edit"  component={EditCustomer}/>,
    <Route exact path="/customer/view/:idcustomer"  component={CustomerDetail}/>,
    <Route exact path="/customer"  component={Customer}/>,

    <Route exact path="/saleExchangeRequests"  component={SaleExchangeRequests}/>,
    <Route exact path="/saleExchangeReserved"  component={SaleExchangeReserved}/>,
  ],
  'services': [
    <Route exact path="/newCourse"  component={NewCourse} />,
    <Route exact path="/courses/:idcourse"  component={CourseDetail} />,
    <Route exact path="/courses"  component={Courses} />,
    <Route exact path="/customer/edit/:idcustomer"  component={EditCustomer}/>,
    <Route exact path="/customer/edit"  component={EditCustomer}/>,
    <Route exact path="/customer"  component={Customer}/>,
  ],
  'counsellor': [
    <Route exact path="/"  component={CounsellorOrders} />,
    <Route exact path="/counsellorOrders"  component={CounsellorOrders} />,
    <Route exact path="/statistic"  component={CounsellorStatistic}/>,
    <Route exact path="/orders/:idorder"  component={OrderDetail} />,
    <Route exact path="/counsellorPackages"  component={CounsellorPackages} />,
    <Route exact path="/counsellorPackages/:idorderPackage"  component={PackageDetail} />,
    <Route exact path="/counsellorPriceRequests"  component={CounsellorPriceRequests} />,
    <Route exact path="/courses/:idcourse"  component={CourseDetail} />,
  ],
  'account': [
    <Route exact path="/account/edit/:iduser"  component={EditAccount}/>,
    <Route exact path="/account/edit"  component={EditAccount}/>,
    <Route exact path="/account"  component={Account}/>,
    <Route exact path="/customer/edit/:idcustomer"  component={EditCustomer}/>,
    <Route exact path="/customer/edit"  component={EditCustomer}/>,
    <Route exact path="/customer"  component={Customer}/>
  ],
  // 'translater': [],
  'qa': [
    <Route exact path="/"  component={QualityOrders} />,
    <Route exact path="/qualityOrders"  component={QualityOrders} />,
    <Route exact path="/qualityPackages"  component={QualityPackages} />,
    <Route exact path="/quality/:idorder" component={QualityProcess} />,
    <Route exact path="/orders/:idorder"  component={OrderDetail} />,
    <Route exact path="/testoperation" component={QualityProcess} />,
    <Route exact path="/test" component={Test} />


  ],
  'exchanger': [
    <Route exact path="/exchangeRequests"  component={ExchangeRequests} />,
  ]
}

function getRolePanels(roles){
  let panels = []
  for( let role of roles){
    for( let key in ROLE_PANELS){
      if(key === role) panels = panels.concat(ROLE_PANELS[key])
    }
  }
  return panels
}
function getRoleRoutes(roles){
  let routes = []
  for( let role of roles){
    for( let key in ROLE_ROUTES){
      if(key === role) routes = routes.concat(ROLE_ROUTES[key])
    }
  }
  return routes
}

export {getRolePanels, getRoleRoutes}