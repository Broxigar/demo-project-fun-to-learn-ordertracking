import { observable, computed, action, createTransformer } from 'mobx';
import agent from '../agent';

class UserStore {
  @observable isLoading;
  @observable users = observable.map();
  @observable AllSalesWechat = [];
  @observable AllSalesWechatName =[];


  @computed get Users(){
    return this.users.values()
  }

  @computed get getUserById(){
    return createTransformer(id=>this.users.get(id)||{})
  }
  @computed get getUserByName(){
    return createTransformer(name=>this.users.values().filter(user => user.name===name)[0])
  }

  @computed get getCounsellorsWithMajor() {
    return createTransformer(major => {
      let counsellors = []
      for(let [key, user] of this.users) {
        if(user.role.includes('counsellor'))
          counsellors.push(user)
      }
      console.log('Counsellors:', counsellors)
      return counsellors
    })
  }

  @action init(){
    this.getAllUsers()
    this.updateAllSalesWechat()
    this.AllWechat()
  }

  @action updateUser(newUser) {
    this.isLoading = true;
    return agent.Users.save(newUser)
      .then(action(() => { this.getAllUsers() }))
      .finally(action(() => { this.isLoading = false; }))
  }

  @action getAllUsers(){
    this.isLoading = true;
    return agent.Users.allUsers()
      .then(action((res) => { 
        this.users.clear()
        res.forEach(ele => this.users.set(ele.iduser, ele));
      }))
      .finally(action(() => { this.isLoading = false; }))
  }

  @action createUser(newUser) {
    this.isLoading = true;
    return agent.Users.create(newUser)
        .then(action(() => { this.getAllUsers() }))
        .finally(action(() => { this.isLoading = false; }))
  }

  @action disableUser(iduser) {
      this.isLoading = true;
      return agent.Users.disable(iduser)
          .then(action(() => { this.getAllUsers() }))
          .finally(action(() => { this.isLoading = false; }))
  }
  @action updateAllSalesWechat() {
    this.isLoading = true;
    return agent.Account.getAllSalesWechat()
        .then(action((res) => {
          if(res.length > 0){
            this.AllSalesWechat = res
             // console.log(" saleswechat: "+JSON.stringify(res))
          }else{
            this.AllSalesWechat = []
          }
        }))
        .finally(action(() => { this.isLoading = false; }));
  }
  @action AllWechat() {
    this.isLoading = true;
    return agent.Account.getAllWechat()
        .then(action((res) => {
          if(res.length > 0){
            this.AllSalesWechatName = res
             // console.log(" saleswechat: "+res)
          }else{
            this.AllSalesWechatName = []
          }
        }))
        .finally(action(() => { this.isLoading = false; }));
  }

  @action updateUser(newUser) {
      this.isLoading = true;
      return agent.Users.update(newUser)
          .then(action(() => { this.getAllUsers() }))
          .finally(action(() => { this.isLoading = false; }))
  }

  @action RemoveWeChat(wechatId){
    this.isLoading =true;
    return agent.Account.getIdWeChat(wechatId)
        .then(
            action((res)=>{
          if(res.length>0){
            agent.Account.removeWeChat(res[0].id)
                .then(action(()=>{this.updateAllSalesWechat()}))
                .then(action(()=>{this.AllWechat()}))
          }
          else{
            console.log("getIdWechat no data")
          }
        }))
        .finally(action(()=>{
          console.log("adasfasfasfa")
          this.isLoading=false;}))
  }
  @action AddWeChat(idUser,wechatId){
    this.isLoading=true;
    return agent.Account.getIdWeChat(wechatId)
        .then(
            action((res)=>{
              if(res.length>0){
                agent.Account.addWeChat(idUser,res[0].id)
                    .then(action(()=>{this.updateAllSalesWechat()}))
                    .then(action(()=>{this.AllWechat()}))
              }
              else{
                console.log("AddWeChat no data")
              }
            }))
        .finally(action(()=>{this.isLoading=false;}))
  }
  @action getIdByWeChatId(wechatId){
    this.isLoading=true;
    return agent.Account.getIdWeChat(wechatId)
        .finally(action(()=>{this.isLoading=false;}))
  }



}

export default new UserStore();
