import { observable, action, computed, createTransformer } from 'mobx';
import moment from 'moment'
import agent from '../agent';
import authStore from './authStore';
import API_URL from '../config'

const LIMIT = 100;

export class OrdersStore {

  @observable isLoading = false;
  @observable isSaleOrdersLoading = false;
  @observable isCounsellorOrdersLoading = false;
  @observable submitOrderLoading = false;
  @observable creating = false;
  @observable ordersRegistry = observable.map();
  @observable packageOrders = observable.map();
  @observable ordersChecked = observable.map();
  @observable completedOrders = observable.map(); //by page
  @observable historyOrders = observable.map(); //by month
    @observable PriceSearchOrders = []; //by idcourse
  @observable customerOrders = observable.map();
  @observable orderPackage = observable.map();
  @observable packageChecked = observable.map();
  @observable CompletedOrdersTotal = {"total": 0};
  @observable CompletedOrdersMonths = [];
  @observable OrdersMonths = []

  @observable salePackage = observable.map();

  @observable predicate = {};
  @observable orderComments = observable.map();

  @observable userOrders = observable.map();
  @observable userOrdersDetail = observable.map();
  @observable userPackages = observable.map();


  @observable saleOrdersResult= observable.map();
  @observable saleHistoryOrders = observable.map(); //by month
  @observable saleOrdersDetail = observable.map();
  @observable SaleCompletedOrdersMonths = [];

  @observable orderResult = observable.map();


  @observable invitations = observable.map();
  @observable userInvitations = observable.map();



  @observable statistic = observable.map();
  @observable myStatistic = observable.map();

  @observable givenOrder = {};



  @computed get Orders() {
    return this.ordersRegistry.values();
  };
  @computed get OrderPackages(){
    return this.orderPackage.values()
  }
  @computed get getOrderById(){
    return createTransformer(id=>{
      if(this.ordersRegistry.get(id)){
        return this.ordersRegistry.get(id)
      }
      else {
        console.log(this.historyOrders)
        return this.historyOrders.get(id)
      }
    })
  }

  @computed get getOrdersByCustomerId(){
    return createTransformer(customerId=>{
      let cache = this.customerOrders.get(customerId)
      if(cache){
        return [...cache.values()]
      }else{
        this.getOrdersByIdCustomer(customerId)
        return []
      }
    })
  }
  @computed get getCompletedOrders(){
    return createTransformer(({page, rowsPerPage}) => {
      let cache = this.completedOrders.get(page)
      // console.log(page, cache)
      if(cache){
        return [...cache.values()]
      }else{
        this.getCompletedOrdersByPage(page, rowsPerPage)
        return []
      }
    })
  }
  @computed get getHistoryOrders(){
    return createTransformer((month) => {
      let cache = this.historyOrders.get(month)
      console.log(month, cache)
      if(cache){
        return [...cache.values()]
      }else{
        this.getHistoryOrdersByMonth(month)
        return []
      }
    })
  }

  // @computed getOrdersByOrderPackageId(idorderPackage){
  //   return createTransformer(idorderPackage => {
  //     let orderPackageOrders = []
  //     for(let [key, value] of this.ordersRegistry){
  //       if(value.orderPackage_idorderPackage === idorderPackage){
  //         orderPackageOrders.push(value)
  //       }
  //     }
  //     console.log('orderPackageOrders:', orderPackageOrders)
  //     return orderPackageOrders
  //   })
  // }
  @computed get getOrderPackageById(){
    return createTransformer(id=>this.orderPackage.get(id))
  }

  @computed get UserOrdersDetail(){
    let orders = []
    for(let [key, orderDetail] of this.userOrdersDetail){
      console.log(key, this.userOrdersDetail, orderDetail)
      if(!orderDetail) return []
      console.log(orderDetail.orderPackage_idorderPackage, !orderDetail.orderPackage_idorderPackage)
      // if(!orderDetail.orderPackage_idorderPackage){
        orderDetail['result'] = this.userOrders.get(orderDetail.idorder)
        orders.push(orderDetail)
      // }
    }
    return orders
  }
  @computed get UserOrdersPaidNumber(){
    let number = 0
    for(let [key, orderDetail] of this.userOrdersDetail){
      if(orderDetail.counselorPayment) number += 1
    }
    return number
  }
  @computed get UserPackages(){
    let userPackages = new Map()
    for(let [key, packageDetail] of this.orderPackage){
      for(let [orderKey, orderDetail] of this.userOrdersDetail){
        console.log(orderDetail.orderPackage_idorderPackage, key)
        if(key == orderDetail.orderPackage_idorderPackage){
          userPackages.set(key,packageDetail)
        }
      }
    }
    return [...userPackages.values()]
  }
  @computed get UserOrderDetailByIdpackage(){
    return createTransformer(idpackage=>{
      let orders = []
      for(let [orderKey, orderDetail] of this.userOrdersDetail){
        if(idpackage === orderDetail.orderPackage_idorderPackage){
          orderDetail['result'] = this.userOrders.get(orderDetail.idorder)
          orders.push(orderDetail)
        }
      }
      return orders
    })
  }

  @computed get getUserOrderById(){
    return createTransformer(id=>this.userOrders.get(id))
  }
  @computed get getUserOrderDetailById(){
    return createTransformer(id=>this.userOrdersDetail.get(id))
  }
  @computed get getUserPackageResultById(){
    return createTransformer(id=>this.userPackagesResult.get(id))
  }
  @computed get getUserPackageDetailById(){
    return createTransformer(id=>this.userPackagesDetail.get(id))
  }

  @computed get SaleOrdersDetail(){
    return this.saleOrdersDetail.values()
  }
  @computed get getSaleHistoryOrders(){
    return createTransformer((month) => {
      let cache = this.saleHistoryOrders.get(month)
      if(cache){
        return [...cache.values()]
      }else{
        let iduser = authStore.currentUser.iduser
        this.getSaleHistoryOrdersByMonth(iduser, month)

        return []
      }
    })
  }
  @computed get SaleOrdersResult(){
    return this.saleOrdersResult.values()
  }
  @computed get SalePackages(){
    return this.salePackage.values()
  }

  @computed get getSaleOrderResultById(){
    return createTransformer(id=>this.saleOrdersResult.get(id))
  }
  @computed get getSaleOrderDetailById(){
    return createTransformer(id=>this.saleOrdersDetail.get(id))
  }

  @computed get getOrderCommentsById(){
    return createTransformer(idorder=>{
      if(this.orderComments.get(idorder)) return this.orderComments.get(idorder)
      else {
        this.getOrderCommentByOrderId(idorder)
        return []
      }
    })
  }
  @computed get getOrderResultById(){
    return createTransformer(idorder=>{
      if(this.orderResult.get(idorder)) return this.orderResult.get(idorder)
      else {
        this.getOrderResultByOrderId(idorder)
        return []
      }

    })
  }

    // @computed get getOrderResultByCourse(){
    //     return createTransformer(course=>{
    //         if(this.PriceSearchOrders.get(course)) return this.PriceSearchOrders.get(course)
    //         else {
    //              this.getHistoryOrdersByIdCourse(course)
    //             return []
    //
    //         }
    //     })
    // }

  @computed get getInvitations(){
    return createTransformer(idorder=>{
      if(this.invitations.get(idorder)) return this.invitations.get(idorder)
      else {
        this.getInvitationsByOrderId(idorder)
        return []
      }
    })
  }

  @computed get qaInvitations(){
    return this.userInvitations.get('qa')
  }
  @computed get translaterInvitations(){
    return this.userInvitations.get('translater')
  }


  @computed get getStatisticByKey(){
    return createTransformer(key=>this.statistic.get(key))
  }
  @computed get getMyStatisticByKey(){
    return createTransformer(key=>this.myStatistic.get(key))
  }



  @action putOrdersChecked(idorder) {
    this.ordersChecked.set(idorder, this.ordersRegistry.get(idorder))
    console.log(idorder)
  }
  @action delOrdersChecked(idorder) {
    this.ordersChecked.delete(idorder)
  }
  @computed get  checked(){
    return createTransformer(idorder =>{
      if(this.ordersChecked.get(idorder)) return true
      return false
    })
  }

  @computed get getGivenOrder(){
    return createTransformer(idorder=>{
      if(this.ordersRegistry.get(idorder)) return this.ordersRegistry.get(idorder)
      else {
        this.getGivenOrderById(idorder)
        //return []
      }

    })
  }

  @action putPackageChecked(idorderPackage) {
    this.packageChecked.set(idorderPackage, this.orderPackage.get(idorderPackage))
  }
  @action delPackageChecked(idorderPackage) {
    this.packageChecked.delete(idorderPackage)
  }
  @computed get  packageCheckedCheck(){
    return createTransformer(idorderPackage =>{
      if(this.packageChecked.get(idorderPackage)) return true
      return false
    })
  }

  @action init(){
    this.loadOrders()
    this.loadOrderPackage()
    this.getStatistic()
    this.loadPackageOrders()

    //console.log(authStore.currentUser)
    // if((''+authStore.currentUser.name).length>0)
      if(authStore.currentUser.name !== undefined && authStore.currentUser.name !== null)
    {
      this.getUserOrders(authStore.currentUser.iduser)
      this.getUserOrdersDetail(authStore.currentUser.iduser)
      this.getSaleOrdersResult(authStore.currentUser.iduser)

      this.getSalesOrdersDetail(authStore.currentUser.iduser)
      this.loadSalePackage(authStore.currentUser.iduser)
      this.getMyStatistic(authStore.currentUser.iduser)
      this.getInvitationsByIduser(authStore.currentUser.iduser, 'qa')
      this.getInvitationsByIduser(authStore.currentUser.iduser, 'translater')
      this.getCompletedOrdersTotal()
      this.getCompletedOrdersMonths()
      this.getOrdersMonths()
      this.getSaleCompletedOrdersMonths(authStore.currentUser.iduser)
      let current_month = moment().format("YYYY-MM")
      this.getHistoryOrdersByMonth(current_month)

    }
  }

  @action loadOrders() {
    this.isLoading = true;
    return agent.Orders.all()
      .then(action((Orders) => {
        this.ordersRegistry.clear();
        this.ordersChecked.clear();
        Orders.forEach(order => this.ordersRegistry.set(order.idorder, order));
      }))
      .finally(action(() => { this.isLoading = false; }));
  }
  @action loadPackageOrders() {
    this.isLoading = true;
    return agent.Orders.getPackageOrders()
        .then(action((Orders) => {
          this.packageOrders.clear();
          Orders.forEach(order => this.packageOrders.set(order.idorder, order));
        }))
        .finally(action(() => { this.isLoading = false; }));
  }

  @action createOrder(order) {
    this.creating = true;
    console.log(order)
    return agent.Orders.create(order)
    .then(action(res =>{
      this.init()
      return res
    }))
    .finally(action(() => {
      this.creating = false;
    this.isLoading = false;
    this.submitOrderLoading = false;
    }))
  }


  @action EditPackageOrder(order,totalCost,idorderPackage) {
    this.creating = true;
    console.log(order)
    return agent.Orders.create(order)
        .then(action(res =>{
          return res
        }))
        .then(
            this.updatePackageCosts(totalCost,idorderPackage)
                .then(action(res =>{
                  this.init()
                  return res
                }))
        )
        .finally(action(() => { this.creating = false;}))
  }

  @action createNewPackageOrder(order,totalCost,idorderPackage) {
    this.creating = true;
    console.log(order)
    return agent.Orders.create(order)
        .then(action(res =>{
          return res
        }))
        .then(
            this.updatePackageCosts(totalCost,idorderPackage)
                .then(action(res =>{
                  this.init()
                  return res
                }))
        )
        .finally(action(() => { this.creating = false;}))
  }




  @action updateOrder(orderObj) {
    this.creating = true;
    console.log(orderObj)
    return agent.Orders.update(orderObj) ////obj likes {key=key, value=value, idorder=idorder}
    .then(action(res =>{
      this.init()
      return res
    }))
    .finally(action(() => { this.creating = false;}))
  }
  @action updatePackage(orderObj) {
    this.creating = true;
    console.log(orderObj)
    return agent.Package.update(orderObj) ////obj likes {key=key, value=value, idorder=idorder}
        .then(action(res =>{
          this.init()
          return res
        }))
        .finally(action(() => { this.creating = false;}))
  }
  @action deleteOrder(idorder) {
    this.creating = true;
    return agent.Orders.delete(idorder) ////obj likes {key=key, value=value, idorder=idorder}
    .then(action(res =>{
      this.init()
      return res
    }))
    .finally(action(() => { this.creating = false;}))
  }

  @action getOrderResultByOrderId(idorder) {
    return agent.Orders.getOrderResultByOrderId(idorder)
      .then(action((Orders) => {
        if(Orders) this.orderResult.set(idorder, Orders);
      }))
  }
  @action deleteOrderResultByOrderId(idorder) {
    return agent.Orders.deleteOrderResultByOrderId(idorder)
      .then(action((res) =>
        this.orderResult.delete(idorder)
      ))
  }
  @action deleteOrderResultByOrderIdUserId(idorder, iduser) {
    return agent.Orders.deleteOrderResultByOrderIdUserId(idorder, iduser)
      .then(action((res) => {
        let orginal = this.orderResult.get(idorder)
        let newOrderResults = orginal.filter(ele=>ele.assigned_iduser!==iduser)
        this.orderResult.set(idorder, newOrderResults)
      }))
  }

  @action getCompletedOrdersByPage(page, rowsPerPage){
    return agent.Orders.getCompletedOrdersByPage(page*rowsPerPage, rowsPerPage)
    .then(action((orders) => {
      // console.log(page, orders)
      if(orders)  this.completedOrders.set(page, orders)
    }))
  }
  @action getCompletedOrdersTotal(){
    return agent.Orders.getCompletedOrdersTotal()
    .then(action(total => {
      // console.log(total)
      this.CompletedOrdersTotal = total[0]
    }))
  }

  @action getCompletedOrdersMonths(){
    return agent.Orders.getCompletedOrdersMonths()
    .then(action(months => {
      // console.log(months)
      this.CompletedOrdersMonths = months
    }))
  }
  @action getOrdersMonths(){
    return agent.Orders.getOrdersMonths()
    .then(action(months => {
      // console.log(months)
      this.OrdersMonths = months
    }))
  }


  @action getOrdersByIdCustomer(idcustomer){
    return agent.Orders.getOrdersByIdCustomer(idcustomer)
    .then(action(Orders => {
      // console.log(months)
      if(Orders) this.customerOrders.set(idcustomer, Orders)
    }))
  }
  @action getHistoryOrdersByMonth(month){
    return agent.Orders.getHistoryOrdersByMonth(month)
    .then(action(orders => {
      this.historyOrders.set(month, orders)
    }))
  }


    @action getHistoryOrdersByIdCourse(course){
        this.isLoading = true;
        console.log(course)
        fetch(`${API_URL.ROOT_API_URL}/searchbycourse`, {
            method: 'POST',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',

            },
            body: JSON.stringify({
                "course": course
            })

        })
            .then(action(res=>{
                if(res.status === 200){
                    return res.json()
                }else{

                }
            }))
            .then(action(json => {
                if(json.length > 0){
                    this.PriceSearchOrders = json
                  // console.log("pricedata： "+json)
                }else{
                    this.PriceSearchOrders = []
                    //this.total = 0
                }
            }))
            .finally(action(()=>this.isLoading=false))
    }


    @action loadOrderPackage() {
    this.isLoading = true;
    return agent.OrderPackage.getAll()
      .then(action((OrderPackages) => {
        this.orderPackage.clear();
        this.packageChecked.clear();
        OrderPackages.forEach(ele => this.orderPackage.set(ele.idorderPackage, ele));
      }))
      .finally(action(() => { this.isLoading = false; }));
    }

  @action updateOrderPackage(orderPackage) {
    this.isLoading = true;
    return agent.OrderPackage.update(orderPackage)
    .then(action(res =>{
      this.init()
      return res
    }))
    .finally(action(() => { this.isLoading = false;}))
  }

  @action updatePackageStatus(idorderPackage, status) {
    this.isLoading = true;
    return agent.OrderPackage.updatePackageStatus(idorderPackage, status)
    .then(action(res =>{
      this.init()
      return res
    }))
    .finally(action(() => { this.isLoading = false;}))
  }

  @action updatePackageCosts(cost,idorderPackage) {
    console.log("update packge "+cost,idorderPackage)
    this.isLoading = true;
    return agent.OrderPackage.updateThePackageCost(cost,idorderPackage)
        .then(action(res =>{
          this.init()
          return res
        }))
        .finally(action(() => { this.isLoading = false;}))
  }




  @action getUserOrders(iduser) {
    this.isLoading = true;
    return agent.SignedUserOrders.getUserOrders(iduser)
      .then(action((Orders) => {
        this.userOrders.clear();
        Orders.forEach(order => this.userOrders.set(order.orders_idorder, order));
        console.log("userorder"+JSON.stringify(Orders))
      }))
      .finally(action(() => { this.isLoading = false; }));
  }
  @action getUserOrdersDetail(iduser) {
    this.isCounsellorOrdersLoading = true;
    return agent.SignedUserOrders.getUserOrdersDetail(iduser)
      .then(action((Orders) => {
        this.userOrdersDetail.clear();
        Orders.forEach(order => this.userOrdersDetail.set(order.idorder, order));
      }))
      .finally(action(() => { this.isCounsellorOrdersLoading = false; }));
  }
  @action updateUserOrder(order) {
    this.isLoading = true;
    // console.log(order)
    return agent.SignedUserOrders.updateUserOrder(order)
    .then(action(res =>{
      this.orderResult.clear()
      this.init()
      return res
    }))
    .finally(action(() => { this.isLoading = false;}))
  }

  @action getSalesOrdersDetail(iduser) {
    this.isSaleOrdersLoading = true;
    //console.log(iduser)
    return agent.Orders.getSaleOrdersByWechat(iduser)
      .then(action((Orders) => {
        this.saleOrdersDetail.clear();
        if (Orders && Orders.length)
          Orders.forEach(order => this.saleOrdersDetail.set(order.idorder, order));
      }))
      .finally(action(() => { this.isSaleOrdersLoading = false; }));
  }

  @action getSaleCompletedOrdersMonths(iduser){
    console.log(iduser)
    return agent.Orders.getSaleCompletedOrdersWechatMonths(iduser)
    .then(action(months => {
        // console.log("where is month"+months)
      this.SaleCompletedOrdersMonths = months

    }))
  }

  @action getSaleHistoryOrdersByMonth(iduser, month){
    return agent.Orders.getSaleHistoryOrdersByMonth(iduser, month)
    .then(action(orders => {
      this.saleHistoryOrders.set(month, orders)
        // console.log(month,orders)
    }))
  }

  @action getSaleOrdersResult(iduser) {
    this.isLoading = true;
    return agent.Orders.getOrderResultsByIduser(iduser)
      .then(action((Orders) => {
        this.saleOrdersResult.clear();
        if(Orders) Orders.forEach(order => this.saleOrdersResult.set(order.orders_idorder, order));
      }))
      .finally(action(() => { this.isLoading = false; }));
  }
  @action loadSalePackage(iduser) {
    this.isLoading = true;
    return agent.OrderPackage.getOrderPackageByUser(iduser)
      .then(action((OrderPackages) => {
        this.salePackage.clear();
        OrderPackages.forEach(ele => this.salePackage.set(ele.idorderPackage, ele));
      }))
      .finally(action(() => { this.isLoading = false; }));
  }




  @action getOrderCommentByOrderId(idorder){
    return agent.Orders.getComments(idorder).then(
      action( comments => {
        this.orderComments.set(idorder, comments)
        return comments
      })
    )
  }
  @action updateOrderComment(comment){
    this.isLoading = true;
    return agent.Orders.updateComment(comment)
    .then(action(res =>{
      this.getOrderCommentByOrderId(comment.order_idorder)
      return res
    }))
    .finally(action(() => { this.isLoading = false;}))
  }

  @action updatePackageComment(comment){
    this.isLoading = true;
    return agent.Package.updateComment(comment)
        .then(action(res =>{
          //this.getOrderCommentByOrderId(comment.order_idorder)
          return res
        }))
        .finally(action(() => { this.isLoading = false;}))
  }
  @action deleteOrderComment(comment){
    this.isLoading = true;
    return agent.Orders.deleteComment(comment.idcommentsToOrder)
    .then(action(res =>{
      this.getOrderCommentByOrderId(comment.order_idorder)
      return res
    }))
    .finally(action(() => { this.isLoading = false;}))
  }

  @action getInvitationsByOrderId(idorder){
    return agent.Orders.getInvitations(idorder).then(
      action( invitations => {
        this.invitations.set(idorder, invitations)
        return invitations
      })
    )
  }
  @action getInvitationsByIduser(iduser, role){
    return agent.Orders.getInvitationsByIduser(iduser, role).then(
      action( invitations => {
        this.userInvitations.set(role, invitations)
      })
    )
  }
  @action updateInvitation(invitation){
    this.isLoading = true;
    return agent.Orders.updateInvitation(invitation)
    .then(action(res =>{
      this.init()
      this.getInvitationsByOrderId(invitation.orders_idorder)
      return res
    }))
    .finally(action(() => { this.isLoading = false;}))
  }

  @action getStatistic(){
    this.isLoading = true;
    return agent.Orders.getStatistic()
    .then(action(res =>{
      this.statistic.clear()
      res.forEach(ele => this.statistic.set(ele.status, ele.count));

    }))
    .finally(action(() => { this.isLoading = false;}))
  }
  @action getMyStatistic(iduser){
    this.isLoading = true;
    return agent.Orders.getMyStatistic(iduser)
    .then(action(res =>{
      this.myStatistic.clear()
      if(res) res.forEach(ele => this.myStatistic.set(ele.status, ele.count));

    }))
    .finally(action(() => { this.isLoading = false;}))
  }

  @action getOrdersBySaleCreateTime(iduser, createTime){
    return agent.Orders.getOrdersBySaleCreateTime(iduser, createTime)
  }
  @action getOrdersBySaleDeadline(iduser, deadline){
    return agent.Orders.getOrdersBySaleDeadline(iduser, deadline)
  }
  @action getOrdersByCounsellorDeadline(iduser, deadline){
    return agent.Orders.getOrdersByCounsellorDeadline(iduser, deadline)

  }

  @action emailAdmin(orderId, orderName, counsellorID, counsellorName) {
    this.isLoading = true;

    return agent.Councellor.emailAdmin(orderId, orderName, counsellorID, counsellorName) ////obj likes {key=key, value=value, idorder=idorder}
        .then(action(res =>{
          return res
        }))
        .finally(action(() => { this.isLoading = false;}))
  }
  @action emailSale(orderId, orderName, counsellorID, counsellorName, mode) {
    this.isLoading = true;
   console.log(mode)
    return agent.Councellor.emailSale(orderId, orderName, counsellorID, counsellorName, mode) ////obj likes {key=key, value=value, idorder=idorder}
        .then(action(res =>{
          return res
        }))
        .finally(action(() => { this.isLoading = false;}))
  }

  @action getOrdersByOrderPackageId(idorderPackage){

      let orderPackageOrders = []
    console.log(this.packageOrders)
      for(let [key, value] of this.packageOrders){
        if(value.orderPackage_idorderPackage === idorderPackage){
          orderPackageOrders.push(value)
        }
      }
      console.log('orderPackageOrders:', orderPackageOrders)
      return orderPackageOrders

  }

  @action WeChatOptionValue(WeChat){
    let option =0 ;
    console.log(WeChat)
    let idWechat = WeChat.split('(')[1].replace(')', "")
     return agent.Account.getIdWeChat(idWechat)
  }

  @action submitProcess(){

    this.submitOrderLoading = true
    this.isLoading = true

  }
  @action getGivenOrderById(id){
    // if (this.ordersRegistry.get(id)){
    //   console.log("AAAAAAA")
    //   //return this.ordersRegistry.get(id)
    //   this.givenOrder = this.ordersRegistry.get(id)
    // }
    // else {
      //this.isLoading = true
      console.log("BBBBB")
       return agent.Orders.getOrderByID(id).then(action(res =>{
        if(res[0]){

          this.ordersRegistry.set(id, res[0])
          console.log("AAAAAAAAAAAAAAAAAAA")
          console.log(this.ordersRegistry.get(id))
          return res[0]
        }
      }))
          .finally(action(() => { this.isLoading = false;}))


  }
}

export default new OrdersStore();
