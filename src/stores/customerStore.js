import { observable, computed, action, createTransformer } from 'mobx';
import agent from '../agent';
import authStore from './authStore';

class CustomerStore {
  @observable isLoading;
  @observable users = observable.map();
  @observable saleCustomers = [];
    @observable saleName = '';

  @computed get Customers(){
    return [...this.users.values()]
  }

  @computed get getCustomerById(){
    return createTransformer(id=>this.users.get(id)||{})
  }


  @computed get getSalerCustomer(){
    return createTransformer(iduser=>{
        if(this.saleCustomers.length>0) {
            // console.log("yes" +iduser)
          return this.saleCustomers;

        }
        else {
          // console.log("no"+iduser);
            this.getSaleCustomer(iduser)
            return []
    }})
  }
  @computed get getCustomerByCode(){
    return createTransformer(code=>{
      let customers = this.users.values();
      for(let cu of customers){
        // console.log(cu, code)
        if(cu.code==code) return cu
      }
      return {}
    })
    
  }
  @computed get getCustomerByEmail(){
    return createTransformer(email=>{
      let customers = this.users.values();
      for(let cu of customers){
        if(cu.customerEmail==email) return cu
      }
      return {}
    })
  }

  @action init(){
    this.getAllCustomers()
  }

  @action getAllCustomers(){
    this.isLoading = true;
    if(authStore.currentUser.role && authStore.currentUser.role.indexOf('saleman')>=0){
      this.getSaleCustomer(authStore.currentUser.iduser)
    }
    return agent.Customers.all()
      .then(action((res) => { 
        this.users.clear()
        if(res) res.forEach(ele => this.users.set(ele.idcustomer, ele));
      }))
      .finally(action(() => { this.isLoading = false; }))
  }


  @action deleteCustomer(iduser) {
      this.isLoading = true;
      return agent.Customers.delete(iduser)
          .then(action(() => { this.getAllCustomers() }))
          .finally(action(() => { this.isLoading = false; }))
  }

  @action updateCustomer(newUser) {
      this.isLoading = true;
      return agent.Customers.update(newUser)
          .then(action((res) => { 
            this.getAllCustomers()
            return res
           }))
          .finally(action(() => { this.isLoading = false; }))
  }

    @action  getSaleCustomer(iduser){
        this.isLoading = true;
        return agent.Customers.getSaleCustomersByWechat(iduser)
            .then((res) => {
              this.saleCustomers = res
                // console.log("my customers: "+res)
            })
            .finally(action(() => { this.isLoading = false; }))
    }
    @action  getSaleInfo(iduser){
        this.isLoading = true;
        return agent.Account.getWechatInfo(iduser)
            .then(action((res) => {
                let name = `${res[0].wechatName}(${res[0].wechatId})`
                this.saleName = name
                // console.log("my customers: "+res)
                console.log(this.saleName)
            }))
            .finally(action(() => { this.isLoading = false;
            }))
    }
}

export default new CustomerStore();
