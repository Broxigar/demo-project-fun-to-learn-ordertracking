import { observable, computed, action, createTransformer } from 'mobx';
import agent from '../agent';

class ExchangeRequestStore {
    @observable isLoading = false;
    @observable requests = observable.map();
  
    @computed get exchangeRequests(){
      return this.requests.values()
    }
  
    @computed get getExchangeRequestById(){
      return createTransformer(id=>this.requests.get(id)||{})
    }
  
    @action init(){
      this.getAllExchangeRequests()
    }
  
    @action getAllExchangeRequests(){
      this.isLoading = true;
      return agent.Exchange.selectRequest()
        .then(action((res) => { 
          this.requests.clear()
          if(res) res.forEach(ele => this.requests.set(ele.idexchangeRequest, ele));
        }))
        .finally(action(() => { this.isLoading = false; }))
    }
  
  
    @action deleteExchangeRequest(idexchangeRequest) {
        this.isLoading = true;
        return agent.Exchange.deleteRequest(idexchangeRequest)
            .then(action(() => { this.getAllExchangeRequests() }))
            .finally(action(() => { this.isLoading = false; }))
    }
  
    @action updateExchangeRequest(exchangeRequest) {
        this.isLoading = true;
        return agent.Exchange.updateRequest(exchangeRequest)
          .then(action((res) => { 
            this.getAllExchangeRequests()
            return res 
          }))
          .finally(action(() => { this.isLoading = false; }))
    }
    
  }
  
  export default new ExchangeRequestStore();