import React, { Component } from 'react';
import {Button, Modal, Form, Input, Radio, notification, Alert, Select} from 'antd';
import agent from '../agent';
import {inject} from "mobx-react";

const { Option } = Select;


const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
    // eslint-disable-next-line

    class extends React.Component {

        render() {
            const { visible, onCancel, onCreate, form, errorVisible, onclose, country } = this.props;
            const { getFieldDecorator } = form;
            return (
                <Modal
                    visible={visible}
                    title="Add a new university"
                    okText="Create"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <Form.Item label="University Name">
                            {getFieldDecorator('name', {
                                rules: [{ required: true, message: 'Please input the university name!' }],
                            })(<Input />)}
                        </Form.Item>
                        <Form.Item label="Country of University">
                            {getFieldDecorator('country', {
                                rules: [{ required: true, message: 'Please input the country of university!' }],
                            })(
                                //<Input type="textarea" />
                                <Select style={{ width: 270 }}>
                                    {country.map((c) => <Option value={c.idnationCode}>{c.nation.toUpperCase()}</Option>)}

                                </Select>
                                )}
                        </Form.Item>
                    </Form>
                    {errorVisible ? (
                        <Alert
                            message="Error"
                            description="University already exists"
                            type="error"
                            closable
                            onClose={onclose}
                        />): null}

                </Modal>
            );
        }
    },
);
const openNotificationWithIcon = (type) => {
    notification.config({
        placement: 'bottomRight',
        bottom: 50,
        duration: 5,
    });

    notification[type]({
        message: 'Successfully Added New Wechat',
    });


};
@inject('userStore','helperStore')
export default class UniAddTable extends React.Component {
    state = {
        visible: false,
        errorVisible: false,
    };

    showModal = () => {
        this.setState({ visible: true });
    };

    handleCancel = () => {
        this.setState({ visible: false });
    };

    handleCreate = () => {
        const { form } = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
             //console.log(values)
            // if (this.props.userStore.AllSalesWechatName.wechatId.indexOf(values.account) >= 0){
            //     console.log("no")
            // }

            for (let val of this.props.helperStore.universities){
                if (val.name.indexOf(values.name)>=0){
                    this.setState({errorVisible: true})
                    return;
                }
            }
            //console.log("asdfasdfasfd")
            return agent.Helper.addUni(values.country, values.name)
                .then((res)=>{
                    //console.log(res)
                    if (res.statusCode ===200){
                        //this.props.reloadUni();
                        this.props.helperStore.loadUniversities()
                        form.resetFields();
                        this.setState({ visible: false });
                        openNotificationWithIcon('success')
                    }
                })



        })
    };
    onClose = () => {
        this.setState({errorVisible: false})
    };


    saveFormRef = formRef => {
        this.formRef = formRef;
    };

    render() {
        return (
            <div>
                <Button type="primary" onClick={this.showModal}>
                    Add New University
                </Button>
                <CollectionCreateForm
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                    errorVisible={this.state.errorVisible}
                    onclose={this.onClose}
                    country={this.props.helperStore.country}
                />
            </div>
        );
    }
}
