import { Table, Input, InputNumber, Popconfirm, Form, message, notification, Divider } from 'antd';
import React, { Component } from 'react';
import IconButton from '@material-ui/core/IconButton';
import ArrowBack from '@material-ui/icons/ArrowBack';
import {withStyles} from "@material-ui/core";
import {inject, observer} from "mobx-react";
import {withRouter} from "react-router-dom";
import agent from '../agent';


// const data = [];
// for (let i = 0; i < 100; i++) {
//     data.push({
//         key: i.toString(),
//         name: `Edrward ${i}`,
//         age: 32,
//         address: `London Park no. ${i}`,
//     });
// }
const EditableContextE = React.createContext();

class EditableCell extends React.Component {
    getInput = () => {
        if (this.props.inputType === 'number') {
            return <InputNumber />;
        }
        return <Input />;
    };

    renderCell = ({ getFieldDecorator }) => {
        const {
            editing,
            dataIndex,
            title,
            inputType,
            record,
            index,
            children,
            ...restProps
        } = this.props;
        return (
            <td {...restProps}>
                {editing ? (
                    <Form.Item style={{ margin: 0 }}>
                        {getFieldDecorator(dataIndex, {
                            rules: [
                                {
                                    required: true,
                                    message: `Please Input ${title}!`,
                                },
                            ],
                            initialValue: record[dataIndex],
                        })(this.getInput())}
                    </Form.Item>
                ) : (
                    children
                )}
            </td>
        );
    };

    render() {
        return <EditableContextE.Consumer>{this.renderCell}</EditableContextE.Consumer>;
    }
}

const openNotificationWithIcon = (type, message) => {
    notification.config({
        placement: 'bottomRight',
        bottom: 50,
        duration: 3,
    });
    if (message ===1){
        notification[type]({
            message: 'Successfully Edited Wechat',
        });
    }
    else if (message ===2){
        notification[type]({
            message: 'Successfully Deleted Wechat',
        });
    }

};
@inject('userStore', 'helperStore')
@withRouter
@observer
class EditableTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data:'', editingKey: '' };
        this.columns = [
            {
                title: 'Wechat Name',
                dataIndex: 'wechatName',
                width: '40%',
                editable: true,
            },
            {
                title: 'Wechat Account',
                dataIndex: 'wechatId',
                width: '40%',
                editable: false,
            },

            {
                title: 'operation',
                dataIndex: 'operation',
                render: (text, record) => {
                    const { editingKey } = this.state;
                    const editable = this.isEditing(record);
                    return editable ? (
                        <span>
              <EditableContextE.Consumer>
                {form => (
                    <a
                        onClick={() => this.save(form, record.id)}
                        style={{ marginRight: 8 }}
                    >
                        Save
                    </a>
                )}
              </EditableContextE.Consumer>
              <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.id)}>
                <a>Cancel</a>
              </Popconfirm>
            </span>
                    ) : (<span>
                        <a disabled={editingKey !== ''} onClick={() => this.edit(record.id)}>
                            Edit
                        </a>
                            <Divider type="vertical" />
                            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.id)}>
                                <a>Delete</a>
                            </Popconfirm>
                    </span>

                    );
                },
            },
        ];
    }

    componentDidMount() {
        this.loadData()
    }

    loadData = ()=> {

        this.props.userStore.AllWechat()

    }

    handleDelete = key => {
        return agent.Account.deleteWechat(key)
            .then((res)=>{
                console.log(res)
                if (res.statusCode ===200){
                    openNotificationWithIcon('success', 2)
                    this.loadData()
                }
            })
    };

    isEditing = record => record.id === this.state.editingKey;

    cancel = () => {
        this.setState({ editingKey: '' });
    };



    save(form, key) {
        form.validateFields((error, row) => {
            if (error) {
                return;
            }

            //console.log(row.wechatName)
            return agent.Account.updateWechat(row.wechatName, key)
                .then((res)=>{
                    console.log(res)
                    if (res.statusCode ===200){
                        console.log("adsfasf")
                        openNotificationWithIcon('success', 1)
                        this.setState({ editingKey: '' });
                        this.loadData()
                    }
                })
            // const newData = [...this.state.data];
            // const index = newData.findIndex(item => key === item.id);
            // if (index > -1) {
            //     const item = newData[index];
            //     console.log(item)
            //     newData.splice(index, 1, {
            //         ...item,
            //         ...row,
            //     });
            //     this.setState({ data: newData, editingKey: '' });
            //
            //
            // } else {
            //     newData.push(row);
            //     this.setState({ data: newData, editingKey: '' });
            // }
        });
    }

    edit(key) {
        this.setState({ editingKey: key });
    }

    render() {
        const { classes, userStore} = this.props
        const components = {
            body: {
                cell: EditableCell,
            },
        };

        const columns = this.columns.map(col => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    inputType: col.dataIndex === 'age' ? 'number' : 'text',
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                }),
            };
        });

        return (
            <EditableContextE.Provider value={this.props.form}>
                <Table
                    components={components}
                    bordered
                    dataSource={userStore.AllSalesWechatName}
                    columns={columns}
                    rowClassName="editable-row"
                    pagination={{
                        onChange: this.cancel,
                    }}
                />
            </EditableContextE.Provider>
        );
    }
}
export const EditableFormTable = Form.create()(EditableTable);

// export default class wechatEditTable extends React.Component {
//     render() {
//         return (
//             <EditableFormTable />
//         )
//     }
// }