import React, { Component } from 'react';
import {EditableFormTable} from "./wechatEditTable";
import IconButton from '@material-ui/core/IconButton';
import ArrowBack from '@material-ui/icons/ArrowBack';

export default class WechatEdit extends React.Component{

    render(){

        return(
            <div>
                <h3>
                <IconButton>
                    <ArrowBack onClick={() => this.props.cancleFunction ? this.props.cancleFunction() : this.props.history.goBack()} />
                </IconButton>
                &nbsp; { this.props.formName || "Edit Wechat"}
            </h3>
                <br/>
            <EditableFormTable />
            </div>
        )
    }
}
