import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import {NavLink, withRouter} from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { Typography ,Modal} from 'antd';
import UserStore from '../stores/userStore';
import EditableAddFormTable from "./wechatAddTables";
import { Table, Tooltip, Tag,Menu, Dropdown, Icon ,Select} from 'antd';
import {Grid} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {Link} from 'react-router-dom';
import uniAddTable from './uniAddTables'
import SaleKPI from "../charts/SaleKPI";


const styles = theme => ({
    root: {
        width: '100%',
        padding: theme.spacing.unit * 2,
        backgroundColor: theme.palette.background.paper,
    },
    paddingTop: {
        paddingTop: theme.spacing.unit * 2,
    },
    tabsRoot: {
        width: '100%',
        borderBottom: '1px solid #e8e8e8',
        paddingTop: theme.spacing.unit,
    },
    paddingBot: {

        paddingBottom:10,
    },
});


const confirm = Modal.confirm;

@withStyles(styles)
@inject('userStore', 'helperStore')
@withRouter
@observer

export default class SalesWechat extends Component {

    state = {
        minValue:0,
        maxValue:8,
        numEachPage:8,
    };

    //delete label
    onDeselect = (value) => {

        if (value !== null){
            confirm({
                title: 'Are you sure remove this WeChat?',
                content: 'Remove this WeChat from the current Salesman',
                okText: 'Yes',
                okType: 'danger',
                cancelText: 'No',
                onOk() {
                    UserStore.RemoveWeChat(value.key);
                    // alert("Removed")

                },
                onCancel() {
                    ;
                },
            });
        }

    }


    //Add new tag
    handleChangeSelect = column => value =>{

        confirm({
            title: 'Are you sure changing this WeChat?',
            content: 'Remove this WeChat from the current Salesman and Assign to: '+column.name +' ?',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                console.log(value)
                UserStore.AddWeChat(column.iduser,value);
                // alert("The selected WeChat has been assigned to: "+column.name)
            },
            onCancel() {
                ;
            },
        });
    };




    render() {
        const { classes, userStore} = this.props;
        const { Title,Text } = Typography;
        const { Option } = Select;
        const data = userStore.AllSalesWechat;
        const SalewechatName =userStore.AllSalesWechatName;
        const columns = [
            {
                title: 'SalesName',
                dataIndex: 'name',
                key: 'name',
            },
            {
                title: 'Country',
                dataIndex: 'country',
                key: 'country',
            },
            {
                title: 'PhoneNumber',
                dataIndex: 'phone',
                key: 'phone',
            },
            {
                title: 'Email',
                dataIndex: 'email',
                key: 'email',
            },
            {
                title: 'WeChatName(WeChatID)',
                key: 'wechat',
                dataIndex: 'wechat',

                render: (tags,column) => (
                    <Select
                        mode="multiple"
                        style={{ width: '100%' }}
                        placeholder="select one WeChat"
                        value={
                            (tags)?Object.keys(JSON.parse(tags)).map(key=> {
                                console.log(key)
                                return (<span role="img" key={JSON.parse(tags)[key]}>
                                    <Text type="warning">{key}</Text>{"(" + JSON.parse(tags)[key] + ")"}
                                </span>)



                        })
                        : null}
                        // onChange={()=>this.handleChangeSelect()}
                        onSelect={this.handleChangeSelect(column)}
                        onDeselect={this.onDeselect}
                        optionLabelProp="label"
                    >
                        {SalewechatName.map(value=> {
                            //console.log(JSON.parse(tags)['Joey'])
                            return(
                                <Option  key={value.wechatId}
                                          disabled={value.name === column.name}
                                >
                                    <span role="img">{value.wechatName}</span>{"("+value.wechatId+")"}
                                    {value.name?
                                        <Text code>{value.name}</Text>
                                        :
                                        <Text mark>Available</Text>
                                    }
                                </Option>
                            )
                        })}
                    </Select>
                ),
            },
        ];

        return (
            <div className={classes.root}>
                <Grid container justify={'space-between'} >
                    <Grid item>
                        <Title level={3}>Sales WeChat</Title>
                    </Grid>

                    <Grid className={classes.paddingTop}/>

                    <Grid item /><Grid item /><Grid item /><Grid item /><Grid item /><Grid item /><Grid item /><Grid item /><Grid item /><Grid item /><Grid item /><Grid item /><Grid item /><Grid item /><Grid item /><Grid item />
                    <Grid item >
                        <EditableAddFormTable />

                    </Grid>
                    <Grid item >
                        <Button variant="outlined" component={Link} to="/WechatEdit" color="secondary" className={classes.paddingLeft} >
                            Edit Wechat
                        </Button>
                    </Grid>



                </Grid>
                <Table columns={columns} dataSource={data} rowKey="iduser" pagination={false}/>
            </div>

        )
    }
}



