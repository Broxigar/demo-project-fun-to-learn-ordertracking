import ordersStore from '../stores/ordersStore'
import authStore from '../stores/authStore'
import {addOrderComment, addPackageComment} from './addOrderComment'
import config from '../config'
import agent from "../agent";


// const URL = config.WS_URL;
// const ws = new WebSocket(URL)

const updateOrderStatus = (order, status) =>{
    /*
    {
      idorder: 1,
      fields: [
        {key: "status", value: "verifying"}
        {key: "currentProcessorRole", value: "qa"}
      ]
    }
    */
  let orderObj = {} 
  let fieldStatus = {}
  orderObj['idorder'] = order.idorder
  fieldStatus['key'] = 'status'
  fieldStatus['value'] = status
  orderObj['fields'] = [fieldStatus];


  ordersStore.updateOrder(orderObj).then(res => {
    if(res.statusCode === 200){
      addOrderComment(order, authStore.currentUser.iduser, `${authStore.currentUser.name}(${authStore.currentUser.iduser}) marked this order as ${fieldStatus['value']}`)
    }
  }).then(()=>{
    // console.log(authStore.ws)
    //authStore.ws.onopen =()=> {
    console.log('Order Status ws is connected!!')
    let message={
      orderID:order.idorder,
      to_salesID:order.user_iduser,
      to_xieshouID:0,
      to_qaID:0,
      from_iduser:authStore.currentUser.iduser,
      type:3
    }
    // console.log(message)
    authStore.ws.send(JSON.stringify(message))
  })
  // if(order.orderPackage_idorderPackage){
  //   ordersStore.updatePackageStatus(order.orderPackage_idorderPackage, status)
  // }
}

const updateOrderCurrencyCost = (order) =>{
  let orderObj = {} 
  let fieldStatus = {}
  orderObj['idorder'] = order.idorder
  fieldStatus['key'] = 'currency_cost'
  fieldStatus['value'] = order.currency_cost
  orderObj['fields'] = [fieldStatus]
  ordersStore.updateOrder(orderObj).then(res => {
    if(res.statusCode === 200){
      addOrderComment(order, authStore.currentUser.iduser, `${authStore.currentUser.name}(${authStore.currentUser.iduser}) changed the currency cost to ${fieldStatus['value']} . `)
    }
  })
}

const updateOrderCost = (order) =>{
  let orderObj = {} 
  let fieldStatus = {}
  orderObj['idorder'] = order.idorder
  fieldStatus['key'] = 'cost'
  fieldStatus['value'] = order.cost
  orderObj['fields'] = [fieldStatus]
  ordersStore.updateOrder(orderObj).then(res => {
    if(res.statusCode === 200){
      addOrderComment(order, authStore.currentUser.iduser, `${authStore.currentUser.name}(${authStore.currentUser.iduser}) changed the cost to ${fieldStatus['value']} . `)
    }
  })
}

const confirmOrderPayment = (order) =>{
  let orderObj = {} 
  let fieldStatus = {}
  let toIdUser =''
  orderObj['idorder'] = order.idorder
  fieldStatus['key'] = 'paymentVerify'
  fieldStatus['value'] = 'verified'
  orderObj['fields'] = [fieldStatus]
console.log(authStore.ws)
  //authStore.ws.onopen =()=> {
    console.log('confirmOrderPayment ws is connected!!')
    let message={
      orderID:order.idorder,
      // fromIdUser:authStore.currentUser.iduser,
      // fromUserName:authStore.currentUser.name,
      to_salesID:order.user_iduser,
      to_xieshouID:0,
      to_qaID:0,
      // from:'007',
      // to:'001',
      from_iduser:authStore.currentUser.iduser,
      type:1
    }
    console.log(message)
    authStore.ws.send(JSON.stringify(message))
  //}

   ordersStore.updateOrder(orderObj).then(res => {
     if(res.statusCode === 200){
       // websocket send confirm info to the salesman
       agent.Account.getWechatInfo(order.wechat).then((res)=>{
         if(res.statusCode===200){
  
           console.log("wechat: "+JSON.stringify(res[0]))
  
         }
        })
           // .then(()=>{
  
  
       addOrderComment(order, authStore.currentUser.iduser, `${authStore.currentUser.name}(${authStore.currentUser.iduser}) verified the ${order.paymentStatus} payment. `)
     }
   })

}

const confirmPackagePayment = (order) =>{
  let orderObj = {}
  let fieldStatus = {}
  orderObj['idorderPackage'] = order.idorderPackage
  fieldStatus['key'] = 'paymentVerify'
  fieldStatus['value'] = 'verified'
  orderObj['fields'] = [fieldStatus]
  ordersStore.updatePackage(orderObj).then(res => {
    if(res.statusCode === 200){
      addPackageComment(order, authStore.currentUser.iduser, `${authStore.currentUser.name}(${authStore.currentUser.iduser}) verified the ${order.paymentStatus} payment. `)
    }
  })
}

const updateCounselorPayment = (order, status) =>{
  let orderObj = {}
  let fieldStatus = {}
  orderObj['idorder'] = order.idorder
  fieldStatus['key'] = 'counselorPayment'
  fieldStatus['value'] = JSON.stringify(status)
  orderObj['fields'] = [fieldStatus]
  ordersStore.updateOrder(orderObj).then(res =>{
    if (res.statusCode == 200) {
      addOrderComment(order, authStore.currentUser.iduser, `${authStore.currentUser.name}(${authStore.currentUser.iduser}) confirmed the counselor paid. `)
    }
  }) 
}

export {updateOrderStatus, updateOrderCurrencyCost, updateOrderCost, confirmOrderPayment, updateCounselorPayment, confirmPackagePayment}