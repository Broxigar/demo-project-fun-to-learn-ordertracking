import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import Badge from '@material-ui/core/Badge';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import {success, info, warning, error, gray, bginfo, bgwarning, bgerror, bgsuccess, lightBlue, bgLightBlue} from '../common/Colors'

const styles = theme => ({
    label:{
      padding: '3px',
      paddingLeft: '5px',
      paddingRight: '5px',
      marginRight: '5px',
      marginTop: '4px',
      borderRadius: 3,
      fontSize: '12px',
      color: gray, 
      textDecoration: 'None',
    },
    span: {
      fontSize: '14px',
      color: gray,
      paddingLeft: '5px'
    },
    Extremely: {
        backgroundColor: error,
    },
    Important: {
        backgroundColor: warning,
    },
    Normal: {
        backgroundColor: info,
    }, 
    Success: {
        backgroundColor: success,
    },
    marginRight: {
        marginRight: theme.spacing.unit *1,
    },
    badge: {
        top: 5,
        right: -15,
        cursor: 'pointer',
    },
  });

@withStyles(styles)
@withRouter
@observer
class GenerateLabelView extends Component{
    render(){
        let labelArray = this.props.labels
        let labels = []
        if (typeof(labelArray) === 'string'){
            labelArray = JSON.parse(labelArray)
        }
        labelArray = labelArray || []
        labelArray = labelArray.sort(function(a,b){
            if(a.level<b.level) return -1
            if(a.level>b.level) return 1
            return 0
        })
        for(let i=0; i<labelArray.length; i++){
            let labelobj = labelArray[i]
            labels.push(<span className={[this.props.classes.label, this.props.classes[labelobj.level]].join(' ')} key={i} >{labelobj.content}</span>)
        }
        return labels
  }
}

@withStyles(styles)
@withRouter
@observer
class GenerateLabelEdit extends Component{
    handleLabelDelete(label){
        let labels = this.props.labels
        if(typeof(labels) == 'string') labels = JSON.parse(labels)
        labels = labels||[]
        let newLabels = this.deleteLabelArrayEle(labels, label)
        this.props.updateLabel(newLabels)
    }
    
    deleteLabelArrayEle(array, label){
        let newArray = []
        for(let ele of array){
          if(label.level!==ele.level && label.content!=ele.content) newArray.push(ele)
        }
        return newArray
      }

    render(){
        let labelArray = this.props.labels
        let labels = []
        if (typeof(labelArray) === 'string') labelArray = JSON.parse(labelArray)
        labelArray = labelArray || []
        labelArray = labelArray.sort(function(a,b){
          if(a.level<b.level) return -1
          if(a.level>b.level) return 1
          return 0
        })

        for(let i=0; i<labelArray.length; i++){
          let labelobj = labelArray[i]
          labels.push(
            <Badge 
                badgeContent={'x'} 
                color="secondary" 
                classes={{badge: this.props.classes.badge}} 
                className={this.props.classes.marginRight} 
                onClick={()=> this.handleLabelDelete(labelobj)}>
                <span className={[this.props.classes.label, this.props.classes[labelobj.level]].join(' ')} key={i} >
                {labelobj.content}
                </span>
            </Badge>)
        }
        return labels
    }
}

@withStyles(styles)
@withRouter
@observer
class GenerateSimpleLabel extends Component{
    render(){
        let labelArray = this.props.labels||''
        let labels = []
        labelArray = labelArray.split('|')

        for(let i=0; i<labelArray.length; i++){
          let labelobj = labelArray[i]
          if(labelobj.length >0)
            labels.push(
                <span className={[this.props.classes.label, this.props.classes.Success].join(' ')} key={i} >{labelobj}</span>
                )
        }
        return labels
    }
}

@withStyles(styles)
@withRouter
@observer
class GenerateSimpleLabelEdit extends Component{
    handleLabelDelete(label){
        let labelArray = this.props.labels||''
        labelArray = labelArray.split('|')
        let newLabels = this.deleteLabelArrayEle(labelArray, label)
        this.props.updateLabel(newLabels.join('|'))
    }
    
    deleteLabelArrayEle(array, label){
        let newArray = []
        for(let ele of array){
          if(label!==ele) newArray.push(ele)
        }
        return newArray
      }

    render(){
        let labelArray = this.props.labels||''
        let labels = []
        labelArray = labelArray.split('|')

        for(let i=0; i<labelArray.length; i++){
          let labelobj = labelArray[i]
          if(labelobj.length >0)
            labels.push(
                <Badge 
                    badgeContent={'x'} 
                    color="secondary" 
                    key={i}
                    classes={{badge: this.props.classes.badge}} 
                    className={this.props.classes.marginRight} 
                    onClick={()=> this.handleLabelDelete(labelobj)}>
                    <span className={[this.props.classes.label, this.props.classes.Success].join(' ')} key={i} >
                    {labelobj}
                    </span>
                </Badge>)
        }
        return labels
    }
}

export { GenerateLabelView, GenerateLabelEdit, GenerateSimpleLabel, GenerateSimpleLabelEdit } 