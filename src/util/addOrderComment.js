import moment from 'moment'
import ordersStore from '../stores/ordersStore'

const addOrderComment = (order, iduser, content) =>{
    let comment = {}
    comment['dateTime'] = moment.tz().format('YYYY-MM-DDTHH:mm:ss')
    comment['content'] = content
    comment['order_idorder'] = order.idorder
    comment['commentBy'] = iduser
    comment['status'] = 1
    ordersStore.updateOrderComment(comment)
  }

const addPackageComment = (order, iduser, content) =>{
    let comment = {}
    comment['dateTime'] = moment.tz().format('YYYY-MM-DDTHH:mm:ss')
    comment['content'] = content
    comment['idorderPackage'] = order.idorderPackage
    comment['commentBy'] = iduser
    comment['status'] = 1
    ordersStore.updatePackageComment(comment)
}
export {addOrderComment, addPackageComment}