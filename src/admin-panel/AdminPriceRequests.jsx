import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter, NavLink } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { Tabs, Tab, Grid} from '@material-ui/core'
import Button from '@material-ui/core/Button';


import loading from '../common/loading'
import PriceRequestDetail from '../common/PriceRequestDetail'

const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  paddingTop: {
    paddingTop: theme.spacing.unit * 2,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
    paddingTop: theme.spacing.unit,
  },
});

@withStyles(styles)
@inject('priceRequestStore')
@withRouter
@observer
export default class AdminPriceRequests extends Component {
  state = {
    currentStatus: 'requesting', 
    priceRequestStatus: ['requesting','confirm']
  }
  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
  };
  render(){
    const { classes, priceRequestStore } = this.props;
    const { currentStatus, priceRequestStatus } = this.state
    const priceRequests = priceRequestStore.PriceRequests
    if(priceRequestStore.isLoading) return loading
    return(
      <div className={classes.root}>
        <Grid container justify={'space-between'} >
          <Grid item>
          </Grid>
          <Grid item>
            <Button  variant="outlined" component={NavLink} to="/newPriceRequest" color="secondary" className={classes.button}>
              Create Price Request
            </Button>
          </Grid>
        </Grid>
        <Tabs 
          value={currentStatus} 
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          className={classes.tabsRoot}
          >
          { priceRequestStatus.map((item, key) => <Tab key={key} value={item} label={item} /> )}
        </Tabs>

        <Grid container item className={classes.paddingTop}>
          {priceRequests.map((row, index) => {
            if(row.status == currentStatus) return <PriceRequestDetail request={row} key={index} />
          })}
        </Grid>
      </div>
    );
    
  }
} 

