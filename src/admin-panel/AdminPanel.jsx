import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Collapse from '@material-ui/core/Collapse'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ViewList from '@material-ui/icons/ViewList'
import TrackChanges from '@material-ui/icons/TrackChanges';
import ListIcon from '@material-ui/icons/List';
import Folder from '@material-ui/icons/Folder';
import LibraryBooks from '@material-ui/icons/LibraryBooks';
import HdrWeak from '@material-ui/icons/HdrWeak'
import BarChart from '@material-ui/icons/BarChart'
import PieChart from '@material-ui/icons/PieChart'
import BlurOn from '@material-ui/icons/BlurOn'
import History from '@material-ui/icons/History'
import ShowChart from '@material-ui/icons/ShowChart'
import Divider from '@material-ui/core/Divider';
import cyan from '@material-ui/core/colors/cyan';


@withStyles( theme =>({
  active: {
    backgroundColor: cyan[50],
  },
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 3,
  },
}))
export default class AdminPanel extends Component{
  state = {
    open: true,
  };

  handleClick = () => {
    this.setState(state => ({ open: !state.open }));
  };
  render(){
    const { classes } = this.props;
    return(
      <List>
        <ListItem button onClick={this.handleClick}>
          <ListItemIcon>
            <ViewList />
          </ListItemIcon>
          <ListItemText inset primary="Admin Panel" />
          {this.state.open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
      
        <Collapse in={this.state.open} timeout="auto" unmountOnExit>
          <Divider />
          <List component="div" >
            <ListItem button component={NavLink} to="/orders"  className={classes.nested}  exact activeClassName={classes.active} >
              <ListItemIcon>
                <ListIcon />
              </ListItemIcon>
              <ListItemText primary="Orders" />
            </ListItem>
            <ListItem button component={NavLink} to="/historyOrders"  className={classes.nested}  exact activeClassName={classes.active} >
              <ListItemIcon>
                <History />
              </ListItemIcon>
              <ListItemText primary="History Orders" />
            </ListItem>
            <ListItem button className={classes.nested} component={NavLink} to="/packages" exact activeClassName={classes.active} >
              <ListItemIcon>
                <Folder />
              </ListItemIcon>
              <ListItemText primary="Packages" />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/priceRequest" exact activeClassName={classes.active}>
              <ListItemIcon>
                <TrackChanges />
              </ListItemIcon>
              <ListItemText primary="Price Quotes" />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/courses" exact activeClassName={classes.active}>
              <ListItemIcon>
                <LibraryBooks />
              </ListItemIcon>
              <ListItemText primary="Courses" />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/orderTrend" exact activeClassName={classes.active}>
              <ListItemIcon>
                <ShowChart />
              </ListItemIcon>
              <ListItemText primary="Order Trend" />
            </ListItem>
            
            <ListItem button  className={classes.nested} component={NavLink} to="/orderIncrease" exact activeClassName={classes.active}>
              <ListItemIcon>
                <BarChart />
              </ListItemIcon>
              <ListItemText primary="Order increase " />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/customerAnalysis" exact activeClassName={classes.active}>
              <ListItemIcon>
                <PieChart />
              </ListItemIcon>
              <ListItemText primary="Order increase " />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/heatmapAnalysis" exact activeClassName={classes.active}>
              <ListItemIcon>
                <BlurOn />
              </ListItemIcon>
              <ListItemText primary="Order increase " />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/statistic" exact activeClassName={classes.active}>
              <ListItemIcon>
                <HdrWeak />
              </ListItemIcon>
              <ListItemText primary="Statistic" />
            </ListItem>
          </List>
        </Collapse>
        <Divider />
      </List>
    );
  }
} 


